package com.chatbot.exceptions.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {
	@ExceptionHandler({ Exception.class })
	public ResponseEntity<Object> handleExceptions(Exception ex, WebRequest request){
		return new ResponseEntity<Object>(
		          "Internal exception", new HttpHeaders(), HttpStatus.EXPECTATION_FAILED);
	}
}

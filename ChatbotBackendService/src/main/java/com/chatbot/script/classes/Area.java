package com.chatbot.script.classes;

import lombok.Data;

@Data
public class Area {
	private String shape;
	private String propertice;
	private String link;
}

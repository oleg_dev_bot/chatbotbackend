package com.chatbot.script.classes;

import lombok.Data;

@Data
public class Value {
	private String text;
	private String link;
}

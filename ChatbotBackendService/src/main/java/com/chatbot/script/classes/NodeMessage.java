package com.chatbot.script.classes;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Data;

//@Document
@Data
@JsonInclude(Include.NON_NULL)
public class NodeMessage{
	private String type;
	private String text;
	private String url;
	private String extension;
	private String location;
	private String link;
	private String state;
	private String image;
	private List<Value> values;
	private List<Area> areas;
	private List<Object> messages;
}

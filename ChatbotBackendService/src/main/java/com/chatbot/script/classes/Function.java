package com.chatbot.script.classes;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class Function {
	private String id;
}

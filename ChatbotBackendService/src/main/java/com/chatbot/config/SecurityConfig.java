package com.chatbot.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.savedrequest.NullRequestCache;
import org.springframework.session.web.http.HeaderHttpSessionStrategy;
import org.springframework.session.web.http.HttpSessionStrategy;

import com.chatbot.handlers.CustomLogoutSuccessHandler;
import com.chatbot.services.UserSecurityService;


/**
 * This class manages the configuration of spring security
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	@Autowired
	private Environment env;
	
	@Autowired
	private UserSecurityService userSecurityService;
	@Autowired
	private CustomLogoutSuccessHandler logoutSuccessHandler;
	
	private static final String[] PUBLIC_MATCHERS = {
			"/css/**",
			"/js/**",
			"/images/**",
			"/webjars/**",
			"/forgot**",
			"/reset-password**",
			"/registration**",
			"/test/**"
	};
	
	private BCryptPasswordEncoder passwordEncoder() {
		return SecurityUtility.passwordEncoder();
	}
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().cors().disable().httpBasic().and().authorizeRequests()
		.antMatchers(PUBLIC_MATCHERS).permitAll().anyRequest().authenticated();
		//.and()
		//.requestCache()
		//.requestCache(new NullRequestCache())
		//.and()
		//.logout()
		//.logoutSuccessHandler(logoutSuccessHandler);
		/*http.cors().disable().httpBasic().and().authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/reset-password**").permitAll()
		.antMatchers(PUBLIC_MATCHERS).permitAll().anyRequest().authenticated();*/
	}	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userSecurityService).passwordEncoder(passwordEncoder());
	}
}
 
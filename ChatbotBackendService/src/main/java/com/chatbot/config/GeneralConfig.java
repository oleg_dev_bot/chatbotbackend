package com.chatbot.config;

import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.chatbot.services.CounterServiceImpl;
import com.chatbot.services.MessageService;
import com.ibm.watson.developer_cloud.tone_analyzer.v3.ToneAnalyzer;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;


/**
 * This class manage the general configuration of application
 * and creates spring beans
 */
@Configuration
@ComponentScan(basePackages= {"com.chatbot"})
public class GeneralConfig {
	@Value("${toneanalyzer.user}")
    private String user;
    @Value("${toneanalyzer.pass}")
    private String pass;
    @Value("${spring.data.mongodb.uri}")
    private String mongoUri;
    
    
    @Bean
    public ToneAnalyzer toneAnalyzer(){
    	return new ToneAnalyzer("2017-09-21", user, pass);
    }
    
	@Bean
	public RestTemplate restTemplate() {		
		return new RestTemplate(new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build()));
	}
	
	@Bean
	public CounterServiceImpl counterService() {
		return new CounterServiceImpl();
	}
	
	/*@Bean
	public MongoClient mongoClient() {
		 MongoClientURI connectionString = new MongoClientURI("");
		 return new MongoClient(connectionString);
	}*/
}

package com.chatbot.dto;

import java.util.List;

import lombok.Data;

@Data
public class ScriptDictionariesDto {
	private Long scriptId;
	private List<Long> dictionaryIdList;
}

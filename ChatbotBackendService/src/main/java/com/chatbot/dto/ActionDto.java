package com.chatbot.dto;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class ActionDto {
	private Long id;
	private Long accountId;
	private Long ownerId;
	private LocalDateTime date;
	private String status;
	private String description;
}

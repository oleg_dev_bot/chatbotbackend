package com.chatbot.dto;

import java.time.LocalDateTime;
import java.util.List;

import com.chatbot.domain.Group;
import com.chatbot.domain.Message;
import com.chatbot.domain.Script;

import lombok.Data;

@Data
public class BroadcastFullInfoDto {
	private int id;
	private String name;
	private int status;
	private long relatedGroupId	;
	private int messageSendCount;	
	private int messageOpendCount;	
	private long messageId;
	private long scriptId;
	private int platform;
	private LocalDateTime startDay;
	
	private Script script;
	private Group group;
	private Message message;
}

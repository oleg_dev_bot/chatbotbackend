package com.chatbot.dto;

import java.util.List;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class ChatbotDto {
	private Long id;
	private String name;	
	private String description;	
	private Integer lang;
	private String company;	
	private String serverURL;	
	private Integer timeZoneIndex;
	private String apiKey;	
	private String developerToken;	
	private Double totalPayment;
	private Long adminId;
	private Integer openAccount;
	private Long frontSettingsWebApplicationId;
	private Long frontSettingsWidgetId;
	private Long frontSettingsFacebookId;
	private Long frontSettingsTwitterId;
	private Long frontSettingsTelegramId;
	private Long frontSettingsSlackId;
	private Long frontSettingsVibarId;
	private Long plan;
	private Long payment;
	
	private List<ScriptDto> scripts;
}

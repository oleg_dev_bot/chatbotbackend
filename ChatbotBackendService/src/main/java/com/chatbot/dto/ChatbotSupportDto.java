package com.chatbot.dto;

import com.chatbot.domain.Support;

import lombok.Data;

@Data
public class ChatbotSupportDto {
	private Long chatbotId;
	private Support ticket;
}

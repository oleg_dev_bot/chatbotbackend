package com.chatbot.dto;

import com.chatbot.domain.Message;

import lombok.Data;

@Data
public class BroadcastMessageDto {
	private long broadcastId;
	private Message message;
}

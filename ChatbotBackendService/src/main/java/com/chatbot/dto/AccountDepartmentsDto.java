package com.chatbot.dto;

import java.util.List;

import lombok.Data;

@Data
public class AccountDepartmentsDto {
	private Long accountId;
	private List<Long> departments;
}

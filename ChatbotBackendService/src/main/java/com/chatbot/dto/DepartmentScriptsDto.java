package com.chatbot.dto;

import java.util.List;

import lombok.Data;


/**
 * This class manages the relation department and scripts
 *
 */
@Data
public class DepartmentScriptsDto {
	private Long departmentId;
	private List<Long> scripts;
}

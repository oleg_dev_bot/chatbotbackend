package com.chatbot.dto;

import java.util.List;

import lombok.Data;

@Data
public class ChatbotDictionariesDto {
	private Long chatbotId;
	private List<Long> dictionaryIdList;
}

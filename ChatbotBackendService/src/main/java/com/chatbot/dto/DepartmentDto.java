package com.chatbot.dto;

/**
 * This class manages department object which is sent to client
 *
 */
public class DepartmentDto {
	private Long id;
	private String name;
	private Long ownerId;
}

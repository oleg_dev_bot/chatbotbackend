package com.chatbot.dto;

import java.util.List;

import lombok.Data;

/**
 * This class manages the relation department and accounts
 *
 */
@Data
public class DepartmentAccountsDto {
	private Long departmentId;
	private List<Long> accounts;
}

package com.chatbot.dto;

import java.util.List;

import com.chatbot.domain.Dictionary;
import com.chatbot.domain.Edge;
import com.chatbot.domain.Node;

import lombok.Data;

@Data
public class ScriptDto {
	private Long id;
	private String quickReply;
	private String name;
	private List<Node> nodes;
	private List<Edge> edges;
	
	private List<Dictionary> dictionaries;
}

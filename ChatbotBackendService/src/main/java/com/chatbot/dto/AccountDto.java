package com.chatbot.dto;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;

import lombok.Data;

@Data
public class AccountDto {
	private Long id;
	private String name;	
	private String title;
	private String email;
	private String phone;
	private String imageURL;
	private Integer status;
	private LocalDateTime lastLogin;
}

package com.chatbot.dto;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class BroadcastDto {
	private int id;
	private String name;
	private int status;
	private long relatedGroupId	;
	private int messageSendCount;	
	private int messageOpendCount;	
	private long messageId;
	private long scriptId;
	private int platform;
	private LocalDateTime startDay;
	//private long groupId;
}

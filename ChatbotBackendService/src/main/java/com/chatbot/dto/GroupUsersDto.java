package com.chatbot.dto;

import java.util.List;

import lombok.Data;

@Data
public class GroupUsersDto {
	private Long groupId;
	private List<Long> users;
}

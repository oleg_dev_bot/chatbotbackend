package com.chatbot.dto;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;

import javax.validation.constraints.AssertTrue;

/**
 * This class manages the form "registration"
 *
 */
@Data
public class UserRegistrationDto {
	
	private String name;
	
	@Email
    @NotEmpty
    private String email;
	
	//@NotEmpty
	private String company;
	
	//@NotEmpty
	private String title;
	
    @NotEmpty
    private String password;

    //@NotEmpty
    private String confirmPassword;   

}

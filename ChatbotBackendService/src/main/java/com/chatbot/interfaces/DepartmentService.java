package com.chatbot.interfaces;

import java.util.List;

import com.chatbot.domain.Account;
import com.chatbot.domain.Answer;
import com.chatbot.domain.Department;
import com.chatbot.domain.Script;

public interface DepartmentService {
	Department findById(Long departmentId);
	List<Department> loadDepartments();
	Boolean saveOrUpdate(Department department);
	void delete(Long departmentId);
	
	Boolean addAccountsToDepartment(Long departmentId, List<Long> accountsId);
	Boolean addScriptsToDepartment(Long departmentId, List<Long> scriptssId);
	Account getAccountInfoByDepartmentId(Long departmentId);
	List<Account> getAccountsOfDepartment(Long departmentId);
	List<Script> getScriptsOfDepartment(Long departmentId);
	Answer removeAccountsFromDepartment(Long departmentId, List<Long> accountsId);
	Answer removeScriptsFromDepartment(Long departmentId, List<Long> scriptsId);
	
}

package com.chatbot.interfaces;

import java.util.List;

import com.chatbot.domain.Account;
import com.chatbot.domain.Answer;
import com.chatbot.domain.Department;
import com.chatbot.domain.Group;
import com.chatbot.domain.User;

public interface GroupService {
	Group findById(Long groupId);
	List<Group> loadGroups();
	Boolean saveOrUpdate(Group group);
	void delete(Long groupId);
	
	Boolean addUsersToGroup(Long groupId, List<Long> usersId);
	Answer removeUsersFromGroup(Long groupId, List<Long> usersId);
	List<User> getUsersOfGroup(Long groupId);
	Account getAccountInfoByGroupId(Long groupId); 
}

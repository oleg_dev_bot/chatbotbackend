package com.chatbot.interfaces;

import java.util.List;

import com.chatbot.domain.Support;

public interface SupportService {
	List<Support> loadOpenTickets();
	List<Support> loadClosedTickets();
	Boolean saveOrUpdate(Support support);
	void delete(Long ticketId);
	List<Support> getAllTicketsOfChatbot(Long botId);
	Boolean saveChatbotSupport(Long chatbotId, Support support);
}

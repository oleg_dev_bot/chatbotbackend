package com.chatbot.interfaces;

import java.util.List;

import com.chatbot.domain.Dictionary;
import com.chatbot.domain.Script;
import com.chatbot.dto.ChatbotDictionariesDto;
import com.chatbot.dto.ScriptDictionariesDto;

public interface DictionaryService {
	Dictionary loadDictionaryById(Long dictionaryId);
	Boolean save(Dictionary dictionary);
	List<Dictionary> loadDictionariesOfChatbot(Long chatbotId);
	//List<Dictionary> loadDictionariesOfScriptsOfChatbot(Long chatbotId);
	void delete(Long dictionaryId);
	void assignDictionariesToChatbot(ChatbotDictionariesDto chatbotDictionaries);
	void assignDictionariesToScript(ScriptDictionariesDto scriptDictionaries);
}

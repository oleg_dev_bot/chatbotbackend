package com.chatbot.interfaces;

import java.time.LocalDateTime;
import java.util.Map;

public interface BroadcastGraphService {
	Map<Integer, Double> getBroadcastOpenGraph(Long broadcastId, LocalDateTime filter);
	Map<String, String> getBroadcastDeviceGraph(Long broadcastId, LocalDateTime filter);
	Map<Integer, Integer> getBroadcastConvertGraph(Long broadcastId, LocalDateTime filter);
	Map<Integer, Double> getBroadcastSentGraph(Long broadcastId, LocalDateTime filter);
}

package com.chatbot.interfaces;

import java.util.List;

import com.chatbot.domain.Plan;

public interface PlanService {
  List<Plan> loadPlans();
  void assignPlanToBot(Long chatbotId, Long planId);
  Double chatbotSpend(Long botId);
}

package com.chatbot.interfaces;

import java.util.List;
import java.util.Map;

import com.chatbot.domain.Broadcast;
import com.chatbot.domain.Department;
import com.chatbot.domain.Group;
import com.chatbot.dto.BroadcastDto;
import com.chatbot.dto.BroadcastFullInfoDto;

public interface BroadcastService {
	List<Broadcast> loadBroadcasts();
	Boolean saveOrUpdate(Broadcast broadcast);
	void delete(Long broadcastId);
	String getBroadcastNameById(Long broadcastId);
	Integer getBroadcastStatus(Long broadcastId);
	Group getBroadcastGroup(Long relatedGroupId);
	Integer HowManyMessagesSent(Long broadcastId);
	Integer HowManyChatsOpend(Long broadcastId);
	Map<Long, String> getBroadcastScriptName(Long broadcastId);
	String getBroadcastPlatform(Long broadcastId);
	Broadcast saveBroadcastMetadata(BroadcastDto broadcast);
	Broadcast loadBroadcastById(Long broadcastId);
	BroadcastFullInfoDto loadFullInfoAboutBroadcastById(Long broadcastId);
}

package com.chatbot.interfaces;

import java.util.List;

import com.chatbot.domain.Dictionary;
import com.chatbot.domain.Script;

public interface ScriptService {
	Script findById(Long scriptId);
	Boolean save(Script script);
	List<Script> loadScripts();
	List<Script> loadScriptsOfChatbot(Long chatbotId);
	void delete(Long scriptId);
	Boolean saveChatbotScript(Long chatbotId, Long scriptId);
	List<Dictionary> loadDictionariesOfScript(Long scriptId);
}

package com.chatbot.interfaces;

import java.util.List;

import com.chatbot.domain.Chatbot;
import com.chatbot.dto.ChatbotDto;

public interface ChatbotService {
	Chatbot findById(Long botId);
	Chatbot save(Chatbot chatbot);
	ChatbotDto loadScriptsAndDictionariesOfChatbot(Long chatbotId);
}

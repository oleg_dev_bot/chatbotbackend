package com.chatbot.interfaces;

import java.util.List;

import com.chatbot.domain.Department;
import com.chatbot.domain.HelpNote;

public interface HelpNoteService {
	List<HelpNote> loadHelpNotes();
	Boolean saveOrUpdate(HelpNote helpNote);
	void delete(Long helpNoteId);
}

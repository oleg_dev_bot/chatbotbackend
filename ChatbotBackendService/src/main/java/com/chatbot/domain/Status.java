package com.chatbot.domain;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public enum Status {
	A(0), B(1), C(2);
	
	private final int code;
	
	public int getCode() {
		return code;
	}

	Status(int code){
		this.code = code;
	}
	
	private static final Map<Integer, Status> BY_CODE_MAP = new HashMap<>();
	
	static {
        for (Status s : Status.values()) {
            BY_CODE_MAP.put(s.code, s);
        }
    }

    public static Status forCode(int code) {
        return BY_CODE_MAP.get(code);
    }
}

package com.chatbot.domain;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
public class Graph {
	private List<Node> nodes;
	private List<Edge> edges;
}

package com.chatbot.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This class manages the table department-script
 *
 */
@Getter
@Setter
@AllArgsConstructor
@Document(collection="department-script")
public class DepartmentScript {
	private Long departmentId;
	private Long scriptId;
}

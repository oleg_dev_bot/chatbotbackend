package com.chatbot.domain;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * This class manages all messages of user
 *
 */
@Data
public class UserMessages {
	private Long uid;
	private List<Message> messages;
	
	public UserMessages() {
		messages = new ArrayList<>();
	}	
}

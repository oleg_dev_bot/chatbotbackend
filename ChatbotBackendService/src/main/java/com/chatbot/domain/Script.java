package com.chatbot.domain;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class manages a script
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document
public class Script {
	@Id
	private Long id;
	private String quickReply;
	private String name;
	private String description;
	private List<Node> nodes;
	private List<Edge> edges;
}

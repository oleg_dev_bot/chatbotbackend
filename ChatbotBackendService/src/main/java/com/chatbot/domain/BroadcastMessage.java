package com.chatbot.domain;

import java.time.LocalDateTime;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class manages a broadcast's messages
 *
 */
@Getter
@Setter
@AllArgsConstructor
@Document(collection="broadcast-message")
public class BroadcastMessage {
	private long broadcastId;
	private long messageId;
}

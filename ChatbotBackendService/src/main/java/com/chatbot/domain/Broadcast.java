package com.chatbot.domain;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class manages a broadcast table
 *
 */
@Getter
@Setter
@NoArgsConstructor
@Document
public class Broadcast {
	@Id
	private Long id;
	private String name;
	private int status;
	private long relatedGroupId	;
	private int messageSendCount;	
	private int messageOpendCount;	
	private long messageId;
	private long scriptId;
	private int platform;
	private LocalDateTime startDay;
}

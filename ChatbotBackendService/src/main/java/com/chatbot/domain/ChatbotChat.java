package com.chatbot.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This class manages the table chatbot-chat
 *
 */
@Getter
@Setter
@AllArgsConstructor
@Document(collection="chatbot-chat")
public class ChatbotChat {
	private Long chatbotId;
	private Long chatId;
}

package com.chatbot.domain;

import java.time.LocalDateTime;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This class manages the table user-chat
 *
 */
@Getter
@Setter
@AllArgsConstructor
@Document(collection="user-chat")
public class ChatUser {
	private Long userId;
	private Long chatId;
	private Long nodeId;
}

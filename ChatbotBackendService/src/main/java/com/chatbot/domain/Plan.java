package com.chatbot.domain;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class manages chatbot's plan
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(collection="plans")
public class Plan {
	@Id
	private Long id;
	private String title;	
	private String description;	 
	private Integer cost;
}

package com.chatbot.domain;

import com.chatbot.enums.AnswerStatus;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Answer {	
	private String result;
	private boolean isSuccess;
	
	public Answer(AnswerStatus response) {
		result = response.name();
	}
}

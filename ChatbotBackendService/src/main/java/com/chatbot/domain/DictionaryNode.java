package com.chatbot.domain;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.chatbot.script.classes.Function;
import com.chatbot.script.classes.NodeMessage;

import lombok.Data;


@Data
public class DictionaryNode {
	private String id;
	private boolean isHit;
	private NodeMessage input_message;
	private List<NodeMessage> output_messages;
	private List<Function> functions;
}

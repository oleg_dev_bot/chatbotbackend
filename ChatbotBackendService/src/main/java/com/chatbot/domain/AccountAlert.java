package com.chatbot.domain;

import java.time.LocalDateTime;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This class manages alert of account
 *
 */
@Getter
@Setter
@AllArgsConstructor
@Document
public class AccountAlert {
	private Long accountId;
	private Long alertId;
}

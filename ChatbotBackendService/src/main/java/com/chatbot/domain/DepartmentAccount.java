package com.chatbot.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This class manages the table department-account
 *
 */
@Getter
@Setter
@AllArgsConstructor
@Document(collection="department-account")
public class DepartmentAccount {
	private Long departmentId;
	private Long accountId;
}

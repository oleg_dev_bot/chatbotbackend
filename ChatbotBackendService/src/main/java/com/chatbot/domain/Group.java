package com.chatbot.domain;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class manages a group
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document
public class Group {
	@Id
	private Long id;
	private String name;
	private String descripton;	
	private Long managerId;	
}

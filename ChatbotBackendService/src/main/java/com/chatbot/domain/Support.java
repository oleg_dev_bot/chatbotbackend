package com.chatbot.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class manages the support table
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Support {
	@Id
	private long id;
	private int type;
	private int urgency;
	private  String title;
	private String description;
	private int status;
	private LocalDateTime openDate;
	private LocalDateTime closeDate;
	private String response;
	private Boolean isOpen;
}

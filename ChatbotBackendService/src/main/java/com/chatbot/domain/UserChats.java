package com.chatbot.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This class manages all chats of user
 *
 */
@Getter
@Setter
public class UserChats {
	private Long userId;
	private List<Chat> chats;
	
	public UserChats(Long uid) {
		this.userId = uid;
		chats = new ArrayList<>();
	}
	
}

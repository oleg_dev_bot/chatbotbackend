package com.chatbot.domain;

import lombok.Data;

@Data
public class UserComment {
	private Long userId;
	private String comment;
}

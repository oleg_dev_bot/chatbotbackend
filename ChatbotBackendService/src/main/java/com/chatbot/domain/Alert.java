package com.chatbot.domain;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This class manages alert of account
 *
 */
@Getter
@Setter
@AllArgsConstructor
@Document
public class Alert {
	@Id
	private Long id;
	private Long accountId;
	private String title;
	private String message;	
	private LocalDateTime time;
	private Boolean isOpened;
	private String link;
}

package com.chatbot.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
@Data
public class Edge {
	private String from;
	private String to;
	private String condition;
}

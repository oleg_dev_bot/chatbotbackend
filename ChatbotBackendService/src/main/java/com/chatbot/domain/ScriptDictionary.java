package com.chatbot.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * This class manages the table script-dictionary
 *
 */
@Data
@Document(collection="script-dictionary")
public class ScriptDictionary {
	private Long scriptId;
	private Long dictionaryId;
}

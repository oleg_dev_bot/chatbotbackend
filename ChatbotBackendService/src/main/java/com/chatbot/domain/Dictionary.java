package com.chatbot.domain;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This class manages a dictionary
 *
 */
@Getter
@Setter
@AllArgsConstructor
@Document
public class Dictionary {
	@Id
	private Long id;
	private String name;
	private String description;
	private List<DictionaryNode> nodes;
}

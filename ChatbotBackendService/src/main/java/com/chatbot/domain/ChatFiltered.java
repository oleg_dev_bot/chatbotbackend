package com.chatbot.domain;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This class manages filtered chats
 *
 */
@Getter
@Setter
@AllArgsConstructor
@Document
public class ChatFiltered {
	@Id 
	private Long id;
	private Long userId;
	private Long botId;
	private Integer timeOnChat;
	private Boolean isOpen;
	private Boolean isHuman;
	private Boolean isHumanTalked;
	private LocalDateTime startChatTime;
	private boolean isEndHit;
	private Integer messagesToHit;
	private Long mechanism;
}

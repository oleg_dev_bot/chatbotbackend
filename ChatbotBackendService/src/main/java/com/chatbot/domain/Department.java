package com.chatbot.domain;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This class manages department
 *
 */
@Getter
@Setter
@AllArgsConstructor
@Document
public class Department {
	@Id
	private Long id;
	private String name;
	private Long ownerId;
	private String description;
}

package com.chatbot.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * This class manages an account
 *
 */
@Getter
@Setter
@NoArgsConstructor
@Document
public class Account implements UserDetails, Serializable{
	private static final long serialVersionUID = 1572177186687763275L;
	
	public Account(Long id, String name, String password, String token, String title, String email, String phone,
			String imageURL, Integer status, LocalDateTime lastLogin, String permissions, String[] authorities) {
		
		this.id = id;
		this.name = name;
		this.password = password;
		this.token = token;
		this.title = title;
		this.email = email;
		this.phone = phone;
		this.imageURL = imageURL;
		this.status = status;
		this.lastLogin = lastLogin;
		this.permissions = permissions;
		this.grantedAuthorities = AuthorityUtils.createAuthorityList(authorities);
	}

	
	
	@Id
	private Long id;
	private String name;	
	private String password;
	private String token;
	private String title;
	private String email;
	private String phone;
	private String imageURL;
	private Integer status;
	private LocalDateTime lastLogin;
	private String permissions;
	private List<GrantedAuthority> grantedAuthorities;
	
	public void setAuthorities(String[] authorities) {
		AuthorityUtils.createAuthorityList(authorities);
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return name;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}

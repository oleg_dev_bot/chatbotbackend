package com.chatbot.domain;

import java.util.HashMap;
import java.util.Map;

public enum Platform {
	PLATFORM1(0), PLATFORM2(1), PLATFORM3(2), PLATFORM4(3), PLATFORM5(4), PLATFORM6(5), PLATFORM7(6);
	
	private final int code;
	
	Platform(int code){
		this.code = code;
	}
	
	private static final Map<Integer, Platform> BY_CODE_MAP = new HashMap<>();
	
	static {
		for(Platform p: Platform.values()) {
			BY_CODE_MAP.put(p.code, p);
		}
	}
	
	public static Platform forCode(int code) {
        return BY_CODE_MAP.get(code);
    }
}

package com.chatbot.domain;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class manages a broadcastgraph_timestamp table
 *
 */
@Getter
@Setter
@NoArgsConstructor
@Document(collection="broadcastgraph_timestamp")
public class BroadcastGraph {
	@Id
	private Long id;
	private long broadcastId;
	private int timeStamp;
	private int messageSendCount;
	private int messageOpendCount;
	private int phoneCount;
	private int tabletCount;
	private int computerCount;
	private int convertCount;
}

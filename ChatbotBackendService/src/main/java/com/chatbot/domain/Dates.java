package com.chatbot.domain;

import java.time.LocalDateTime;

import lombok.Data;

/**
 * This class manages date interval
 *
 */
@Data
public class Dates {
	private LocalDateTime startDt;
	private LocalDateTime finishDt;
}

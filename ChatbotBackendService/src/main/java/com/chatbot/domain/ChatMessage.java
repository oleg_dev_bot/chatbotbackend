package com.chatbot.domain;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This class manages the table chatbot-message
 *
 */
@Getter
@Setter
@AllArgsConstructor
@Document(collection="chat-message")
public class ChatMessage {
	private Long chatId;
	private Long messageId;
	
}

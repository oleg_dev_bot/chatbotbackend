package com.chatbot.domain;

import java.time.LocalDateTime;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class manages connections of account
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document
public class AccountConnection {
	private Long accountId;
	private LocalDateTime timeDate;
	private boolean isLogin;
}

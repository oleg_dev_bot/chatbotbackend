package com.chatbot.domain;

import java.time.LocalDateTime;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This class manages action of account
 *
 */
@Getter
@Setter
@AllArgsConstructor
@Document(collection="accountAction")
public class Action {
	private Long id;
	private Long ownerId;
	private LocalDateTime date;
	private Integer status;
	private String description;
}

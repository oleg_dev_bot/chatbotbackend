package com.chatbot.domain;

import java.time.LocalDateTime;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This class manages the table chatbot-user
 *
 */
@Getter
@Setter
@AllArgsConstructor
@Document(collection="chatbot-user")
public class ChatbotUser {
	private Long chatbotId;
	private Long userId;
}

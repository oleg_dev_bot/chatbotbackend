package com.chatbot.domain;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.chatbot.script.classes.Function;
import com.chatbot.script.classes.NodeMessage;

import lombok.Data;

@Document
@Data
public class Node {
	private String id;
	private boolean isHit;
	private List<NodeMessage> messages;
	private List<Function> functions;
}

package com.chatbot.domain;

import lombok.Data;

/**
 * This class manages the user summary info
 *
 */
@Data
public class UserSummary {
	private Long id;
	private String name;
	private Integer status;
	private String comment;
	private String photoURL;
	private String phone;
	private String summary;
	private String location;
}

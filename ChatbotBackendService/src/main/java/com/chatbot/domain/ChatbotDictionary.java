package com.chatbot.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Document
public class ChatbotDictionary {
	private Long chatbotId;
	private Long dictionaryId;
}

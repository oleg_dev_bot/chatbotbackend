package com.chatbot.domain;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class manages the engine of chat
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Engine {
	@Id
	private Long id;
	private Integer state;
	private Integer errorCount;
	private Integer device;
	private Integer platform;

}

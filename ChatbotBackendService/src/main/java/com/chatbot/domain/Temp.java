package com.chatbot.domain;

/**
 * This class manages the calculation of summa 
 * and avg
 */
public class Temp {
	private static Long sum = 0L;
	private static Long count = 0L;
	
	public static void summa(Integer value) {
		sum += value;
		count++;
		//System.out.println(count);
	}
	
	public static Double getAvg() {
		Double avg = 0.0;
		
		if(count != null && count > 0) {
			avg = (double) (sum/count);
		}
		
		sum = 0L;
		count = 0L;
		
		return avg;
	}
	
	public static void setCount(Long cnt) {
		count = cnt;
		//System.out.println(cnt);
	}
}

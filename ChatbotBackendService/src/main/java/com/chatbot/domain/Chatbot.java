package com.chatbot.domain;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This class manages a chatbot
 *
 */
@Getter
@Setter
@Document
public class Chatbot {
	@Id
	private Long id;
	private String name;	
	private String description;	
	private Integer lang;
	private String company;	
	private String serverURL;	
	private Integer timeZoneIndex;
	private String apiKey;	
	private String developerToken;	
	private Double totalPayment;
	private Long adminId;
	private Integer openAccount;
	private Long frontSettingsWebApplicationId;
	private Long frontSettingsWidgetId;
	private Long frontSettingsFacebookId;
	private Long frontSettingsTwitterId;
	private Long frontSettingsTelegramId;
	private Long frontSettingsSlackId;
	private Long frontSettingsVibarId;
	private Long plan;
	private Long payment;
}

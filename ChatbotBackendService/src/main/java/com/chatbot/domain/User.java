package com.chatbot.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class manages the table user
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document
public class User {
	@Id
	private Long id;
	private String name;
	private String password;
	private Integer isMale;
	private Integer age;
	private String token;
	private String email;
	private Integer status;
	private String location;
	private String enterPoint;
	private Boolean isOnline;
	private LocalDate lastOnline;
	private String comment;
	private LocalDate tokenTime;
	private Integer secInChats;
	private Integer messagesSent;
	private Integer moneySpend;
	private String passwordToken;
	private LocalDate passwordTokenTime;
	private String UUID;
	private String summary;
	private String phone;
	private String photoURL;
}

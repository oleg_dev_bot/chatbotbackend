package com.chatbot.domain;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class manages a table messages
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(collection="messages")
public class Message {
	@Id
	private Long id;
	private Long creatorId;
	private String body;
	private Boolean isBot;
	private Boolean isFile;
	private LocalDateTime time;	
	private Integer type;

}

package com.chatbot.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This class manages action of account
 *
 */
@Getter
@Setter
@AllArgsConstructor
@Document(collection="account-action")
public class AccountAction {
	private Long accountId;
	private Long actionId;
}

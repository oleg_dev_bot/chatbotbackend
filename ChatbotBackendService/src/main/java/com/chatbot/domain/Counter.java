package com.chatbot.domain;

import java.time.LocalDateTime;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * This class manages general counter of identifiers
 *
 */
@Getter
@Setter
@Document
public class Counter {
	private String name;
	private Long sequence;
}

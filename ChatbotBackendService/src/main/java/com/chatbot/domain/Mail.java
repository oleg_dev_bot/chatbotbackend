package com.chatbot.domain;

import java.util.Map;

import lombok.Data;

/**
 * This class manages a mail
 *
 */
@Data
public class Mail {
    private String from;
    private String to;
    private String subject;
    private EmailType emailType;
    private Map<String, Object> model;

}

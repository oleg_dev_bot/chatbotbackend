package com.chatbot.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This class manages a group-broadcast table
 *
 */
@Getter
@Setter
@AllArgsConstructor
@Document(collection="group-broadcast")
public class GroupBroadcast {
	private Long groupId;
	private Long broadcastId;
}

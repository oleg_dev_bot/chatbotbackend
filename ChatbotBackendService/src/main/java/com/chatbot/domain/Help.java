package com.chatbot.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class manages the table help
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Help {
	@Id
	private Long id;
	private String title;	
	private String description;
}

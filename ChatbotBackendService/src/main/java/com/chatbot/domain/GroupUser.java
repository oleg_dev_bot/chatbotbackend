package com.chatbot.domain;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class manages the table group-user
 *
 */
@Getter
@Setter
@NoArgsConstructor
@Document(collection="group-user")
public class GroupUser {
	private Long groupId;
	private Long userId;
	
	public GroupUser(Long _groupId, Long _userId) {
		groupId = _groupId;
		userId = _userId;
	}
}

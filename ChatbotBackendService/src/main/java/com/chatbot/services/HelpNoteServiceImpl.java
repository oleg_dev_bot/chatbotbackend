package com.chatbot.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.domain.Department;
import com.chatbot.domain.HelpNote;
import com.chatbot.interfaces.HelpNoteService;
import com.chatbot.repositories.HelpNoteRepository;

/**
 * This class manages the help table
 *
 */
@Service
public class HelpNoteServiceImpl implements HelpNoteService {
	
	@Autowired
	private CounterServiceImpl counterService;
	
	@Autowired
	private HelpNoteRepository helpNoteRepository;

	// input: none
    // output: list of help notes
	// action: get all help notes on page load
	@Override
	public List<HelpNote> loadHelpNotes() {		
		return helpNoteRepository.findAll();
	}

	// input: HelpNote (JSON)
	// output: boolean (if help note saved successfully then return true)
	// action: save help note to DB
	@Override
	public Boolean saveOrUpdate(HelpNote helpNote) {
		boolean isSaved = true;
		
		if(helpNote.getId() <= 0) {
			helpNote.setId(counterService.getNextIdSequence());
		}
		
		HelpNote helpNoteOut = helpNoteRepository.save(helpNote);
		
		if(helpNoteOut == null) {
			isSaved = false;
		}
		
		return isSaved;
	}

	// input: identifier of help note
	// output: none
	// action: delete help note from the DB
	@Override
	public void delete(Long helpNoteId) {
		helpNoteRepository.delete(helpNoteId);
	}

}

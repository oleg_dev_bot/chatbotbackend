package com.chatbot.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.domain.AccountAlert;
import com.chatbot.domain.Alert;
import com.chatbot.repositories.AccountAlertRepository;
import com.chatbot.repositories.AlertRepository;

@Service
public class AlertService {
	@Autowired
	private AlertRepository alertRepository;
	@Autowired
	private AccountAlertRepository accountAlertRepository;
	@Autowired
	private CounterServiceImpl counterService;
	
	// input: alert from account (JSON) and identifier of account
	// output: none
	// action: save alert of account
	public void saveAlert(Long accountId, Alert alert) {
		
		if(alert.getId() <= 0) {
			alert.setId(counterService.getNextIdSequence());
		}
		
		AccountAlert accountAlert = new AccountAlert(accountId, alert.getId());
		
		AccountAlert saved = accountAlertRepository.save(accountAlert);
		
		if(saved != null) {
			alertRepository.save(alert);
		}
	}
}

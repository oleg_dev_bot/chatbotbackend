package com.chatbot.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.chatbot.domain.Counter;


/**
 *  This class manages the counter of entities
 *
 */
@Service
public class CounterServiceImpl {	
	@Autowired
	private MongoTemplate mongoTemplate;	
	
	private final String SEQUENCE_FOR_ID = "sequence_id";
	
	// input: none
	// output: id of sequence
	// action: get next id of sequence
	public long getNextIdSequence() {
        return increaseCounter(SEQUENCE_FOR_ID);
    }     
     
	// input: counter name
	// output: sequence
	// action: modify sequence
    private long increaseCounter(String counterName){
        Query query = new Query(Criteria.where("name").is(counterName));
        Update update = new Update().inc("sequence", 1);
        Counter counter = mongoTemplate.findAndModify(query, update, Counter.class);
        return counter.getSequence();
    }
	
	
}

package com.chatbot.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.domain.ChatbotScript;
import com.chatbot.domain.Dictionary;
import com.chatbot.domain.Script;
import com.chatbot.interfaces.ScriptService;
import com.chatbot.repositories.ChatbotScriptRepository;
import com.chatbot.repositories.DictionaryRepository;
import com.chatbot.repositories.ScriptDictionaryRepository;
import com.chatbot.repositories.ScriptRepository;

@Service
public class ScriptServiceImpl implements ScriptService {

	@Autowired
	private ScriptRepository scriptRepository;	
	@Autowired
	private ChatbotScriptRepository chatbotScriptRepository;
	@Autowired
	private ScriptDictionaryRepository scriptDictionaryRepository;
	@Autowired
	private DictionaryRepository dictionaryRepository;	
	@Autowired
	private CounterServiceImpl counterService;
	
	// input: script (JSON)
	// output: boolean (if script was saved successfully then true)
	// action: save script to DB
	@Override
	public Boolean save(Script _script) {
		
		if(_script.getId() <= 0) {
			_script.setId(counterService.getNextIdSequence());
		}
		
		Script script = scriptRepository.save(_script);
		
		if(script == null) {
			return false;
		}
		
		return true;
	}

	// input: none
	// output: list of scripts
	// action: get all scripts from DB
	@Override
	public List<Script> loadScripts() {
		return scriptRepository.findAll();
	}

	// input: identifier of chatbot
	// output: list of scripts
	// action: get all scripts of chatbot from DB
	@Override
	public List<Script> loadScriptsOfChatbot(Long chatbotId) {
		return chatbotScriptRepository.findByChatbotId(chatbotId)
			.stream().map(cs -> scriptRepository.findById(cs.getScriptId()))
			.collect(Collectors.toList());
	}

	// input: identifier of script
	// output: none
	// action: delete script from DB
	@Override
	public void delete(Long scriptId) {
		scriptRepository.delete(scriptId);
		chatbotScriptRepository.deleteByScriptId(scriptId);		
	}

	// input: identifier of chatbot and identifier of script
	// output: boolean (if saved successfully then true)
	// action: save script of chatbot
	@Override
	public Boolean saveChatbotScript(Long chatbotId, Long scriptId) {
		ChatbotScript cs = chatbotScriptRepository.findByChatbotIdAndScriptId(chatbotId, scriptId);
		
		if(cs != null) {
			return true;
		}
		
		ChatbotScript chatbotScript = new ChatbotScript(chatbotId, scriptId);
		ChatbotScript saved = chatbotScriptRepository.save(chatbotScript);
		
		if(saved == null) {
			return false;
		}
		
		return true;
	}

	// input: identifier of script
	// output: list of dictionary
	// action: get dictionaries of script
	@Override
	public List<Dictionary> loadDictionariesOfScript(Long scriptId) {
		return scriptDictionaryRepository.findByScriptId(scriptId)
				.stream().map(sd -> dictionaryRepository.findById(sd.getDictionaryId()))
				.collect(Collectors.toList());
	}

	// input: identifier of script
	// output: script
	// action: find script by id
	@Override
	public Script findById(Long scriptId) {
		return scriptRepository.findById(scriptId);
	}
}

package com.chatbot.services;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.domain.Chat;
import com.chatbot.domain.ChatMessage;
import com.chatbot.domain.ChatUser;
import com.chatbot.domain.ChatbotChat;
import com.chatbot.domain.ChatbotUser;
import com.chatbot.domain.Department;
import com.chatbot.domain.Group;
import com.chatbot.domain.GroupUser;
import com.chatbot.domain.Message;
import com.chatbot.domain.User;
import com.chatbot.domain.UserMessages;
import com.chatbot.repositories.ChatMessageRepository;
import com.chatbot.repositories.ChatRepository;
import com.chatbot.repositories.ChatUserRepository;
import com.chatbot.repositories.ChatbotChatRepository;
import com.chatbot.repositories.ChatbotRepository;
import com.chatbot.repositories.ChatbotUserRepository;
import com.chatbot.repositories.DepartmentAccountRepository;
import com.chatbot.repositories.DepartmentRepository;
import com.chatbot.repositories.GroupRepository;
import com.chatbot.repositories.GroupUserRepository;
import com.chatbot.repositories.MessagesRepository;
import com.chatbot.repositories.UserRepository;

import ch.qos.logback.core.net.SyslogOutputStream;
import lombok.Data;


/**
 *  This class manages analytics-performance tasks
 *
 */
@Service
public class AnalyticsPerformanceService {
	
	@Autowired
	private ChatRepository chatRepository;
	@Autowired
	private ChatbotChatRepository chatbotChatRepository;
	@Autowired
	private ChatbotRepository chatbotRepository;
	@Autowired
	private ChatbotUserRepository chatbotUserRepository;
	@Autowired
	private ChatUserRepository chatUserRepository;
	@Autowired
	private ChatMessageRepository chatMessageRepository;
	@Autowired
	private MessagesRepository messagesRepository;
	@Autowired
	private GroupRepository groupRepository;
	@Autowired
	private GroupUserRepository groupUserRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private DepartmentAccountRepository departmentAccountRepository;
	
	// input: identifier of chatbot
	// output: String = sent/received, Long = total messages
	// action: how many massages sent and how many recived to the bot  
	public Map<String, Long> howManyMessagesSentAndReceivedToTheBot(Long botId){
		
		List<Message> messages = messagesRepository.findByCreatorId(botId);
		
		Map<String, Long> result = messages.stream()
				.collect(Collectors.groupingBy(Message::getIsBot, Collectors.counting()))
				.entrySet()
				.stream()
				.map(m -> {
							 Map.Entry<String, Long> entry = null;
											
							 if(m.getKey()) {
								entry = new AbstractMap.SimpleEntry<String, Long>("sent", m.getValue());
							 }
							 else {
								entry = new AbstractMap.SimpleEntry<String, Long>("received", m.getValue());
						     }
											
							return entry;
				}).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		
		if(result.get("sent") == null){
			result.put("sent", 0L);
		}
		
		if(result.get("received") == null){
			result.put("received", 0L);
		}
										
		return result;
		
	}
	
	// input: none
	// output: identifier of chat, amount of messages 
	// action: get how many users send only 1 message   
	public Map<String, Long> howManyMessagesPerChat(){
		List<Chat> chats = chatRepository.findAll();
		List<ChatMessage> chatMessages = chatMessageRepository.findAll();
		List<Message> messages = messagesRepository.findAll();
		
		Map<Long, Long>  barMessagesPerChat = new HashMap<>();
		
		Long count = 0L;
		Long total = 0L;
		
		//search all chats
		for(Chat chat: chats) {			
			count = 0L;
			
			// search chat-message relations
			for(ChatMessage chatMessage: chatMessages) {						
				if(chatMessage.getChatId() == chat.getId()) {
					
					//search messages of chat
					for(Message message: messages) {								
						if(message.getId() == chatMessage.getMessageId()) {									
							count++;
						}
					}
				}
			}
			
			total += count;
			barMessagesPerChat.put(chat.getId(), count);
		}
		
		final long totalMessages = total;
		
		Map<String, Long> barMessagesPerChatWithPercent = barMessagesPerChat
				.entrySet()
				.stream()
				.map(m -> {
						     Map.Entry<String, Long> entry = null;
						     Double percent = (double) (m.getValue() * 100)/totalMessages;
						     String key = "id=" + m.getKey() + "; percent=" + String.format("%.2f", percent);
							 entry = new AbstractMap.SimpleEntry<String, Long>(key, m.getValue());										
																	
							 return entry;
				}).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)); 
		
		return barMessagesPerChatWithPercent;
	}
	
	// input: none
	// output: amount of users
	// action: get how many users send only 1 message 
	public Long countUsersOnly1Message() {
		List<UserMessages> userMessages = getMessagesOfUsers();

		return userMessages.stream().filter(um -> um.getMessages().size() == 1).count();
	}
	
	// input: none
	// output: group id, amount of users in each group
	// action: get how many users we have in each group 
	public Map<Long, Long> howManyUsersInEachGroup(){
		List<Group> groups = groupRepository.findAll();
		List<GroupUser> groupUsers = groupUserRepository.findAll();
		List<User> users = userRepository.findAll();
		
		Map<Long, Long>  barGroupUsers = new HashMap<>();
		
		Long count = 0L;
		Long total = 0L;
		
		// search groups
		for(Group group: groups) {			
			count = 0L;
			
			//search group-user
			for(GroupUser gu: groupUsers) {						
				if(gu.getGroupId() == group.getId()) {
					
					// search users
					for(User user: users) {								
						if(gu.getUserId() == user.getId()) {									
							count++;
						}
					}
				}
			}
			
			barGroupUsers.put(group.getId(), count);
		}
		
		return barGroupUsers;
	}
	
	// input: none
	// output: messages of user
	// action: get messages of user
	private List<UserMessages> getMessagesOfUsers(){
		List<ChatbotUser> chatbotUsers = chatbotUserRepository.findAll();
		List<ChatUser> chatUsers = chatUserRepository.findAll();
		List<ChatMessage> chatMessages = chatMessageRepository.findAll();
		List<Message> messages = messagesRepository.findAll();
		List<UserMessages> userMessages = new ArrayList<>();
		
		//search chatbot-user
		for(ChatbotUser chatbotUser: chatbotUsers) {
			UserMessages um = new UserMessages();
			um.setUid(chatbotUser.getUserId());
			
			for(ChatUser chatUser: chatUsers) {				
				if(chatUser.getUserId() == chatbotUser.getUserId()) {					
					for(ChatMessage chatMessage: chatMessages) {						
						if(chatMessage.getChatId() == chatUser.getChatId()) {							
							for(Message message: messages) {								
								if(message.getId() == chatMessage.getMessageId()) {									
									if(message.getType() == 0) {										
										um.getMessages().add(message);
									}
								}
							}
						}
					}
				}
			}
			
			userMessages.add(um);			
		}
		
		return userMessages;
	}
	
	// input: chatbotid
	// output: min to hit the target
	// action: get how much messages took to hit the target (minimal)  
	public Integer minToHitTheTarget(Long botId) {
		List<ChatbotChat> relationList = chatbotChatRepository.findByChatbotId(botId);
		List<Chat> chats = chatRepository.findAll();
		List<Chat> chatsOfChatbot = new ArrayList<>();
		
		for(ChatbotChat cc: relationList) {
			for(Chat chat: chats) {
				if(chat.getId() == cc.getChatId())
				{
					chatsOfChatbot.add(chat);
				}
			}			
		}
		
		
		return chatsOfChatbot.stream().map(chat -> chat.getMessagesToHit()).min(Integer::compare).get();
	}
	
	// input: chatbotid
	// output: max to hit the target
	// action: get how much messages took to hit the target (maximal)
	public Integer maxToHitTheTarget(Long botId) {
		List<ChatbotChat> relationList = chatbotChatRepository.findByChatbotId(botId);
		List<Chat> chats = chatRepository.findAll();
		List<Chat> chatsOfChatbot = new ArrayList<>();
		
		// cycle chatbot-chat
		for(ChatbotChat cc: relationList) {
			for(Chat chat: chats) {
				if(chat.getId() == cc.getChatId())
				{
					chatsOfChatbot.add(chat);
				}
			}			
		}
		
		return chatsOfChatbot.stream().map(chat -> chat.getMessagesToHit()).max(Integer::compare).get();
	}
	
	// input: chatbotid
	// output: AVG messages
	// action: get AVG messages number took to hit the target
	public Double AVGToHitTheTarget(Long botId) {
		List<ChatbotChat> relationList = chatbotChatRepository.findByChatbotId(botId);
		List<Chat> chats = chatRepository.findAll();
		List<Chat> chatsOfChatbot = new ArrayList<>();
		
		// cycle chatbot-chat
		for(ChatbotChat cc: relationList) {
			for(Chat chat: chats) {
				if(chat.getId() == cc.getChatId())
				{
					chatsOfChatbot.add(chat);
				}
			}			
		}
		
		return chatsOfChatbot.stream()
			    .mapToInt(chat -> chat.getMessagesToHit())
			    .average()
			    .orElse(Double.NaN);

	}
	
	// input: none
	// output: department's name, amount of accounts
	// action: get how many accounts we have in each department
	public Map<String, Integer> howManyAccountsInEachDepartment(){
		Map<String, Integer> distribMap = new HashMap<>();
		List<Department> departments = departmentRepository.findAll();
		
		departments.stream().forEach(department -> {
			distribMap.put(department.getName(), departmentAccountRepository.countByDepartmentId(department.getId()));
		});
		
		return distribMap;
	}
}
 

/*@Data
class UserMessages{
	private Long uid;
	private List<Message> messages;
	
	public UserMessages() {
		messages = new ArrayList<>();
	}	
}*/
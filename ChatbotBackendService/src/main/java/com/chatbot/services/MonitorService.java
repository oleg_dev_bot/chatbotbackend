package com.chatbot.services;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.chatbot.domain.Chat;
import com.chatbot.repositories.ChatRepository;

@Service
public class MonitorService {
	@Autowired
	public ChatRepository chatRepository;
	
	// input: none
	// output: ROI
	// action: get ROI percent in last day
	public Double getROI() {
		Long count = chatRepository.findByMessagesToHitGreaterThanAndStartChatTimeBetween(0, LocalDateTime.now().minusHours(24), LocalDateTime.now())
			.stream()
			.count();
		Long total = chatRepository.count();
		double ROI = (double) count/total;	
		
		return ROI;
	}
	
	/*public List<Chat> loadChatsByPage(Pageable pageable){
		List<Chat> chats = chatRepository.findAll(pageable).getContent();
	}*/
}

package com.chatbot.services;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.math3.util.Precision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.chatbot.domain.Account;
import com.chatbot.domain.AccountAction;
import com.chatbot.domain.AccountConnection;
import com.chatbot.domain.Action;
import com.chatbot.domain.Chat;
import com.chatbot.domain.ChatMessage;
import com.chatbot.domain.ChatUser;
import com.chatbot.domain.ChatbotChat;
import com.chatbot.domain.ChatbotUser;
import com.chatbot.domain.Dates;
import com.chatbot.domain.Message;
import com.chatbot.domain.User;
import com.chatbot.domain.UserChats;
import com.chatbot.repositories.ChatMessageRepository;
import com.chatbot.repositories.ChatRepository;
import com.chatbot.repositories.ChatUserRepository;
import com.chatbot.repositories.ChatbotChatRepository;
import com.chatbot.repositories.ChatbotUserRepository;
import com.chatbot.repositories.EngineRepository;
import com.chatbot.repositories.MessagesRepository;
import com.chatbot.repositories.UserRepository;

/**
 * This class manages analytics-marketing tasks
 *
 */
@Service
public class AnalyticsMarketingService {
	@Autowired
	public MessagesRepository messageRepository;
	@Autowired
	public ChatRepository chatRepository;
	@Autowired
	public ChatMessageRepository chatMessageRepository;
	@Autowired
	public ChatbotChatRepository chatbotChatRepository;
	@Autowired
	public ChatbotUserRepository chatBotUserRepository;
	@Autowired
	public UserRepository userRepository;
	@Autowired
	public EngineRepository engineRepository;
	@Autowired
	public ChatUserRepository chatUserRepository;
	
	// input: identifier of chatbot
	// output: total messages for chatbot
	// action: count total messages from the first day of the bot
	public Long getCountMessagesForChatBot(Long botId){
		//Supplier<Stream<Chat>> chatSupplier = () -> chatRepository.findByBotId(botId).stream();
		
		/*Long count = chatSupplier.get().map(chat -> chatMessageRepository.findByChatId(chat.getId()))
		.flatMap(cml -> cml.stream())
		.map(cm-> messageRepository.findById(cm.getMessageId()))
		.count();*/
		
		Supplier<Stream<ChatbotChat>> chatbotChatSupplier = () -> chatbotChatRepository
				.findByChatbotId(botId)
				.stream();
		
		List<ChatMessage> chatMessagesList = chatMessageRepository.findAll();
		List<Message> messages = messageRepository.findAll();
		
		return chatbotChatSupplier.get().map(cbc -> findByChatId(cbc.getChatId(), chatMessagesList))
				.flatMap(cml -> cml.stream())
				.map(cm-> findMessageById(cm.getMessageId(), messages))
				.count();
		
	}
	
	// input: identifier of chat, list of chats
	// output: list of ChatMessage
	// action: get all ChatMessage by chat's identifier
	private List<ChatMessage> findByChatId(Long chatId, List<ChatMessage> fullList){
		
		return fullList.stream()
				.filter(cm -> cm.getChatId() == chatId)
				.collect(Collectors.toList());
	}
	
	// input: identifier of message, list of all messages
	// output: message
	// action: get  by message's identifier
	private Message findMessageById(Long messageId, List<Message> fullMessagesList){
		return fullMessagesList.stream()
				.filter(m -> m.getId() == messageId)
				.findFirst().get();
	}
	
	// input: identifier of chatbot
	// output: total chats
	// action: count total chats from the first day of the bot
	public Long getCountChatsForChatBot(Long botId){
		return  chatbotChatRepository
				.findByChatbotId(botId)
				.stream()
				.count();//chatRepository.findByBotId(botId).stream().count();
	}
	
	// input: identifier of chatbot
	// output: AVG chats per user
	// action: count total chats per user from the first day of the bot
	public Double getAvgChatsPerUserForChatBot(Long botId) {
		Supplier<Stream<ChatbotUser>> chatBotUserSupplier = () -> chatBotUserRepository.findByChatbotId(botId).stream();
		Long count = chatBotUserSupplier.get().flatMap(cu -> chatUserRepository.findByUserId(cu.getUserId())
				.stream())
				.count(); //chatRepository.findByUserId(cu.getUserId()).stream()).count();				
		Long countUsersOfChatbot = chatBotUserSupplier.get().count();
		
		return Precision.round((double) count/countUsersOfChatbot, 2);
	}
	
	// input: identifier of chatbot
	// output: AVG messages per user
	// action: count total messages per user from the first day of the bot
	public Double getAvgMessagesPerUserForChatBot(Long botId) {
		Long countUsersOfChatbot = chatBotUserRepository.findByChatbotId(botId).stream().count();
		Long totalMessages = getCountMessagesForChatBot(botId);
		Double avg = (double) totalMessages/countUsersOfChatbot; 
		
		return Precision.round(avg, 2);
	}
	
	// input: identifier of chatbot
	// output: AVG chat time
	// action: count total chats time from the first day of the bot
	public Double getAvgChatTimeForChatBot(Long botId) {
		Double avg = 0.0;
		
		OptionalDouble optional = chatRepository.findByBotId(botId)
				.stream()
				.mapToLong(chat -> chat.getTimeOnChat())
				.average();
		
		if(optional.isPresent()) {
			avg = optional.getAsDouble();
		}
		
		return avg;
	}
	
	// input: identifier of chatbot
	// output: total users (talked with the bot)
	// action: count total user from the first day of the bot
	public Long getTotalUsersTalkedWithChatBot(Long botId) {
		Long total = chatBotUserRepository.findByChatbotId(botId)
			.stream().map(cbu -> userRepository.findById(cbu.getUserId()))
			.filter(user -> user.getIsOnline() == true).count();
		
		return total;
	}
	
	// input: none
	// output: Integer = day, Long = total chats
	// action: how many chats happen in each day of the week
	public Map<Integer, Long> getChatsHappenEachDayOfTheWeek() {
		Map<Integer, Long> chatsPerWeek = new HashMap<>();
		
		for (int i = 1; i <= 7; i++) {
			chatsPerWeek.put(i, 0L);
		}
		
		List<Chat> chats = chatRepository.findByStartChatTimeBetween(LocalDateTime.now().minusDays(7), LocalDateTime.now());
		
		chats.stream()
			.map(chat -> chat.getStartChatTime())
			.forEach(startDate -> barWeek(startDate, chatsPerWeek));
		
		return chatsPerWeek;
	}
	
	// input: none
	// output: Integer = hour, Long = total chats
	// action: how many chats happen in each time of the day
	public Map<Integer, Long> getChatsHappenEachTimeOfTheDay() {
		Map<Integer, Long> chatsPerDay = new HashMap<>();
		
		for (int i = 1; i <= 24; i++) {
			chatsPerDay.put(i, 0L);
		}
		
		List<Chat> chats = chatRepository.findByStartChatTimeBetween(LocalDateTime.now().minusHours(24), LocalDateTime.now());
		
		chats.stream()
			.map(chat -> chat.getStartChatTime())
			.forEach(startDate -> barDay(startDate, chatsPerDay));
		
		return chatsPerDay;
	}
	
	// input: startChatTime, Map (bar chart)
    // output: none
	// action: count startChatTime 
	private final void barWeek(LocalDateTime startTime, Map<Integer, Long> week){
		LocalDateTime currentDt = LocalDateTime.now();
		Long count = 0L;
		final int DAYS = 7;
		
		LocalDateTime temp = currentDt.minusDays(DAYS);
		
		int i = 1;
		
		// search day when the chat have been started
		while(i <= DAYS ) {
			if(startTime.compareTo(temp) >= 0 && startTime.compareTo(temp.plusDays(1)) <= 0){
				count = week.get(i);
				
				if(count == null) {					
					count = 0L;
				}
				
				count++;
				week.put(i, count);
				
				break;
			}			
			i++;
			temp = temp.plusDays(1);
		}
	}
	
	// input: startChatTime, Map (bar chart)
    // output: none
	// action: count startChatTime 
	private final void barDay(LocalDateTime startTime, Map<Integer, Long> day){
		LocalDateTime currentDt = LocalDateTime.now();		
		Long count = 0L;
		
		final int HOURS = 24;
		
		LocalDateTime temp = currentDt.minusHours(HOURS);
		
		int i = 1;
		
		// search hour when the chat have been started
		while(i <= HOURS ) {
			if(startTime.compareTo(temp) >= 0 && startTime.compareTo(temp.plusHours(1)) <= 0){
				count = day.get(i);
				
				if(count == null) {					
					count = 0L;
				}
				
				count++;
				day.put(i, count);
				
				break;				
			} else {
				day.put(i, 0L);
			}
			
			i++;
			temp = temp.plusHours(1);
		}
	}
	
	// input: identifier of chatbot
	// output: total error messages
	// the number of messages that the bot was not able to answer and open an 'human intervation" flag
	public Integer getTotalErrorMessagesForChatBot(Long botId){
		Supplier<Stream<Chat>> chatSupplier = () -> chatRepository.findByBotId(botId).stream();
		
		Integer errors = chatSupplier.get()
			.filter(chat -> chat.getMechanism() != 0 && chat.getMechanism() > 0 )
		    .map(chat -> engineRepository.findById(chat.getMechanism()))	
			.mapToInt(engine -> engine.getErrorCount())
			.sum();
		
		return errors;
	}
	
	// input: none
	// output: total chats with errors
	// action: count the number of chats with happend an error message inside of them
	public Double getTotalChatsWithErrorMessages(){
		Supplier<Stream<Chat>> chatSupplier = () -> chatRepository.findAll().stream();
		long amountOfChats = chatSupplier.get().count();
		
		Long chatsWithErrors = chatSupplier.get()
			.filter(chat -> chat.getMechanism() != 0 && chat.getMechanism() > 0 )
		    .map(chat -> engineRepository.findById(chat.getMechanism()))	
			.filter(engine -> engine.getErrorCount() > 0)
			.count();
		
		return Precision.round((double)(chatsWithErrors * 100)/amountOfChats, 2);
	}
	
	// input: none
	// output: AVG percent of error messages in a chat
	// action: count the percent of errors messages in a chat 
	public Double getChatsAVGErrorMessages(){
		Supplier<Stream<Chat>> chatSupplier = () -> chatRepository.findAll().stream();
		
		Integer errors = chatSupplier.get()
			.filter(chat -> chat.getMechanism() != 0 && chat.getMechanism() > 0 )
		    .map(chat -> engineRepository.findById(chat.getMechanism()))	
		    .mapToInt(engine -> engine.getErrorCount())
			.sum();
		
		Double avg = (double) errors/chatSupplier.get().count();
		
		Double percent = (avg * 100)/errors;
		
		return percent;
	}
	
	// input: identifier of chatbot, date and time
	// output: Integer = points (20) Long = total chats
	// action: get graph line how many chats over time (from the first day of the bot with time filter )
	public Map<Integer, Long> getUserActivity(Long botId, LocalDateTime filterDt) {
		Map<Integer, Long> graph = new HashMap<>();
		LocalDateTime startChatBot = chatRepository.findTop1ByBotIdOrderByStartChatTimeAsc(botId).getStartChatTime();
		Supplier<Stream<Chat>> chatSupplier = () -> chatRepository.findByBotId(botId).stream();
		
		final int POINTS = 20;
		
		Duration diff = Duration.between(startChatBot, filterDt);
		Duration step = diff.dividedBy(POINTS);
		
		chatSupplier.get().forEach(chat -> graph(chat.getStartChatTime(), graph, startChatBot, step));
		
		return graph;
	}
	
	// input: startChatTime, Map (bar chart), startInterval (period from startInterval tiil now), step (duration in the period)
    // output: none
	// action: count startChatTime 
	private final void graph(LocalDateTime startTime, Map<Integer, Long> graph, LocalDateTime startInterval, Duration step){
		LocalDateTime currentDt = startInterval;
		LocalDateTime temp = currentDt;
		Long count = 0L;
		Long tempCount = 0L;
		
		Long delta = step.toNanos();
		
		final int POINTS = 20;
		
		int i = 1;
		
		// search point when the chat have been started
		while(i <= POINTS) {
			
			if(startTime.compareTo(temp) >= 0 && startTime.compareTo(temp.plusNanos(delta)) <= 0)
			{
				count = graph.get(i);
				
				// initialize of count
				if(count == null) {
					count = 0L;					
				}
				
				count++;
				graph.put(i, count);
				
				break;							
			} else {
				graph.put(i, 0L);
			}
			
			temp = temp.plusNanos(delta);	
			i++;
		}
	}
	
	// input: none
	// output: Long = days, Long = retention
	// action: get table of the retention of the users in the last week
	public Map<Long, Long> getRetentionUsersForLastWeek() {
		List<ChatUser> chatUserList = chatUserRepository.findAll();
		List<Chat> chats = chatRepository.findByStartChatTimeBetween(LocalDateTime.now().minusDays(7), LocalDateTime.now());
		List<User> userList = userRepository.findAll();		
		List<UserChats> userChats = new ArrayList<>();
		
		Map<Long, Long> retentionMap = new HashMap<>();
		
		// cycle users
		for(User user: userList) {
			UserChats uc = new UserChats(user.getId());
			
			//cycle user-chat
			for(ChatUser cu: chatUserList) {
				
				// concrete user
				if(cu.getUserId() == user.getId()) {
					
					// chats of user
					for(Chat chat: chats) {
						
						//concrete chat
						if(chat.getId() == cu.getChatId()) {
							uc.getChats().add(chat);
						}
					}
				}
			}
			
			userChats.add(uc);
		}
		
		userChats.stream().forEach(uc -> getRetentionUser(uc, retentionMap));
		
		return retentionMap;
	}
	
	// input: none
	// output: String = name, Long = percent
	// action: get percent table (name, percent) of the locations of the users 
	public Map<String, Double> getUsersLocations() {
		List<User> users = userRepository.findAll();
		Map<String, Long> locationsMap = new HashMap<>();
		Map<String, Double> percentMap = new HashMap<>();
		
		users.stream().forEach(user -> pieDiagram(user.getLocation(), locationsMap));		
		Long total = users.stream().count();
		Long amount = 0L;
		Double percent = 0.0;
		
		// get percent for each location
		for (Map.Entry<String, Long> entry : locationsMap.entrySet()) {
	         amount = entry.getValue(); 
	         percent = Precision.round((double) (amount * 100)/total, 2);
	         //entry.setValue(percent);
	         percentMap.put(entry.getKey(), percent);
	    }
		
		return percentMap;
		
	}
	
	// input: String = type, Long = count items
	// output: none
	// action: get pie diagram 
	private final void pieDiagram(String type, Map<String, Long> map) {
		Long count = map.get(type);
		
		// initialize of count
		if(count == null) {
			count = 0L;
		}
		
		count++;
		map.put(type, count);
	}
	
	// input: chats of user, Map (bar)
	// output: String = name, Long = percent
	// action: get percent table (name, percent) of the locations of the users 
	private  void getRetentionUser(UserChats uc, Map<Long, Long> rm) {
		Comparator<Chat> comparator = (c1, c2) -> c1.getStartChatTime().compareTo(c2.getStartChatTime());
		List<Chat> lastChats = uc.getChats().stream().sorted(comparator.reversed()).limit(2).collect(Collectors.toList());
		
		// two last of chats
		if(lastChats.size() >= 2) {
			
			Duration diff = Duration.between(lastChats.get(1).getStartChatTime(), lastChats.get(0).getStartChatTime());
			Long delta = diff.toDays();
			
			// days between startChatTime
			if(delta >= 0) {
				// the same day
				if (delta == 0) {
					delta = 1L;
				}
				
				Long count = rm.get(delta);
				
				// initialize of count
				if(count == null) {
					count = 0L;
				}
				
				count++;
				rm.put(delta, count);
				
			} else {
				
				//not retention
				if (rm.get(1) == null) {
					rm.put(1L, 0L);
				}
			}
		}
		
	}
	
	// input: Integer = type(0 = days, 1 = weeks), amount (days or weeks, depend on type)
	// output: Integer = day/week, Long = summary timeOnChat
	// action: get graph line which present how long a over all chats have been taken in each day / week
	public Map<Integer, Long> getUserEngagement(Integer type, Integer amount) {
		Map<Integer, Long> graph = new HashMap<>();
		List<Chat> chats = chatRepository.findAll();
		Map<Integer, Dates> map = getDateMap(amount, type);
		
		chats.stream().forEach(chat -> graphUserEngagement(type, amount, chat.getTimeOnChat(), graph, chat.getStartChatTime(), map));
				
		return graph;
	}
	
	// input: type (day/weeks), amount (days/weeks depends on type), timeOnChat, Map (bar), startChatTime, intervals
	// output: none
	// action: get graph 
	private final void graphUserEngagement(Integer type, Integer amount,  Integer timeOnChat,  Map<Integer, Long> graph, LocalDateTime startChatTime, Map<Integer, Dates> map){
    	Long summary = 0L;
		
    	// search interval where chat has been started
		for (Map.Entry<Integer, Dates> entry : map.entrySet()) {
			
			// check interval
			if(startChatTime.compareTo(entry.getValue().getStartDt()) >= 0 && startChatTime.compareTo(entry.getValue().getFinishDt()) <= 0)
			{
				summary = graph.get(entry.getKey());
				
				//initialize count
				if(summary == null) {
					summary = 0L;					
				}
				
				summary += (long) timeOnChat;
				graph.put(entry.getKey(), summary);
				
				break;							
			} 
			else {
				
				// if chat has not been started then summary time on chat is set 0
				if(graph.get(entry.getKey()) == null) {
					graph.put(entry.getKey(), 0L);
				}
			}
	    }
	}
	
	// input: type = day/week, amount day/week
	// output: intervals
	// action: get intervals 
	public Map<Integer, Dates> getDateMap(Integer amount, Integer type) {
		Map<Integer, Dates> map = new HashMap<>();
		LocalDateTime startDt = null;
		LocalDateTime finishDt = null;
		Duration diff = null;
		Duration step = null;
		Long delta = 0L;
		
		// 0 - days from now, 1 - weeks from now
		switch(type) {
			case 0: startDt = LocalDateTime.now().minusDays(amount);
     					  break;
			case 1: startDt = LocalDateTime.now().minusWeeks(amount);
						  break;			  
		}
		
		diff = Duration.between(startDt, LocalDateTime.now());
		step = diff.dividedBy(amount);
		delta = step.toNanos();
		finishDt = startDt.plusNanos(delta);	
		
		// initialize intervals
		for(int i = 1; i <= amount; i++) {
			  Dates dates = new Dates();
			  dates.setStartDt(startDt);
			  dates.setFinishDt(finishDt);
			  
			  map.put(i, dates);
			  
			  startDt = finishDt;
			  finishDt = startDt.plusNanos(delta);
		  }
		
		return map;
	}
	
	
	public void setUp() {
	    /*List<ChatMessage> list = new ArrayList();
	    
		ChatMessage cm1 = new ChatMessage(3L, 3L);		
		list.add(cm1);
		
		ChatMessage cm2 = new ChatMessage(5L, 1L);		
		list.add(cm2);
		
		ChatMessage cm3 = new ChatMessage(5L, 2L);		
		list.add(cm3);
		
		chatMessageRepository.saveAll(list);*/
	}
}

package com.chatbot.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.domain.Chat;
import com.chatbot.domain.User;
import com.chatbot.repositories.ChatRepository;
import com.chatbot.repositories.EngineRepository;
import com.chatbot.repositories.UserRepository;


/**
 * This class manages analytics-user 
 *
 */
@Service
public class AnalyticsUserService {
	
	@Autowired
	public UserRepository userRepository;
	@Autowired
	public EngineRepository engineRepository;
	@Autowired
	public ChatRepository chatRepository;
	
	// input: none
	// output: gender (male/female), amount of each gender
	// action: how many male, female, and unknown
	public Map<String, Long> genderDistribution(){
		Map<String, Long> gender = new HashMap<>();
		Map<Integer, Long> temp = new HashMap<>();
		
		List<User> users = userRepository.findAll();		
		users.stream()
			.map(user -> user.getIsMale())
			.forEach(gndr -> pieDiagram(gndr, temp));
		
		// map gender
		for (Map.Entry<Integer, Long> entry : temp.entrySet()) {
			
			switch (entry.getKey()){
				case 0:
					gender.put("female", entry.getValue());
					break;
				case 1:
					gender.put("male", entry.getValue());
					break;
				default:
					gender.put("unknown", entry.getValue());
			}
	    }
		
		return gender;
	}
	
	// input: none
	// output: gender (male/female), amount of each gender
	// action: how many male, female, and unknown
	public Map<Integer, Long> ageDistribution(){
		Map<Integer, Long> ageMap = new HashMap<>();
		List<User> users = userRepository.findAll();
		
		users.stream()
			.map(user -> user.getAge())
			.forEach(age -> pieDiagram(age, ageMap));
		
		return ageMap;
	}
	
	// input: none
	// output: type of device, amount of each device
	// action: how many from each device [enum: computer, phone, tablet]
	public Map<Integer, Long> deviceDistribution(){
		Map<Integer, Long> deviceMap = new HashMap<>();
		List<Chat> chats = chatRepository.findByMechanismGreaterThan(0);
		
		chats.stream()
			.map(chat -> engineRepository.findById(chat.getMechanism()))
			.map(engine -> engine.getDevice())
			.forEach(device -> pieDiagram(device, deviceMap));
		
		return deviceMap;
	}
	
	// input: none
	// output: platform type, amount of each platform
	// action: how many from each platform [enum: 7 platforms we have]
	public Map<Integer, Long> platformDistribution(){
		Map<Integer, Long> platformMap = new HashMap<>();
		List<Chat> chats = chatRepository.findByMechanismGreaterThan(0);
				
		chats.stream()
			.map(chat -> engineRepository.findById(chat.getMechanism()))
			.map(engine -> engine.getPlatform())
			.forEach(platform -> pieDiagram(platform, platformMap));
		
		return platformMap;
	}
	
	// input: type (device, platform)
	// output: none
	// action: get map of selected type
	private final void pieDiagram(Integer type, Map<Integer, Long> map) {
		Long count = map.get(type);
		
		// initialize count
		if(count == null) {
			count = 0L;
		}
		
		count++;
		map.put(type, count);
	}
}

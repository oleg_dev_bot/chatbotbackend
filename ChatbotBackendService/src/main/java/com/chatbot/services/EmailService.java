package com.chatbot.services;

import org.springframework.mail.SimpleMailMessage;

import com.chatbot.domain.Mail;

public interface EmailService {
	void sendEmail(Mail mail);
}

package com.chatbot.services;

import java.nio.charset.StandardCharsets;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import com.chatbot.domain.EmailType;
import com.chatbot.domain.Mail;


/**
 * This class manages mail sending
 *
 */
@Service
public class EmailServiceImpl implements EmailService{
	@Autowired
    private JavaMailSender emailSender;
	@Autowired
    private SpringTemplateEngine templateEngine;

	// input: mail
    // output: none
	// action: send mail
	@Override
	public void sendEmail(Mail mail) {
		
		try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message,
                    MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());
            
            Context context = new Context();
            context.setVariables(mail.getModel());
            
            String template = "email-welcome-template";
            
            // check email type
            if(mail.getEmailType() == EmailType.RESET) {
            	template = "email-reset-template";
            }
            
            String html = templateEngine.process("email/" + template, context);            
           
            helper.setTo(mail.getTo());
            helper.setText(html, true);
            helper.setSubject(mail.getSubject());
            helper.setFrom(mail.getFrom());

            emailSender.send(message);
            
        } catch (Exception e){
            throw new RuntimeException(e);
        }
	}

}

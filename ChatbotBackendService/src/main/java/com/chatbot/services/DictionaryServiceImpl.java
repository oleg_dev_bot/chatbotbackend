package com.chatbot.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.domain.ChatbotDictionary;
import com.chatbot.domain.ChatbotScript;
import com.chatbot.domain.Dictionary;
import com.chatbot.domain.Script;
import com.chatbot.dto.ChatbotDictionariesDto;
import com.chatbot.dto.ScriptDictionariesDto;
import com.chatbot.interfaces.DictionaryService;
import com.chatbot.repositories.ChatbotDictionaryRepository;
import com.chatbot.repositories.ChatbotScriptRepository;
import com.chatbot.repositories.DictionaryRepository;
import com.chatbot.repositories.ScriptDictionaryRepository;


/**
 * This class manages dictionaries
 *
 */
@Service
public class DictionaryServiceImpl implements DictionaryService{
	@Autowired
	private CounterServiceImpl counterService;
	@Autowired
	private DictionaryRepository dictionaryRepository;
	@Autowired
	private ChatbotDictionaryRepository chatbotDictionaryRepository;
	@Autowired
	private ChatbotScriptRepository chatbotScriptRepository;
	@Autowired
	private ScriptDictionaryRepository scriptDictionaryRepository;

	// input: dictionary (JSON)
	// output: boolean (if dictionary was saved successfully then true)
	// action: save dictionary to DB
	@Override
	public Boolean save(Dictionary _dictionary) {
		
		if(_dictionary.getId() <= 0) {
			_dictionary.setId(counterService.getNextIdSequence());
		}
		
		Dictionary dictionary = dictionaryRepository.save(_dictionary);
		
		if(dictionary == null) {
			return false;
		}
		
		return true;
	}

	// input: identifier of chatbot
	// output: list of dictionaries
	// action: get all dictionaries of chatbot from DB
	@Override
	public List<Dictionary> loadDictionariesOfChatbot(Long chatbotId) {
		return chatbotDictionaryRepository.findByChatbotId(chatbotId)
				.stream().map(cd -> dictionaryRepository.findById(cd.getDictionaryId()))
				.collect(Collectors.toList());
	}

	// input: identifier of dictionary
	// output: none
	// action: delete dictionary
	@Override
	public void delete(Long dictionaryId) {
		dictionaryRepository.delete(dictionaryId);
		chatbotDictionaryRepository.deleteByDictionaryId(dictionaryId);
		scriptDictionaryRepository.deleteByDictionaryId(dictionaryId);
	}

	// input: identifier of chatbot and list of dictionaries id
	// output: none
	// action: save dictionaries to chatbot
	@Override
	public void assignDictionariesToChatbot(ChatbotDictionariesDto chatbotDictionaries) {
		chatbotDictionaries.getDictionaryIdList().stream().forEach(d -> {
			ChatbotDictionary cd = new ChatbotDictionary(chatbotDictionaries.getChatbotId(), d);
			chatbotDictionaryRepository.save(cd);
		});
	}

	// input: identifier of script and list of dictionaries id
	// output: none
	// action: save dictionaries to script
	@Override
	public void assignDictionariesToScript(ScriptDictionariesDto scriptDictionaries) {
		scriptDictionaries.getDictionaryIdList().stream().forEach(d -> {
			ChatbotScript cs = new ChatbotScript(scriptDictionaries.getScriptId(), d);
			chatbotScriptRepository.save(cs);
		});
	}

	// input: identifier of dictionary
	// output: dictionary
	// action: get dictionary from DB
	@Override
	public Dictionary loadDictionaryById(Long dictionaryId) {
		return dictionaryRepository.findById(dictionaryId);
	}
}

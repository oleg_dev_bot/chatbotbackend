package com.chatbot.services;

import java.lang.invoke.MethodHandles;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.joda.time.Interval;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.chatbot.domain.Alert;
import com.chatbot.domain.Chat;
import com.chatbot.domain.ChatMessage;
import com.chatbot.domain.ChatUser;
import com.chatbot.domain.ChatbotChat;
import com.chatbot.domain.ChatbotUser;
import com.chatbot.domain.Message;
import com.chatbot.domain.TypeTone;
import com.chatbot.domain.User;
import com.chatbot.domain.UserMessages;
import com.chatbot.domain.UserSummary;
import com.chatbot.repositories.ChatMessageRepository;
import com.chatbot.repositories.ChatRepository;
import com.chatbot.repositories.ChatUserRepository;
import com.chatbot.repositories.ChatbotChatRepository;
import com.chatbot.repositories.MessagesRepository;
import com.chatbot.repositories.UserRepository;
import com.ibm.watson.developer_cloud.tone_analyzer.v3.ToneAnalyzer;
import com.ibm.watson.developer_cloud.tone_analyzer.v3.model.DocumentAnalysis;
import com.ibm.watson.developer_cloud.tone_analyzer.v3.model.SentenceAnalysis;
import com.ibm.watson.developer_cloud.tone_analyzer.v3.model.ToneAnalysis;
import com.ibm.watson.developer_cloud.tone_analyzer.v3.model.ToneCategory;
import com.ibm.watson.developer_cloud.tone_analyzer.v3.model.ToneInput;
import com.ibm.watson.developer_cloud.tone_analyzer.v3.model.ToneOptions;
import com.ibm.watson.developer_cloud.tone_analyzer.v3.model.ToneScore;


/**
 * This service manages all functions of live chat
 *
 */
@Service
public class LiveChatService {
	@Autowired
	private ChatRepository chatRepository;
	@Autowired
	private ChatMessageRepository chatMessageRepository;
	@Autowired
	private MessagesRepository messagesRepository;
	@Autowired
	private ChatUserRepository chatUserRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ToneAnalyzer toneAnalyzer;
	@Autowired
	private ChatbotChatRepository chatbotChatRepository;
	@Autowired
	private UserService userService;
	@Autowired
	private AlertService alertService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LiveChatService.class);
	
	// input: identifier of chat
	// output: time on chat
	// action: get how much time a spesific online chat is running
	public Long timeOnChat(Long chatId){
		Chat chat = chatRepository.findByIdAndIsOpenIsTrue(chatId);
		Duration diff = null;
		Long timeOnline = 0L;
		
		//if chat isn't online then return -1 (not exist online)
		if(chat == null) {
			return -1L;
		}
		
		diff = Duration.between(chat.getStartChatTime(), LocalDateTime.now());
		timeOnline = diff.toMillis();
		
		return timeOnline;
	}
	
	// input: identifier of user
	// output: status of user
	// action: get the user's status from the DB and return it
	public Integer getUserStatus(Long userId) {
		User user = userRepository.findById(userId);
		
		//if user isn't exist then return -1 (not exist)
		if(user == null) {
			return -1;
		}
		
		return user.getStatus();
	}
	
	// input: identifier of user
	// output: AVG respone time
	// action: get how much time in AVG takes to the user to answer
	public Double AVGResponseTime(Long userId) {
		Map<Integer, List<Message>> messagesMap = new HashMap<>();
		Comparator<Message> comparator = (c1, c2) -> c1.getTime().compareTo(c2.getTime());
		List<Message> messages = getAllMessagesOfUser(userId).getMessages();
		messagesMap = messages.stream().sorted(comparator).collect(Collectors.groupingBy(Message::getType));
		
		List<Message> sentMessages = messagesMap.get(0);
		List<Message> receivedMessages = messagesMap.get(1);
		
		LocalDateTime sentTime = null;
		LocalDateTime receivedTime = null;
		Long count = 0L;
		long sum = 0L;
		
		//if sentMessages or received messages are not exist then return negative number (error)
		if(sentMessages == null || receivedMessages == null || sentMessages.size() <= 0 || receivedMessages.size() <= 0) {
			return -1.0;
		}
		
		//get sum of responses (response = receivedTime - sentTime
		for(int i = 0; i < sentMessages.size(); i++) {
			try {
				sentTime = sentMessages.get(i).getTime();
				Long sentChatId = chatMessageRepository.findByMessageId(sentMessages.get(i).getId()).getChatId();
				
				Long receivedChatId = 0L;
				Message recvmsg = null;
				
				for(Message received: receivedMessages) {
					receivedChatId = chatMessageRepository.findByMessageId(received.getId()).getChatId();
					
					if(receivedChatId == sentChatId) {
						recvmsg = received;
						receivedTime = received.getTime();						
						Duration diff = Duration.between(sentTime, receivedTime);
						
						//long sentMillis = sentTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
						//long receivedMillis = receivedTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
						
						long millis = diff.toMillis();
					
						
						if(millis > 0) {
							sum += millis;
						}
				
						break;
					}
				}
				
				receivedMessages.remove(recvmsg);
				
				count++;
			} catch(Exception ex) {
				LOGGER.error(ex.getMessage());
			}
		}
		
		Double avg = (double) sum/count;
		
		return avg;

	}
	
	// input: identifier of user
	// output: dominant emotion of user
	// action: get the main feeling from the user from the last message from Watson's tone analyser and present it
	public Map<String, Double> getDominantEmotionUser(Long userId) {
		String message = getLastMessageFromUser(userId);
		
		return getDominantEmotion(message);
	}
	
	// input: identifier of user
	// output: 5 dominant emotions of user (String = emotion, Double = score)
	// action: get the 5 main feeling from the user from the 5 last message from Watson's tone analyzer
	public List<Map<String, Double>> get5DominantEmotionsOfUser(Long userId) {
		List<Message> messages = getLast5MessagesFromUser(userId);
		List<Map<String, Double>> result = new ArrayList<>();
		
		//get dominant emotion for each message
		for(Message message: messages) {
			result.add(getDominantEmotion(message.getBody()));
		}
		
		return result;
	}
	
	// input: identifier of user
	// output: last message of user
	// action: get last message of user from the DB
	private String getLastMessageFromUser(Long userId){
		Comparator<Message> comparator = (c1, c2) -> c1.getTime().compareTo(c2.getTime());
		
		List<ChatUser> chatUsers = chatUserRepository.findByUserId(userId);
		List<ChatMessage> chatMessages = chatMessageRepository.findAll();
		List<Message> messages = messagesRepository.findAll();
		List<UserMessages> userMessages = new ArrayList<>();
		List<Message> messagesOfUser = new ArrayList<>();
		
		//get all messages of user
		for(ChatUser chatUser: chatUsers) {									
			for(ChatMessage chatMessage: chatMessages) {						
				if(chatMessage.getChatId() == chatUser.getChatId()) {							
					for(Message message: messages) {								
						if(message.getId() == chatMessage.getMessageId()) {									
							if(message.getType() == 0) {										
								messagesOfUser.add(message);
							}
						}
					}
				}
			}
		}
	
		Message message = messagesOfUser.stream().sorted(comparator.reversed()).limit(1).findFirst().orElse(null);

		
		return message.getBody();
	}
	
	// input: identifier of user
	// output: list of messages 
	// action: get messages of user from DB
	private List<Message> getLast5MessagesFromUser(Long userId){
		Comparator<Message> comparator = (c1, c2) -> c1.getTime().compareTo(c2.getTime());
		
		List<ChatUser> chatUsers = chatUserRepository.findByUserId(userId);
		List<ChatMessage> chatMessages = chatMessageRepository.findAll();
		List<Message> messages = messagesRepository.findAll();
		List<UserMessages> userMessages = new ArrayList<>();
		List<Message> messagesOfUser = new ArrayList<>();
			
		for(ChatUser chatUser: chatUsers) {									
			for(ChatMessage chatMessage: chatMessages) {						
				if(chatMessage.getChatId() == chatUser.getChatId()) {							
					for(Message message: messages) {								
						if(message.getId() == chatMessage.getMessageId()) {									
							if(message.getType() == 0) {										
								messagesOfUser.add(message);
							}
						}
					}
				}
			}
		}
	
		List<Message> messagesLast = messagesOfUser.stream().sorted(comparator.reversed()).limit(5).collect(Collectors.toList());

		
		return messagesLast;
	}
	
	// input: identifier of user
	// output: object contains user id and messages of  this user
	// action: get all messages of user
	private UserMessages getAllMessagesOfUser(Long userId){
		List<ChatUser> chatUsers = chatUserRepository.findByUserId(userId);
		List<ChatMessage> chatMessages = chatMessageRepository.findAll();
		List<Message> messages = messagesRepository.findAll();
		List<UserMessages> userMessages = new ArrayList<>();
		
		UserMessages um = new UserMessages();
		um.setUid(userId);
			
		//get chats of user
		for(ChatUser chatUser: chatUsers) {
			
			//get messages of chat
			for(ChatMessage chatMessage: chatMessages) {
				
				// if identifiers of chats in the tables is the same  then get messages
				if(chatMessage.getChatId() == chatUser.getChatId()) {
					
					//get messages
					for(Message message: messages) {
						
						//if identifiers of messages in different tables equal then add message to list
						if(message.getId() == chatMessage.getMessageId()) {																			
							um.getMessages().add(message);
						}
					}
				}
			}
		}
						
		
		return um;
	}
	
	// input: identifier of chat
	// output: amount of messages sent on chat
	// action: get how many messages a specific online chat is did
	public Long howManyMessagesSentToChat(Long chatId) {
		List<ChatMessage> chatMessages = chatMessageRepository.findByChatId(chatId);
		List<Message> messages = messagesRepository.findAll();
		
		Map<Long, Long>  barMessagesPerChat = new HashMap<>();		
		Long count = 0L;
		
		//cycle messages of chat
		for(ChatMessage chatMessage: chatMessages) {
			
			//cycle of messages
			for(Message message: messages) {
				
				//message was found
				if(message.getId() == chatMessage.getMessageId()) {
					//if(message.getType() == 1) {
						count++;
					//}
				}
			}
		}
		
		return count;
	}
	
	// input: identifier of user
	// output: dominant emotion of user
	// action: get the main feeling from the user from the last message from Watson's tone analyser and present it
	private Map<String, Double> getDominantEmotion(String message){
		Map<String, Double> emotionMap = new HashMap<>();
		
		ToneInput toneInput = new ToneInput.Builder().text(message).build();
		ToneOptions toneOptions = new ToneOptions.Builder().toneInput(toneInput).build();
		
		ToneAnalysis tone = toneAnalyzer.tone(toneOptions).execute();
		DocumentAnalysis documentAnalysis = tone.getDocumentTone();
		
		getTones(documentAnalysis.getTones(), emotionMap);
		
        return emotionMap;
	
	}
	
	// input: list of ToneScore and map for representing main emotion
	// output: none
	// action: get emotions of user and represent a dominant emotion
	private void getTones(List<ToneScore> toneScores, Map<String, Double> emotionMap) {
		double maxScore = 0.0;
		String dominantTone = "none";
		
		//if tone scores are exist then get dominant emotion
		if(toneScores != null && toneScores.size() > 0) {
			maxScore = toneScores.get(0).getScore();
			dominantTone = toneScores.get(0).getToneName();
			
			//cycle of tone scores
			for(ToneScore toneScore : toneScores){
				
				//if there is tone score greater then maxScore, set new maxScore
	            if(toneScore.getScore() > maxScore){	            	
	                maxScore = toneScore.getScore();
	                dominantTone = toneScore.getToneName();
	            }
	        }
		}
		
		emotionMap.put(dominantTone, maxScore);
	}
	
	// input: identifier of chatbot and identifier of chat
	// output: none
	// action: stops or starts the bot ability to answer an input
	public ResponseEntity giveAndTakeControlFromBot(Long chatbotId, Long chatId) {
				
		ChatbotChat cc = chatbotChatRepository.findByChatbotIdAndChatId(chatbotId, chatId);
		
		if(cc != null) {
			Chat chat = chatRepository.findById(chatId);
			
			if(chat != null) {
				chat.setIsHuman(!chat.getIsHuman());
				chatRepository.save(chat);
				return ResponseEntity.ok(chat.getIsHuman());
			}
		}
		
		return ResponseEntity.ok("NOT_FOUND");
	}
	
	// input: identifier of user
	// output: user's summary information
	// action: get the data that we have about an user - photo, status, name, summary, location, comment
	public UserSummary getUserSummary(Long userId) {
		User user = userService.getUserById(userId);
		UserSummary us = null;
		
		//if user is found then it is not null
		if(user != null) {
			ModelMapper modelMapper = new ModelMapper();
			us = modelMapper.map(user, UserSummary.class);
		}
		
		return us;
	}
	
	// input: alert from account (JSON) and identifier of account
	// output: none
	// action: save alert of account
	public void saveAlertOfAccount(Long accountId, Alert alert) {
		alertService.saveAlert(accountId, alert);
	}
}

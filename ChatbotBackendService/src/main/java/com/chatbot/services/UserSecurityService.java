package com.chatbot.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.chatbot.domain.Account;
import com.chatbot.domain.security.User;
import com.chatbot.repositories.AccountRepository;
import com.chatbot.repositories.UserRepository;


/**
 * This class manages user security
 *
 */
@Service
public class UserSecurityService implements UserDetailsService{
	private static final Logger LOG = LoggerFactory.getLogger(UserSecurityService.class);
	
	//@Autowired
	private AccountRepository accountRepository;
	

	public UserSecurityService(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}


	// input: email
    // output: spring security object (UserDetails)
	// action: load user by email
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Account account = accountRepository.findByEmail(email);
		
		if(account == null) {
			LOG.warn("Account {} not found", email);
			throw new UsernameNotFoundException("Account " + email + " not found");
		}
		
		return account;
	}
	
	// input: email
    // output: Account
	// action: load account by email
	public Account loadAccountByEmail(String email) {
		return accountRepository.findByEmail(email);
	}
	
	// input: Account
    // output: none 
	// action: update account
	public void updateAccount(Account account) {
		accountRepository.save(account);
	}
	
	// input: reset token
    // output: Account
	// action: load account by token
	public Account findUserByToken(String resetToken) {
		return accountRepository.findUserByToken(resetToken);
	}

}

package com.chatbot.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.domain.Broadcast;
import com.chatbot.domain.Department;
import com.chatbot.domain.Group;
import com.chatbot.domain.GroupBroadcast;
import com.chatbot.domain.Platform;
import com.chatbot.domain.Script;
import com.chatbot.dto.BroadcastDto;
import com.chatbot.dto.BroadcastFullInfoDto;
import com.chatbot.interfaces.BroadcastService;
import com.chatbot.repositories.BroadcastRepository;
import com.chatbot.repositories.DepartmentRepository;
import com.chatbot.repositories.GroupBroadcastRepository;
import com.chatbot.repositories.GroupRepository;
import com.chatbot.repositories.MessagesRepository;
import com.chatbot.repositories.ScriptRepository;

/**
 * This class manages broadcast table
 *
 */
@Service
public class BroadcastServiceImpl implements BroadcastService{
	@Autowired
	private BroadcastRepository broadcastRepository;
	@Autowired
	private GroupBroadcastRepository groupBroadcastRepository;
	@Autowired
	private GroupRepository groupRepository;
	@Autowired
	private ScriptRepository scriptRepository;
	@Autowired
	private MessagesRepository messageRepository;
	
	@Autowired
	private CounterServiceImpl counterService;

	// input: none
    // output: list of broadcasts
	// action: get broadcasts on page load
	@Override
	public List<Broadcast> loadBroadcasts() {
		return broadcastRepository.findAll();
	}

	// input: Broadcast (JSON)
	// output: boolean (if broadcast saved successfully then return true)
	// action: save broadcast to DB
	@Override
	public Boolean saveOrUpdate(Broadcast broadcast) {
		boolean isSaved = true;
		
		if(broadcast.getId() <= 0) {
			broadcast.setId(counterService.getNextIdSequence());
		}
		
		Broadcast broadcastOut = broadcastRepository.save(broadcast);
		
		if(broadcastOut == null) {
			isSaved = false;
		}
		
		return isSaved;
	}

	// input: identifier of broadcast
	// output: none
	// action: delete broadcast from the DB
	@Override
	public void delete(Long broadcastId) {
		broadcastRepository.delete(broadcastId);		
	}

	// input: identifier of broadcast
	// output: broadcast's name
	// action: get broadcast's name by id
	@Override
	public String getBroadcastNameById(Long broadcastId) {
		return broadcastRepository.findById(broadcastId).getName();
	}

	// input: identifier of broadcast
	// output: broadcast's status
	// action: get broadcast's status by id
	@Override
	public Integer getBroadcastStatus(Long broadcastId) {
		return broadcastRepository.findById(broadcastId).getStatus();
	}	
	
	// input: identifier of broadcast
	// output: broadcast's group
	// action: get broadcast's group
	@Override
	public Group getBroadcastGroup(Long relatedGroupId){
		return groupRepository.findById(relatedGroupId);
	}

	// input: identifier of broadcast
	// output: amount of sent messages
	// action: get broadcast's sent messages
	@Override
	public Integer HowManyMessagesSent(Long broadcastId) {
		return broadcastRepository.findById(broadcastId).getMessageSendCount();
	}

	// input: identifier of broadcast
	// output: amount of opend chats
	// action: get opend chats
	@Override
	public Integer HowManyChatsOpend(Long broadcastId) {
		return broadcastRepository.findById(broadcastId).getMessageOpendCount();
	}

	// input: identifier of broadcast
	// output: broadcast's script id + name (Long = id, String = name)
	// action: get broadcast's script id and name
	@Override
	public Map<Long, String> getBroadcastScriptName(Long broadcastId) {
		Map<Long, String> map = new HashMap<>();
		Broadcast broadcast = broadcastRepository.findById(broadcastId);
		Long scriptId = broadcast.getScriptId();
		Script script = scriptRepository.findById(scriptId);
		map.put(scriptId, script.getName());
		
		return map;
	}

	// input: identifier of broadcast
	// output: broadcast's platform name
	// action: get which platform broadcast uses
	@Override
	public String getBroadcastPlatform(Long broadcastId) {
		return Platform.forCode(broadcastRepository
				.findById(broadcastId)
				.getPlatform()).name();
	}

	// input: metadata of broadcast
	// output: broadcast
	// action: save broadcast's metadata to DB
	@Override
	public Broadcast saveBroadcastMetadata(BroadcastDto broadcastMetadata) {
		ModelMapper modelMapper = new ModelMapper();
		Broadcast broadcast = modelMapper.map(broadcastMetadata, Broadcast.class);
		
		if(broadcastMetadata.getId() <= 0) {
			broadcast.setId(counterService.getNextIdSequence());
		}
		
		Broadcast broadcastOut = broadcastRepository.save(broadcast);
		
		if(broadcastOut != null) {
			groupBroadcastRepository.deleteByBroadcastId(broadcastOut.getId());
			GroupBroadcast groupBroadcast = new GroupBroadcast(broadcastMetadata.getRelatedGroupId(), broadcastOut.getId());
			groupBroadcastRepository.save(groupBroadcast);
		}
		
		return broadcastOut;
	}

	// input: identifier of broadcast
	// output: broadcast
	// action: load broadcast from DB
	@Override
	public Broadcast loadBroadcastById(Long broadcastId) {
		return broadcastRepository.findById(broadcastId);
	}
	
	// input: identifier of broadcast
	// output: broadcast's full info
	// action: load full info of broadcast from DB
	@Override
	public BroadcastFullInfoDto loadFullInfoAboutBroadcastById(Long broadcastId) {
		Broadcast broadcast = broadcastRepository.findById(broadcastId);
		
		ModelMapper modelMapper = new ModelMapper();		
		BroadcastFullInfoDto bfoadcastFullInfo = modelMapper.map(broadcast, BroadcastFullInfoDto.class);
		bfoadcastFullInfo.setScript(scriptRepository.findById(broadcast.getScriptId()));
		bfoadcastFullInfo.setGroup(groupRepository.findById(broadcast.getRelatedGroupId()));
		bfoadcastFullInfo.setMessage(messageRepository.findById(broadcast.getMessageId()));
		
		return bfoadcastFullInfo;
	}
}

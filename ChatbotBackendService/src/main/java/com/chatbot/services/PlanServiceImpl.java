package com.chatbot.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.domain.Chatbot;
import com.chatbot.domain.Plan;
import com.chatbot.interfaces.PlanService;
import com.chatbot.repositories.ChatbotRepository;
import com.chatbot.repositories.PlanRepository;

/**
 * This class manages plans
 *
 */
@Service
public class PlanServiceImpl implements PlanService{
	@Autowired
	private PlanRepository planRepository;
	@Autowired
	private ChatbotServiceImpl chatbotService;
	
	// input: none
	// output: plans
	// action: load all plans
	@Override
	public List<Plan> loadPlans() {
		return planRepository.findAll();
	}

	// input: identifier of chatbot and identifier of plan
	// output: none
	// action: assign plan to chatbot
	@Override
	public void assignPlanToBot(Long botId, Long planId) {
		Chatbot chatbot = chatbotService.findById(botId);
		chatbot.setPlan(planId);
	}

	// input: identifier of chatbot 
	// output: money spend
	// action: get how much the bot spend
	@Override
	public Double chatbotSpend(Long botId) {
		return chatbotService.findById(botId).getTotalPayment();
	}

}

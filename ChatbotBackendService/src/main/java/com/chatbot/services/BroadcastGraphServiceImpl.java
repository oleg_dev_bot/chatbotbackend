package com.chatbot.services;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.domain.Broadcast;
import com.chatbot.domain.BroadcastGraph;
import com.chatbot.interfaces.BroadcastGraphService;
import com.chatbot.repositories.BroadcastGraphRepository;
import com.chatbot.repositories.BroadcastRepository;


/**
 * This class manages broadcast functions which returns graph
 *
 */
@Service
public class BroadcastGraphServiceImpl implements BroadcastGraphService {
	@Autowired
	private BroadcastRepository broadcastRepository;
	
	@Autowired
	private BroadcastGraphRepository broadcastGraphRepository;
	
	// input: identifier of broadcast and date and time filter
	// output: broadcast open graph
	// action: get how much opend in percent
	@Override
	public Map<Integer, Double> getBroadcastOpenGraph(Long broadcastId, LocalDateTime filter) {
		Map<Integer, Double> graphMap = new HashMap<>();
		Broadcast broadcast = broadcastRepository.findById(broadcastId);
		int messageOpendCount = broadcast.getMessageOpendCount();
		
		Duration duration = Duration.between(broadcast.getStartDay(), filter);
		long hours = duration.toHours();
		
		if(hours > 0) {
			List<BroadcastGraph> bgTempList =  broadcastGraphRepository.findByBroadcastIdOrderByTimeStampAsc(broadcastId);
			List<BroadcastGraph> graph = bgTempList.stream().limit(hours).collect(Collectors.toList());
			
			graph.stream().forEach(bg -> {
				graphMap.put(bg.getTimeStamp(), (double) (bg.getMessageOpendCount() * 100)/messageOpendCount);
			});
					
		}
		
		return graphMap;
	}
	
	// input: identifier of broadcast and date and time filter
	// output: broadcast sent graph
	// action: get how much messages sent in percent
	@Override
	public Map<Integer, Double> getBroadcastSentGraph(Long broadcastId, LocalDateTime filter) {
		Map<Integer, Double> graphMap = new HashMap<>();
		Broadcast broadcast = broadcastRepository.findById(broadcastId);
		int messageSentCount = broadcast.getMessageSendCount();
			
		Duration duration = Duration.between(broadcast.getStartDay(), filter);
		long hours = duration.toHours();
			
		if(hours > 0) {
			List<BroadcastGraph> bgTempList =  broadcastGraphRepository.findByBroadcastIdOrderByTimeStampAsc(broadcastId);
			List<BroadcastGraph> graph = bgTempList.stream().limit(hours).collect(Collectors.toList());
				
			graph.stream().forEach(bg -> {
				graphMap.put(bg.getTimeStamp(), (double) (bg.getMessageSendCount() * 100)/messageSentCount);
			});
						
		}
			
			return graphMap;
	}


	// input: identifier of broadcast and date and time filter
	// output: broadcast device pie graph
	// action: get percent and amount messages from devices
	@Override
	public Map<String, String> getBroadcastDeviceGraph(Long broadcastId, LocalDateTime filter) {
		Map<String, String> broadcastDeviceMap = new HashMap<>();
		List<BroadcastGraph> bgTempList =  broadcastGraphRepository.findByBroadcastIdOrderByTimeStampAsc(broadcastId);
		
		Broadcast broadcast = broadcastRepository.findById(broadcastId);
		int messageOpendCount = broadcast.getMessageOpendCount();
		long hours = 0;
		
		if(filter == null) {
			hours = bgTempList.stream().count();
		} else {
			Duration duration = Duration.between(broadcast.getStartDay(), filter);
		    hours = duration.toHours();
		}		
		
		if(hours > 0) {			
			List<BroadcastGraph> graph = bgTempList.stream().limit(hours).collect(Collectors.toList());
			
			long total = 0;
			int phoneCount = 0;
			int computerCount = 0;
			int tabletCount = 0;

			for(BroadcastGraph bg: graph) {
				phoneCount += bg.getPhoneCount();
				computerCount += bg.getComputerCount();
				tabletCount += bg.getTabletCount();
			}
			
			total = phoneCount + computerCount + tabletCount;
			
			broadcastDeviceMap.put("phone", getPercent(phoneCount, total));
			broadcastDeviceMap.put("computer", getPercent(computerCount, total));
			broadcastDeviceMap.put("tablet", getPercent(computerCount, total));
		}
		
		return broadcastDeviceMap;
	}
	
	// input: amount messages from concrete device, 
	// output: string with amount messages and percent
	// action: get amount messages from device and percent
	private String getPercent(int value, long total) {
		return "count=" + value + "; percent=" + (double) (value * 100)/total;
	}

	// input: identifier of broadcast and date and time filter
	// output: how many chats hit "end"
	// action: get how many chats hit "end"
	@Override
	public Map<Integer, Integer> getBroadcastConvertGraph(Long broadcastId, LocalDateTime filter) {
		Map<Integer, Integer> broadcastConverthMap = new HashMap<>();
		Broadcast broadcast = broadcastRepository.findById(broadcastId);
		
		Duration duration = Duration.between(broadcast.getStartDay(), filter);
		long hours = duration.toHours();
		
		if(hours > 0) {
			List<BroadcastGraph> bgTempList =  broadcastGraphRepository.findByBroadcastIdOrderByTimeStampAsc(broadcastId);
			List<BroadcastGraph> graph = bgTempList.stream().limit(hours).collect(Collectors.toList());
			
			graph.stream().forEach(bg -> {
				broadcastConverthMap.put(bg.getTimeStamp(), bg.getConvertCount());
			});
		}
		
		return broadcastConverthMap;
	}
}

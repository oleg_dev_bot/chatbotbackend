package com.chatbot.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.domain.Account;
import com.chatbot.domain.Answer;
import com.chatbot.domain.Department;
import com.chatbot.domain.DepartmentScript;
import com.chatbot.domain.Group;
import com.chatbot.domain.GroupUser;
import com.chatbot.domain.User;
import com.chatbot.interfaces.GroupService;
import com.chatbot.repositories.GroupRepository;
import com.chatbot.repositories.GroupUserRepository;
import com.chatbot.repositories.UserRepository;

@Service
public class GroupServiceImpl implements GroupService{
	@Autowired
	private GroupRepository groupRepository;
	@Autowired
	private GroupUserRepository groupUserRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CounterServiceImpl counterService;
	@Autowired
	private AccountService accountService;

	// input: none
    // output: list of groups
	// action: get all groups on page load
	@Override
	public List<Group> loadGroups() {
		return groupRepository.findAll();
	}

	
	// input: Group (JSON)
	// output: boolean (if group saved successfully then return true)
	// action: save group to DB
	@Override
	public Boolean saveOrUpdate(Group group) {
		boolean isSaved = true;
		
		// check group id
		if(group.getId() <= 0) {
			group.setId(counterService.getNextIdSequence());
		}
		
		Group groupOut = groupRepository.save(group);
		
		// check saved object
		if(groupOut == null) {
			isSaved = false;
		}
		
		return isSaved;
	}

	// input: identifier of group
	// output: none
	// action: delete group from the DB
	@Override
	public void delete(Long groupId) {
		groupRepository.delete(groupId);
	}

	// input: identifier of group, identifiers of users
	// output: boolean (if relation "group-user" saved successfully then return true)
	// action: add users to group
	@Override
	public Boolean addUsersToGroup(Long groupId, List<Long> usersId) {
				
		//groupUserRepository.deleteByGroupId(groupId);
				
		List<GroupUser> guList = new ArrayList<>();
				
		usersId.stream().forEach(id -> {
			GroupUser groupUser = groupUserRepository.findByGroupIdAndUserId(groupId, id);
			
			if(groupUser == null) {
				GroupUser gu = new GroupUser(groupId, id);
				guList.add(gu);
			}
		});
				
		List<GroupUser> tempList = groupUserRepository.save(guList);
				
		if(tempList == null || tempList.size() <= 0) {
				return false;
		}
				
		return true;
	}
	
	// input: identifier of group, identifiers of users
	// output: Answer (if users removed successfully then isSuccess true)
	// action: remove users from group
	@Override
	public Answer removeUsersFromGroup(Long groupId, List<Long> usersId) {
		Answer answer = new Answer();
									
		List<GroupUser> guList = new ArrayList<>();
					
		usersId.stream().forEach(id -> {
			groupUserRepository.deleteByGroupIdAndUserId(groupId, id);
			
		});
		
		answer.setSuccess(true);
		
		return answer;
	}
	
	// input: identifier of group, identifiers of users
	// output: Answer (if users removed successfully then isSuccess true)
	// action: remove users from group
	@Override
	public List<User> getUsersOfGroup(Long groupId) {
		Answer answer = new Answer();
		
		List<GroupUser> guList = groupUserRepository.findByGroupId(groupId);
										
		List<User> users = new ArrayList<>();
						
		users = guList.stream().map(gu -> {
			Long userId = gu.getUserId();
			return userRepository.findById(userId);
		}).collect(Collectors.toList());
			
		
		return users;
	}

	// input: identifier of group
	// output: account
	// action: get account by group's id
	@Override
	public Account getAccountInfoByGroupId(Long groupId) {
		Group group = groupRepository.findById(groupId);
		
		return accountService.findById(group.getManagerId());
	}

	// input: identifier of group
	// output: group
	// action: get info about group by id
	@Override
	public Group findById(Long groupId) {
		return groupRepository.findById(groupId);
	}
	
	
}

package com.chatbot.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.chatbot.domain.Broadcast;
import com.chatbot.domain.BroadcastMessage;
import com.chatbot.domain.Message;
import com.chatbot.dto.BroadcastMessageDto;
import com.chatbot.repositories.BroadcastMessageRepository;
import com.chatbot.repositories.BroadcastRepository;
import com.chatbot.repositories.MessagesRepository;

@Service
public class MessageService {
    @Autowired
	private CounterServiceImpl counterService;
    @Autowired
    private MessagesRepository messageRepository;
    @Autowired
    private BroadcastMessageRepository broadcastMessageRepository;
    @Autowired
    private BroadcastRepository broadcastRepository;
    
    
    // input: message from account
 	// output: none
 	// action: when sending message the account mark if specific or not and save it to DB
    public Message save(Message message) {
    	
    	//if message is new (id = 0) then set identifier
    	if(message.getId() <= 0) {
    		message.setId(counterService.getNextIdSequence());
    	}
    	
    	return messageRepository.save(message);
    }
    
    // input: identifier of broadcast and message
  	// output: boolean (true if save successfully)
  	// action: add message to the broadcast
    public void saveToBroadcast(BroadcastMessageDto broadcastMessage) {

    	Message savedMessage = save(broadcastMessage.getMessage());
    	
    	if(savedMessage != null) {
    		
    		broadcastMessageRepository.deleteByMessageIdAndBroadcastId(savedMessage.getId(), broadcastMessage.getBroadcastId());
    		
    		BroadcastMessage bm = new BroadcastMessage(broadcastMessage.getBroadcastId(), savedMessage.getId());
    		
    		broadcastMessageRepository.save(bm);
    		
    		Broadcast broadcast = broadcastRepository.findById(broadcastMessage.getBroadcastId());
    		
    		if(broadcast != null) {
    			broadcast.setMessageId(savedMessage.getId());
    			broadcastRepository.save(broadcast);
    		}
    	}
    }
}

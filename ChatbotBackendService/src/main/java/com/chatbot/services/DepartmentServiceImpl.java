package com.chatbot.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.domain.Account;
import com.chatbot.domain.Answer;
import com.chatbot.domain.Department;
import com.chatbot.domain.DepartmentAccount;
import com.chatbot.domain.DepartmentScript;
import com.chatbot.domain.Group;
import com.chatbot.domain.Script;
import com.chatbot.interfaces.DepartmentService;
import com.chatbot.repositories.AccountRepository;
import com.chatbot.repositories.DepartmentAccountRepository;
import com.chatbot.repositories.DepartmentRepository;
import com.chatbot.repositories.DepartmentScriptRepository;
import com.chatbot.repositories.GroupRepository;


/**
 * This class manages the departments table
 *
 */
@Service
public class DepartmentServiceImpl implements DepartmentService {
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private DepartmentAccountRepository departmentAccountRepository;
	@Autowired
	private DepartmentScriptRepository departmentScriptRepository;
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private CounterServiceImpl counterService;
	@Autowired
	private AccountService accountService;
	@Autowired
	private ScriptServiceImpl scriptService;
	
	// input: none
    // output: list of departments
	// action: get all departments on page load
	@Override
	public List<Department> loadDepartments() {
		return departmentRepository.findAll();
	}

	// input: Department (JSON)
	// output: boolean (if department saved successfully then return true)
	// action: save department to DB
	@Override
	public Boolean saveOrUpdate(Department department) {
		boolean isSaved = true;
		
		if(department.getId() <= 0) {
			department.setId(counterService.getNextIdSequence());
		}
		
		Department depOut = departmentRepository.save(department);
		
		if(depOut == null) {
			isSaved = false;
		}
		
		return isSaved;
	}

	// input: identifier of department
	// output: none
	// action: delete department from the DB
	@Override
	public void delete(Long departmentId) {
		departmentRepository.delete(departmentId);
		departmentAccountRepository.deleteByDepartmentId(departmentId);
	}

	// input: identifier of department, identifiers of accounts
	// output: boolean (if relation "department-account" saved successfully then return true)
	// action: add accounts to department
	@Override
	public Boolean addAccountsToDepartment(Long departmentId, List<Long> accountsId) {
		
		//departmentAccountRepository.deleteByDepartmentId(departmentId);
		
		List<DepartmentAccount> daList = new ArrayList<>();
		
		accountsId.stream().forEach(id -> {
			if(departmentAccountRepository.findByDepartmentIdAndAccountId(departmentId, id) == null) {
				DepartmentAccount da = new DepartmentAccount(departmentId, id);
				daList.add(da);
			}
		});
		
		List<DepartmentAccount> tempList = departmentAccountRepository.save(daList);
		
		if(tempList == null || tempList.size() <= 0) {
			return false;
		}
		
		return true;
	}
	
	// input: identifier of department, identifiers of accounts
	// output: boolean (if relation "department-account" deleted successfully then return true)
	// action: remove accounts from department
	@Override
	public Answer removeAccountsFromDepartment(Long departmentId, List<Long> accountsId) {
		Answer answer = new Answer();
		
		accountsId.stream().forEach(id -> {
			departmentAccountRepository.deleteByDepartmentIdAndAccountId(departmentId, id);
		});			
		
		answer.setSuccess(true);
		
		return answer;
	}
	
	// input: identifier of department, identifiers of scripts
	// output: boolean (if relation "department-script" saved successfully then return true)
	// action: add scripts to department
	@Override
	public Boolean addScriptsToDepartment(Long departmentId, List<Long> scriptsId) {
			
		//departmentScriptRepository.deleteByDepartmentId(departmentId);
			
		List<DepartmentScript> dsList = new ArrayList<>();
			
		scriptsId.stream().forEach(id -> {
			System.out.println("SCRIPI ID: " + id);
						
			if(departmentScriptRepository.findByDepartmentIdAndScriptId(departmentId, id) == null) {
				DepartmentScript ds = new DepartmentScript(departmentId, id);
				dsList.add(ds);
			}
		});
			
		List<DepartmentScript> tempList = departmentScriptRepository.save(dsList);
			
		if(tempList == null || tempList.size() <= 0) {
				return false;
		}
			
		return true;
	}
	
	// input: identifier of department, identifiers of accounts
	// output: answer (if relation "department-script" deleted successfully then return isSuccess true)
	// action: remove scripts from department
	@Override
	public Answer removeScriptsFromDepartment(Long departmentId, List<Long> scriptsId) {
		Answer answer = new Answer();
			
		scriptsId.stream().forEach(id -> {
			departmentScriptRepository.deleteByDepartmentIdAndScriptId(departmentId, id);
		});			
			
		answer.setSuccess(true);
			
		return answer;
	}

	// input: identifier of department
	// output: department
	// action: get department by id
	@Override
	public Department findById(Long departmentId) {
		return departmentRepository.findById(departmentId);
	}
	
	// input: identifier of department
	// output: account
	// action: get account by department's id
	@Override
	public Account getAccountInfoByDepartmentId(Long departmentId) {
		Department department = departmentRepository.findById(departmentId);
			
		return accountService.findById(department.getOwnerId());
	}

	// input: identifier of department
	// output: list of account
	// action: get accounts of department
	@Override
	public List<Account> getAccountsOfDepartment(Long departmentId) {
		List<DepartmentAccount> daList = departmentAccountRepository.findByDepartmentId(departmentId);
		
		return daList.stream()
				.map(da -> accountRepository.findById(da.getAccountId()))
				.collect(Collectors.toList());
		
	}

	// input: identifier of department
	// output: list of scripts
	// action: get scripts of department
	@Override
	public List<Script> getScriptsOfDepartment(Long departmentId) {
		List<DepartmentScript> dsList = departmentScriptRepository.findByDepartmentId(departmentId);
		
		return dsList.stream()
				.map(ds -> scriptService.findById(ds.getScriptId()))
				.collect(Collectors.toList());
	}

}

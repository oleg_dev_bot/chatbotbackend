package com.chatbot.services;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.stereotype.Service;

import com.chatbot.config.SecurityUtility;
import com.chatbot.domain.Account;
import com.chatbot.domain.AccountAction;
import com.chatbot.domain.AccountConnection;
import com.chatbot.domain.AccountSettings;
import com.chatbot.domain.Action;
import com.chatbot.domain.Chat;
import com.chatbot.domain.ChatbotAccount;
import com.chatbot.domain.ChatbotChat;
import com.chatbot.domain.Department;
import com.chatbot.domain.DepartmentAccount;
import com.chatbot.domain.Message;
import com.chatbot.domain.Status;
import com.chatbot.domain.UserSummary;
import com.chatbot.dto.ActionDto;
import com.chatbot.dto.DepartmentDto;
import com.chatbot.repositories.AccountActionRepository;
import com.chatbot.repositories.AccountConnectionsRepository;
import com.chatbot.repositories.AccountRepository;
import com.chatbot.repositories.AccountSettingsRepository;
import com.chatbot.repositories.ActionRepository;
import com.chatbot.repositories.ChatRepository;
import com.chatbot.repositories.ChatbotAccountRepository;
import com.chatbot.repositories.ChatbotChatRepository;
import com.chatbot.repositories.DepartmentAccountRepository;
import com.chatbot.repositories.DepartmentRepository;
import com.chatbot.repositories.EventsPageableRepository;
import com.mongodb.AggregationOptions;
import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.Cursor;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;


/**
 * This class manages accounts
 *
 */
@Service
public class AccountService {
	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private AccountConnectionsRepository accountConnectionsRepository;
	@Autowired
	private EventsPageableRepository eventsPageableRepository;
	@Autowired
	private AccountSettingsRepository accountSettingsRepository;
	@Autowired
	private ChatbotAccountRepository chatbotAccountRepository;
	@Autowired
	private ChatbotChatRepository chatbotChatRepository;
	@Autowired
	private ChatRepository chatRepository;
	@Autowired
	public DepartmentAccountRepository departmentAccountRepository;
	@Autowired
	public DepartmentRepository departmentRepository;
	@Autowired
	public AccountActionRepository accountActionRepository;
	@Autowired
	public ActionRepository actionRepository;
	
	
	// input: none
	// output: list of accounts
	// action: get list of accounts from DB
	public List<Account> getAllAccounts(){
		return accountRepository.findAll();
	}
	
	// input: amount of last events
	// output: list of events
	// action: get "top" of last events.If top=0 then it returns all events from DB (table accountConnection) 
	public List<AccountConnection> getTopEvents(Integer top) {
		Comparator<AccountConnection> comparator = (c1, c2) -> c1.getTimeDate().compareTo(c2.getTimeDate());
		List<AccountConnection> events = null;
		
		//if top = 0 then return all events, else "top" of last messages
		if(top == 0) {
			events = accountConnectionsRepository.findAllByOrderByTimeDateDesc();
		} else {			
			List<AccountConnection> acList = accountConnectionsRepository.findAll();
			events = acList.stream().sorted(comparator.reversed()).limit(top).collect(Collectors.toList());
		}
		
		return events;
	}
	
	// input: none
	// output: size of actions
	// action: get size of actions
	public Long getSizeOfActions() {
		return actionRepository.count();
	}
	
	// input: number and size of page (page=...&size=... then convert to Pageable).
	// output: page of actions
	// action: get page of actions
	public List<ActionDto> loadActionByPage(Pageable pageable){
			Page<Action> actionPage = actionRepository.findAll(pageable);			
			List<Action> actions = actionPage.getContent();
			List<AccountAction> accountActions = accountActionRepository.findAll();
			
			ModelMapper modelMapper = new ModelMapper();
			
			return actions.stream().map(action -> {
				ActionDto actionDto = modelMapper.map(action, ActionDto.class);
				Long accountId = findAccountIdByActionId(action.getId(), accountActions);
				actionDto.setAccountId(accountId);
				
				actionDto.setStatus(Status.forCode(action.getStatus()).name());
				
				return actionDto;
			}).collect(Collectors.toList());
	}
	
	// input: identifier jf action, account-action list
	// output: identifier of account
	// action: get identifier of account
	private Long findAccountIdByActionId(Long actionId, List<AccountAction> accountActionList) {
		return accountActionList.stream()
				.filter(accountAction -> accountAction.getActionId() == actionId)
				.findFirst()
				.get()
				.getAccountId();
	}
	
	// input: identifier of account (accountId)
	// output: none
	// action: delete account from DB
	public void delete(Long accountId) {
		accountRepository.delete(accountId);
	}
	
	// input: identifier of account (accountId)
	// output: list account's events (List of AccountConnection)
	// action: get list of events by account's id
	public List<AccountConnection> getAllEventsOfAccountById(Long accountId){
		return accountConnectionsRepository.findByAccountId(accountId);
	}
	
	// input: name of account (accountName)
	// output: list account's events (List of AccountConnection)
	// action: get list of events by account's name
	public List<AccountConnection> getAllEventsOfAccountByName(String accountName){
		Account account = accountRepository.findByName(accountName);
		Long accountId = 0L;
		
		//if account was saved successfully then account is not null
		if(account != null) {
			accountId = account.getId();
		}
		
		return accountConnectionsRepository.findByAccountId(accountId);
	}
	
	// input: account Account (mapped from JSON), boolean isPasswordUpdate
	// output: boolean (if true then account updated successfully)
	// action: update account in the DB
	public Boolean update(Account account, Boolean isPasswordUpdate) {
		
		//updated account must have id > 0;
		if(account.getId() <= 0) {
			return false;
		}
		
		//if true then we need to update password to DB
		if(isPasswordUpdate) {
			account.setPassword(SecurityUtility.passwordEncoder().encode(account.getPassword()));
		}
		
		Account a = accountRepository.save(account);	
		
		//if account have saved successfully then it isn't null
		if(a == null || a.getId() <= 0) {
			return false;
		}
		
		return true;
	}
	
	// input: none
	// output: total amount of events (login/logout)
	// action: get total amount of events from the DB
	public Long totalEvents() {
		return accountConnectionsRepository.count();
	}
	
	// input: number and size of page (page=...&size=... then convert to Pageable).
	// output: page of events
	// action: get page of events
	public Page<AccountConnection> loadEventsByPage(Pageable pageable){
		return eventsPageableRepository.findAll(pageable);
	}
	
	// input: account's identifier
	// output: list of settings
	// action: get settings of account
	public List<AccountSettings> getAccountSettings(Long accountId){
		return accountSettingsRepository.findByAccountId(accountId);
	}
	
	// input: account's settings Account (mapped from JSON)
	// output: boolean (if true then account updated successfully)
	// action: update account in the DB
	public Boolean saveOrUpdateAccountSettings(AccountSettings _accountSettings) {
			AccountSettings accountSettings	= accountSettingsRepository.save(_accountSettings);
			
			//if account settings have saved successfully then it isn't null
			if(accountSettings == null) {
				return false;
			}
			
			return true;
	}
	
	// input: account's identifier
	// output: graph, single point or empty
	// action: get amount account's chat per day in the period
	public Map<Integer, Long> getAmountOfChatsForAccount(Long accountId, LocalDateTime endDt){
		LocalDateTime startDt = accountConnectionsRepository.findTop1ByAccountIdOrderByTimeDateDesc(accountId).get(0).getTimeDate();		
		Duration diff = Duration.between(startDt, endDt);		
		long days = diff.toDays();		
		
		List<ChatbotAccount> chatbotAccountList = chatbotAccountRepository.findByAccountId(accountId);
		List<ChatbotChat> chatbotChatList = chatbotChatRepository.findAll();
		List<Chat> temp = chatRepository.findAll();
		List<Chat> chats = new ArrayList<>();
		
		Map<Integer, Long> graph = new HashMap<>();
		
		Long count = 0L;
		
		if (days > 0) {
			for(ChatbotAccount ca: chatbotAccountList) {
				for(ChatbotChat cc: chatbotChatList) {
					if(cc.getChatbotId() == ca.getChatbotId()) {
						for(Chat chat: temp) {
							if(cc.getChatId() == chat.getId()) {
								LocalDateTime start = startDt;
								LocalDateTime end = startDt.plusDays(1);
								LocalDateTime startChatTime = chat.getStartChatTime();
										
								for(int i = 1; i <= days; i++) {
									if(startChatTime.compareTo(start) >= 0 && startChatTime.compareTo(end) <= 0 ) {
										count = graph.get(i);
										
										// initialize of count
										if(count == null) {
											count = 0L;					
										}
										
										count++;
										graph.put(i, count);
										
										break;
										
									}
									
									start = start.plusDays(1);
									end = end.plusDays(1);
								}
							}
						}
					}
				}
			}
		}
		
		return graph;
	}
	
	// input: account's identifier
	// output: list of related departments
	// action: get related departments
	public List<DepartmentDto> getDepartmentsRelatedToAccount(Long accountId){
		List<DepartmentAccount> departmensOfAccount = departmentAccountRepository.findByAccountId(accountId);
		List<Department> departments = departmentRepository.findAll();
		List<DepartmentDto> departmentsDto = new ArrayList<>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(DepartmentAccount da: departmensOfAccount) {
			for(Department department: departments) {
				if(department.getId() == da.getDepartmentId()) {
					DepartmentDto departmentDto = modelMapper.map(department, DepartmentDto.class);
					departmentsDto.add(departmentDto);
				}
			}
		}
		
		return departmentsDto;
	}
	
	// input: account's identifier
	// output: none
	// action: delete account from department
	public void deleteAccountFromDepartment(Long accountId) {
		departmentAccountRepository.deleteByAccountId(accountId);
	}
	
	// input: identifier of accountId, identifiers of departments
	// output: boolean (if relation "department-account" saved successfully then return true)
	// action: update departments related to account
	public Boolean updateDepartmentsRelatedAccount(Long accountId, List<Long> departmentsId) {
			
		departmentAccountRepository.deleteByAccountId(accountId);
		
			
		List<DepartmentAccount> daList = new ArrayList<>();
			
		departmentsId.stream().forEach(id -> {
			DepartmentAccount da = new DepartmentAccount(id, accountId);
			daList.add(da);
		});
			
		List<DepartmentAccount> tempList = departmentAccountRepository.save(daList);
			
		if(tempList == null || tempList.size() <= 0) {
			return false;
		}
			
		return true;
	}
	
	// input: identifier of account
	// output: account
	// action: get account by id
	public Account findById(Long accountId) {
		return accountRepository.findById(accountId);
	}
}

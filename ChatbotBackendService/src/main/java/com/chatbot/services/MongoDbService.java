package com.chatbot.services;

import java.util.List;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.domain.Support;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

@Service
public class MongoDbService {
	
	private MongoClient mongoClient;
	
	private MongoDatabase database;
	
	@Autowired
	public MongoDbService(MongoClient _mongoClient) {
		mongoClient = _mongoClient;
		database = mongoClient.getDatabase("DB");
	}
	
	public MongoDatabase getDatabase(){
		return database;
	}
}

package com.chatbot.services;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.math3.util.Precision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import com.chatbot.domain.Account;
import com.chatbot.domain.AccountAction;
import com.chatbot.domain.AccountConnection;
import com.chatbot.domain.Action;
import com.chatbot.domain.Chat;
import com.chatbot.domain.Department;
import com.chatbot.domain.DepartmentAccount;
import com.chatbot.domain.Message;
import com.chatbot.domain.Temp;
import com.chatbot.repositories.AccountActionRepository;
import com.chatbot.repositories.AccountConnectionsRepository;
import com.chatbot.repositories.AccountRepository;
import com.chatbot.repositories.ActionRepository;
import com.chatbot.repositories.ChatRepository;
import com.chatbot.repositories.DepartmentAccountRepository;
import com.chatbot.repositories.DepartmentRepository;
import com.chatbot.repositories.MessagesRepository;
import com.chatbot.repositories.UserRepository;


/**
 * This class manages the index page
 *
 */
@Service
public class IndexService {
	@Autowired
	public ChatRepository chatRepository;
	@Autowired
	public UserRepository userRepository;
	@Autowired
	public MessagesRepository messageRepository;
	@Autowired
	public AccountRepository accountRepository;
	@Autowired
	public AccountActionRepository accountActionRepository;
	@Autowired
	public AccountConnectionsRepository accountConnectionRepository;
	@Autowired
	public DepartmentAccountRepository departmentAccountRepository;
	@Autowired
	public DepartmentRepository departmentRepository;
	@Autowired
	public ActionRepository actionRepository;
	
	// input: none
	// output: total active chats
	// action: count how many chats we have online
	public Long getOnlineChats() {		
		return chatRepository.countByIsOpen(true);
	}
	
	// input: none
	// output: amount of online users
	// action: count how many users we have online
	public Long getOnlineUsers() {
		return userRepository.countByIsOnline(true);
	}
	
	// input: none
	// output: avg chat time
	// action: count time for each chat and divide in the number of chats
	public Double getAvgChatTime() throws InterruptedException {

		return Precision.round(chatRepository.findAll()
			.stream()
			.mapToInt(chat -> chat.getTimeOnChat())
			.average()
			.getAsDouble(), 2);		
	}
	
	// input: none
	// output: total messages in last hour
	// action: count how many messages with time of now ( -1 hour)
	public Long getCountMessages() {
		
		return messageRepository.findByTimeBetween(LocalDateTime.now()
				.minusHours(1), LocalDateTime.now())
				.stream()
				.count();
	}
	 
	// input: none
	// output: amount of human chat
	// action: how many chats has human chat support flag
	public Long getHumanChats() {
		return chatRepository.countByIsHuman(true);
	}
	
	// input: none
	// output: gender distribution
	// action: get gender distribution graph
	public Map<String, Long> genderDistribution(){
		Long countMale = userRepository.countByIsMale(1);
		Long countFemale = userRepository.countByIsMale(0);
		Map<String,Long> gender = new HashMap<>();
		gender.put("male", countMale);
		gender.put("female", countFemale);
		
		return gender;
	}
	
	// input: none
	// output: human chat support 
	// action: how many chats has human chat support flag
	public Map<String, Long> humanSupport(){
		
		Long countHuman = chatRepository.countByIsHuman(true);
		Long countBots = chatRepository.countByIsHuman(false);
		
		Map<String,Long> supportMap = new HashMap<>();
		supportMap.put("human", countHuman);
		supportMap.put("bots", countBots);
		
		return supportMap;
	}
	
	// input: none
	// output: accounts online
	// action: count how many accounts is online
	public Long getCountAccountsOnline() {
		Long count = 0L;
		List<Account> accounts = accountRepository.findAll();
		
		// search online accounts
		for(Account account: accounts) {
			List<AccountConnection> acl = accountConnectionRepository.findTop1ByAccountIdOrderByTimeDateDesc(account.getId());
		
			//if account exists check isLogin flag
			if(acl != null && acl.size() > 0) {
				AccountConnection ac = acl.get(0);
				
				// check account isLogin flag
				if(ac.isLogin()) {
					++count;
				}
			}
		}
		
		return count;
	}
	
	// input: none
	// output: human support percent
	// action: get human support percent graph
	public Map<Integer, Double> getSupportHumanChats() {
		LocalDateTime currentDt = LocalDateTime.now();
		LocalDateTime temp = currentDt;	
		
		final int HOURS = 24;
		
		Long total = chatRepository.countByIsHumanTalkedIsTrueAndStartChatTimeBetween(temp.minusHours(HOURS), temp);
		
		temp = temp.minusHours(HOURS);
		
		Long count = 0L;
		Map<Integer, Double> map = new HashMap<>();
		int i = 1;
		
		// search human chats each hour (24 hours)
		while(i <= HOURS) {			
			count = chatRepository.countByIsHumanTalkedIsTrueAndStartChatTimeBetween(temp, temp.plusHours(1)); /*temp.minusHours(1), temp);*/
			map.put(i,  Precision.round((double)(count * 100)/total, 2));
			temp = temp.plusHours(1);//temp.minusHours(1);
			i++;
		}
		
		return map;
	}
	
	// input: none
	// output: amount of accounts in each department
	// action: get department distribution graph
	public Map<String, Integer> getDepartmentDistribution(){
		Map<String, Integer> distribMap = new HashMap<>();
		List<Department> departments = departmentRepository.findAll();
		
		departments.stream().forEach(department -> {
			distribMap.put(department.getName(), departmentAccountRepository.countByDepartmentId(department.getId()));
		});
		
		return distribMap;
	}
	
	// input: identifier of account
	// output: 5 Last actions
	// action: table of the details of the last 5 actions the account did (alerts)
	public List<Action> getLastActionsOfAccount(Long accountId){
		Comparator<Action> comparator = (a1, a2) -> a1.getDate().compareTo(a2.getDate());
	     
		Supplier<Stream<AccountAction>> accountActionSupplier = () -> accountActionRepository.findByAccountId(accountId).stream();
		List<Action> actions = accountActionSupplier.get().map(aa -> actionRepository.findById(aa.getActionId()))
				.sorted(comparator.reversed())
				.limit(5)
				.collect(Collectors.toList());
		
		return actions;
	}
	
	
	public void setUp() {
		/*Message m1 = new Message(1L, "12098", "Hello", false, false, LocalDateTime.now(), 0);
		messageRepository.save(m1);
		Message m2 = new Message(2L, "12095", "Hello Bob", false, false, LocalDateTime.now().minusMonths(3), 0);
		messageRepository.save(m2);
		Message m3 = new Message(3L, "12093", "Hello Billy", false, false, LocalDateTime.now(), 0);
		messageRepository.save(m3);*/
		/*List<AccountConnection> list = new ArrayList();
		
		AccountConnection ac1 = new AccountConnection(1L, LocalDateTime.now().minusHours(5), false);
		list.add(ac1);
		AccountConnection ac2 = new AccountConnection(1L, LocalDateTime.now().minusHours(4), false);
		list.add(ac2);
		AccountConnection ac3 = new AccountConnection(1L, LocalDateTime.now().minusHours(3), false);
		list.add(ac3);
		AccountConnection ac4 = new AccountConnection(1L, LocalDateTime.now().minusHours(2), false);
		list.add(ac4);
		AccountConnection ac5 = new AccountConnection(1L, LocalDateTime.now(), true);
		list.add(ac5);
		
		accountConnectionRepository.saveAll(list);*/
		
		/*List<Account> list = new ArrayList();
		
		Account ac1 = new Account(1L, "Bob", "123", "123", "123", "123", "123", "", 1, LocalDateTime.now(), "");
		list.add(ac1);
		Account ac2 = new Account(2L, "Bill", "123", "123", "123", "123", "123", "", 1, LocalDateTime.now(), "");
		list.add(ac2);
		
		accountRepository.saveAll(list);*/
		
		/*List<Department> list = new ArrayList();
		
		Department d1 = new Department(1L, "Motorolla", 1L);
		list.add(d1);
		Department d2 = new Department(2L, "Samsung", 1L);
		list.add(d2);
		
		departmentRepository.saveAll(list);
		
		List<DepartmentAccount> da = new ArrayList();
		
		DepartmentAccount da1 = new DepartmentAccount(1L, 1L); 
		da.add(da1);
		DepartmentAccount da2 = new DepartmentAccount(1L, 2L); 
		da.add(da2);
		DepartmentAccount da3 = new DepartmentAccount(1L, 3L); 
		da.add(da3);
		DepartmentAccount da4 = new DepartmentAccount(2L, 1L); 
		da.add(da4);
		
		departmentAccountRepository.saveAll(da);
		
		List<AccountAction> aa = new ArrayList();
		
		AccountAction aa1 = new AccountAction(1L, 1L); 
		aa.add(aa1);
		AccountAction aa2 = new AccountAction(1L, 2L); 
		aa.add(aa2);
		AccountAction aa3 = new AccountAction(1L, 3L); 
		aa.add(aa3);
		AccountAction aa4 = new AccountAction(1L, 4L); 
		aa.add(aa4);
		AccountAction aa5 = new AccountAction(1L, 5L); 
		aa.add(aa5);
		AccountAction aa6 = new AccountAction(1L, 6L); 
		aa.add(aa6);
		AccountAction aa7 = new AccountAction(1L, 7L); 
		aa.add(aa7);
		
		
		accountActionRepository.saveAll(aa);
		
		List<Action> a = new ArrayList();
		
		Action a1 = new Action(1L, 0L, LocalDateTime.now().minusMinutes(10), 1, ""); 
		a.add(a1);
		Action a2 = new Action(2L, 0L, LocalDateTime.now().minusMinutes(1), 1, ""); 
		a.add(a2);
		Action a3 = new Action(3L, 0L, LocalDateTime.now().minusMinutes(15), 1, ""); 
		a.add(a3);
		Action a4 = new Action(4L, 0L, LocalDateTime.now().minusMinutes(30), 1, ""); 
		a.add(a4);
		Action a5 = new Action(5L, 0L, LocalDateTime.now().minusMinutes(1), 1, ""); 
		a.add(a5);
		Action a6 = new Action(6L, 0L, LocalDateTime.now().minusMinutes(37), 1, ""); 
		a.add(a6);
		Action a7 = new Action(7L, 0L, LocalDateTime.now().minusMinutes(45), 1, ""); 
		a.add(a7);

		actionRepository.saveAll(a);*/
	}
}

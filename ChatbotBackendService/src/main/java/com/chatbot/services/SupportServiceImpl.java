package com.chatbot.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.domain.ChatbotSupport;
import com.chatbot.domain.Department;
import com.chatbot.domain.Support;
import com.chatbot.interfaces.SupportService;
import com.chatbot.repositories.ChatbotRepository;
import com.chatbot.repositories.ChatbotSupportRepository;
import com.chatbot.repositories.SupportRepository;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;

/**
 * This class manages the support table
 *
 */
@Service
public class SupportServiceImpl implements SupportService {
	@Autowired
	private SupportRepository supportRepository;	
	@Autowired
	private CounterServiceImpl counterService;
	@Autowired
	private ChatbotRepository chatbotRepository;
	@Autowired
	private ChatbotSupportRepository chatbotSupportRepository;
	
	//@Autowired
	//private MongoDbService mongoService;
	
	private MongoCollection<Support> supportCollection;
	
	public SupportServiceImpl(MongoDbService mongoService) {
		supportCollection = mongoService.getDatabase().getCollection("support", Support.class);
	}
		
	// input: none
	// output: list of open tickets
	// action: get open tickets from the DB
	@Override
	public List<Support> loadOpenTickets() {
		return supportRepository.findByIsOpen(true);
	}

	// input: none
	// output: list of closed tickets
	// action: get closed tickets from the DB
	@Override
	public List<Support> loadClosedTickets() {
		return supportRepository.findByIsOpen(false);
	}

	// input: Support (JSON)
	// output: boolean (if ticket saved successfully then return true)
	// action: save ticket to DB
	@Override
	public Boolean saveOrUpdate(Support support) {
		boolean isSaved = true;
			
		if(support.getId() <= 0) {
			support.setId(counterService.getNextIdSequence());
			support.setIsOpen(true);
		}
			
		Support supOut = supportRepository.save(support);
			
		if(supOut == null) {
			isSaved = false;
		}
					
		return isSaved;
	}
	
	// input: identifier of ticket
	// output: none
	// action: delete ticket from the DB
	@Override
	public void delete(Long ticketId) {
		supportRepository.delete(ticketId);
		chatbotSupportRepository.deleteBySupportId(ticketId);
	}
	
	public List<Support> getGroupedSupportList(){
		return supportCollection.aggregate(
			      Arrays.asList(
			              Aggregates.group("$isOpen")
			      )
				).into(new ArrayList<Support>());
	}

	// input: identifier of chatbot
	// output: list of tickets
	// action: get all tickets of chatbot
	@Override
	public List<Support> getAllTicketsOfChatbot(Long chatbotId) {
		List<Support> tickets = new ArrayList<>();
		List<ChatbotSupport> chatbotSupportList = chatbotSupportRepository.findByChatbotId(chatbotId);
		return chatbotSupportList.stream()
				.map(cs -> supportRepository.findOne(cs.getSupportId()))
				.collect(Collectors.toList());
	}

	// input: identifier of chatbot, ticket
	// output: result of saving (true if saved successfully)
	// action: save support of chatbot
	@Override
	public Boolean saveChatbotSupport(Long chatbotId, Support _support) {
		Support support = supportRepository.save(_support);
		
		if(support == null)
		{
			return false;
		}
		
		ChatbotSupport chatbotSupport = new ChatbotSupport(chatbotId, _support.getId());
		
		ChatbotSupport cs = chatbotSupportRepository.save(chatbotSupport);
		
		if(cs == null) {
			supportRepository.delete(support.getId());
			return false;
		}
		
		return true;
	}
}

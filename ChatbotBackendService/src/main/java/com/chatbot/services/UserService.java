package com.chatbot.services;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.chatbot.domain.Answer;
import com.chatbot.domain.Chat;
import com.chatbot.domain.ChatUser;
import com.chatbot.domain.User;
import com.chatbot.domain.UserComment;
import com.chatbot.repositories.ChatRepository;
import com.chatbot.repositories.ChatUserRepository;
import com.chatbot.repositories.UserRepository;


/**
 * This class manages of users
 *
 */
@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ChatUserRepository chatUserRepository;
	@Autowired
	private ChatRepository chatRepository;
	@Autowired
	private CounterServiceImpl counterService;
	
	// input: identifier of user
	// output: user's all information
	// action: get all data that we have about an user
	public User getUserById(Long userId) {
		return userRepository.findById(userId);
	}
	
	// input: none
	// output: list of users
	// action: get all users from DB
	public List<User> getAllUsers(){
		return userRepository.findAll();
	}
	
	// input: user
	// output: result of save
	// action: save user to DB
	public Boolean saveUser(User user) {
		boolean isSaved = true;
		
		// check user's identifier
		if(user.getId() <= 0) {
			user.setId(counterService.getNextIdSequence());
		}
		
		User userOut = userRepository.save(user);
		
		// check saved user
		if(userOut == null) {
			isSaved = false;
		}
		
		return isSaved;
	}
	
	// input: identifier of user
	// output: none
	// action: delete user from DB
	public void delete(Long userId) {
		userRepository.delete(userId);
	}
	
	// input: UserComment object
	// output: Answer object (if isSucccess is true then updated was successfull)
	// action: update comment of user
	public Answer updateUserComment(UserComment uc) {
		Answer answer = new Answer();
		answer.setSuccess(false);
		
		if(uc.getUserId() != null && uc.getUserId() > 0) {
			User user = userRepository.findById(uc.getUserId());
			
			if(user != null) {
				user.setComment(uc.getComment());
				userRepository.save(user);
				answer.setSuccess(true);
			}
		}
		
		return answer;
	}
	
	// input: identifier of user
	// output: information of user
	// action: load all the data we have on the user from the DB 
	public User getUser(Long userId) {
		return userRepository.findById(userId);
	}
	
	// input: identifier of user
	// output: chats
	// action: load all chats of user 
	public List<Chat> chatsHistory(Long userId) {
		Comparator<Chat> comparator = (c1, c2) -> c1.getStartChatTime().compareTo(c2.getStartChatTime());
		
		List<Chat> chats = chatRepository.findAll();
		List<ChatUser> chatUserList = chatUserRepository.findByUserId(userId);
		List<Chat> outList = new ArrayList();
		
		for(ChatUser cu: chatUserList) {
			for(Chat chat: chats) {
				if(chat.getId() == cu.getChatId()) {
					outList.add(chat);
				}
			}
		}
		
		return outList.stream()
				.sorted(comparator)
				.collect(Collectors.toList());
		
	}
}

package com.chatbot.services;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatbot.domain.Chatbot;
import com.chatbot.domain.Dictionary;
import com.chatbot.domain.Script;
import com.chatbot.dto.ChatbotDto;
import com.chatbot.dto.ScriptDto;
import com.chatbot.interfaces.ChatbotService;
import com.chatbot.repositories.ChatbotRepository;


/**
 * This class manages the chatbots table
 *
 */
@Service
public class ChatbotServiceImpl implements ChatbotService{
	@Autowired
	private ChatbotRepository chatbotRepository;
	@Autowired
	private CounterServiceImpl counterService;
	@Autowired
	private ScriptServiceImpl scriptService;

	// input: identifier of chatbot
	// output: chatbot
	// action: find chatbot by id
	@Override
	public Chatbot findById(Long botId) {		
		return chatbotRepository.findById(botId);
	}

	// input: chatbot
	// output: chatbot
	// action: save chatbot to DB
	@Override
	public Chatbot save(Chatbot chatbot) {
		
	    if(chatbot.getId() <= 0) {
	    	chatbot.setId(counterService.getNextIdSequence());
	    }
	    
		return chatbotRepository.save(chatbot);
	}

	// input: chatbot's identifier
	// output: chatbotDto with scripts and dictionaries
	// action: get scripts and dictionaries of chatbot
	@Override
	public ChatbotDto loadScriptsAndDictionariesOfChatbot(Long chatbotId) {
		List<Script> scripts = scriptService.loadScriptsOfChatbot(chatbotId);
		ModelMapper modelMapper = new ModelMapper();
		Chatbot chatbot = chatbotRepository.findById(chatbotId);
		ChatbotDto chatbotDto = modelMapper.map(chatbot, ChatbotDto.class);
		
		chatbotDto.setScripts(new ArrayList<ScriptDto>());
		
		scripts.stream().forEach(script -> {
			List<Dictionary> dictionaries = scriptService.loadDictionariesOfScript(script.getId());
			ScriptDto scriptDto = modelMapper.map(script, ScriptDto.class);
			scriptDto.setDictionaries(new ArrayList<Dictionary>());
			
			for(Dictionary dictionary: dictionaries) {
				scriptDto.getDictionaries().add(dictionary);
			}
			
			chatbotDto.getScripts().add(scriptDto);
			
		});
		
		return chatbotDto;
	}

}

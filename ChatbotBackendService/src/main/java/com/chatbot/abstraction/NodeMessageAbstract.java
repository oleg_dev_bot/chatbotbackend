package com.chatbot.abstraction;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class NodeMessageAbstract {
	private String type;
	
}

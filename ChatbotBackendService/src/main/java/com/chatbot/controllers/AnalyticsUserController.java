package com.chatbot.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.services.AnalyticsUserService;

/**
 * This class manages analytics-user tasks
 *
 */
@RestController
public class AnalyticsUserController {

	@Autowired
	public AnalyticsUserService auService;
	
	// input: none
	// output: gender (male/female), amount of each gender
	// action: how many male, female, and unknown
	@RequestMapping("/gender/distribution/all")
	public Map<String, Long> getGenderDistribution(){
		return auService.genderDistribution();
	}
	
	// input: none
	// output: gender (male/female), amount of each gender
	// action: how many male, female, and unknown
	@RequestMapping("/age/distribution")
	public Map<Integer, Long> getAgeDistribution(){
		return auService.ageDistribution();
	}
	
	// input: none
	// output: type of device, amount of each device
	// action: how many from each device [enum: computer, phone, tablet]
	@RequestMapping("/device/distribution")
	public Map<Integer, Long> getDeviceDistribution(){
		return auService.deviceDistribution();
	}
	
	// input: none
	// output: platform type, amount of each platform
	// action: how many from each platform [enum: 7 platforms we have]
	@RequestMapping("/platform/distribution")
	public Map<Integer, Long> getPlatformDistribution(){
		return auService.platformDistribution();
	}
}

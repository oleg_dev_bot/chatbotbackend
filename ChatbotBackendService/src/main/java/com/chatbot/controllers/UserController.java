package com.chatbot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.domain.Answer;
import com.chatbot.domain.Chat;
import com.chatbot.domain.Department;
import com.chatbot.domain.User;
import com.chatbot.domain.UserComment;
import com.chatbot.services.DepartmentServiceImpl;
import com.chatbot.services.UserService;


/**
 * This class manages of users
 *
 */
@RestController
public class UserController {
	@Autowired
	public UserService userService;
	
	// input: none
	// output: list of users
	// action: get all users on page load
	@GetMapping("/users")
	public List<User> loadUsers(){
		return userService.getAllUsers();
	}
	
	// input: User (JSON)
	// output: true if saved successfully
	// action: get all users on page load
	@PostMapping("/user/save")
	public Boolean saveOrUpdate(@RequestBody User user){
		return userService.saveUser(user);
	}
	
	// input: identifier of user
	// output: none
	// action: delete of the user from DB
	@RequestMapping("/user/delete/{userId}")
	public void delete(@PathVariable("userId") Long userId){
		userService.delete(userId);
	}
	
	// input: UserComment object
	// output: Answer object (if isSucccess is true then updated was successfull)
	// action: update comment of user
	@PostMapping("/user/comment")
	public Answer updateUserComment(@RequestBody UserComment uc){
		return userService.updateUserComment(uc);
	}
	
	// input: identifier of user
	// output: information of user
	// action: load all the data we have on the user from the DB 
	@GetMapping("/user/info/{userId}")
	public User getUser(@PathVariable("userId") Long userId){
		return userService.getUser(userId);
	}
	
	// input: identifier of user
	// output: chats
	// action: load all chats of user  
	@GetMapping("/user/chats/{userId}")
	public List<Chat> getChatsHistory(@PathVariable("userId") Long userId){
		return userService.chatsHistory(userId);
	}
}

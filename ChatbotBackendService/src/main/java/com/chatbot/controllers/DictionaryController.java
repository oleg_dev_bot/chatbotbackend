package com.chatbot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.domain.Dictionary;
import com.chatbot.domain.Script;
import com.chatbot.dto.ChatbotDictionariesDto;
import com.chatbot.dto.ScriptDictionariesDto;
import com.chatbot.services.DictionaryServiceImpl;
import com.chatbot.services.ScriptServiceImpl;

@RestController
public class DictionaryController {
	@Autowired
	private DictionaryServiceImpl dictionaryService;

	// input: dictionary (JSON)
	// output: boolean (if dictionary was saved successfully then true)
	// action: save dictionary to DB
	@PostMapping("/dictionary/save")
	public Boolean save(@RequestBody Dictionary dictionary) {
		return dictionaryService.save(dictionary);
	}
	
	// input: identifier of chatbot
	// output: list of dictionaries
	// action: get all dictionaries of chatbot from DB
	@GetMapping("/dictionary/chatbot/{chatbotId}")
	public List<Dictionary> loadScripts(@PathVariable("chatbotId") Long chatbotId){
		return dictionaryService.loadDictionariesOfChatbot(chatbotId);
	}
	
	// input: identifier of dictionary
	// output: none
	// action: delete dictionary
	@RequestMapping("/dictionary/delete/{dictionaryId}")
	public  void delete(@PathVariable("dictionaryId") Long dictionaryId){
		dictionaryService.delete(dictionaryId);
	}
	
	// input: identifier of chatbot and list of dictionaries id
	// output: none
	// action: save dictionaries to chatbot
	@PostMapping("/dictionary/chatbot/save")
	public void assignDictionariesToChatbot(@RequestBody ChatbotDictionariesDto chatbotDictionaries) {
		dictionaryService.assignDictionariesToChatbot(chatbotDictionaries);
	}
	
	// input: identifier of script and list of dictionaries id
	// output: none
	// action: save dictionaries to script
	@PostMapping("/dictionary/script/save")
	public void assignDictionariesToScript(ScriptDictionariesDto scriptDictionaries) {
		dictionaryService.assignDictionariesToScript(scriptDictionaries);
	}
	
	
	@GetMapping("/dictionary/get/{dictionaryId")
	public void getDictionary(@PathVariable("dictionaryId") Long dictionaryId) {
		dictionaryService.loadDictionaryById(dictionaryId);
	}
}

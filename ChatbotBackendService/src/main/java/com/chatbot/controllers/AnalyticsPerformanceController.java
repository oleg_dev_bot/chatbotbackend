package com.chatbot.controllers;

import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.services.AnalyticsPerformanceService;


/**
 * 
 *
 */
@RestController
public class AnalyticsPerformanceController {
	@Autowired
	public AnalyticsPerformanceService analyticsPerformanceService;
	
	// input: identifier of chatbot
	// output: String = sent/received, Long = total messages
	// action: how many massages sent and how many recived to the bot  
	@GetMapping("/chatbot/messages/{botId}")
	public Map<String, Long> countMessageSendAndReceive(@PathVariable("botId") Long botId) {
		return analyticsPerformanceService.howManyMessagesSentAndReceivedToTheBot(botId);
	}
	
	// input: none
	// output: amount of users
	// action: get how many users send only 1 message   
	@GetMapping("/users/messages/one")
	public Long countUsersSentOnly1Message() {
		return analyticsPerformanceService.countUsersOnly1Message();
	}
	
	// input: none
	// output: messages per chat 
	// action: get how many messages per chat 
	@GetMapping("/messages/per/chat")
	public Map<String, Long> getMessagesPerChat(){
		return analyticsPerformanceService.howManyMessagesPerChat();
	}
	
	// input: none
	// output: group id, amount of users in each group
	// action: get how many users we have in each group   
	@GetMapping("/group/users")
	public Map<Long, Long> getUsersInGroup(){
		return analyticsPerformanceService.howManyUsersInEachGroup();
	}
	
	// input: chatbotid
	// output: min to hit the target
	// action: get how much messages took to hit the target (minimal)  
	@GetMapping("/messages/hit/min/{botId}")
	public Integer minMessagesToHitTheTarget(@PathVariable("botId") Long botId){
		return analyticsPerformanceService.minToHitTheTarget(botId);
	}
	
	// input: chatbotid
	// output: max to hit the target
	// action: get how much messages took to hit the target (maximal)
	@GetMapping("/messages/hit/max/{botId}")
	public Integer maxMessagesToHitTheTarget(@PathVariable("botId") Long botId){
		return analyticsPerformanceService.maxToHitTheTarget(botId);
	}
	
	// input: chatbotid
	// output: AVG messages
	// action: get AVG messages number took to hit the target
	@GetMapping("/messages/hit/avg/{botId}")
	public Double avgMessagesToHitTheTarget(@PathVariable("botId") Long botId){
		return analyticsPerformanceService.AVGToHitTheTarget(botId);
	}
	
	// input: none
	// output: department's name, amount of accounts
	// action: get how many accounts we have in each department
	@GetMapping("/departments/accounts/amount")
	public Map<String, Integer> howManyAccountsInEachDepartment() {
		return analyticsPerformanceService.howManyAccountsInEachDepartment();
	}
}

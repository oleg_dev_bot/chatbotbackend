package com.chatbot.controllers;


import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * This is the test class
 *
 */
@RestController
public class LoginController {
	
	@RequestMapping("/")
	public String login() {
		return "You Are Logged In!";
	}
	
	@RequestMapping("/token")
	public Map<String, String> token(HttpSession session, HttpServletRequest request) {
		System.out.println(request.getRemoteHost());
		
		String remoteHost = request.getRemoteHost();
		int portNumber = request.getRemotePort();
		
		System.out.println(remoteHost+":"+portNumber);
		System.out.println(request.getRemoteAddr());
		
		return Collections.singletonMap("token", session.getId());
	}

	
	@PostMapping(value = "/account/logout")
	//@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity logout(HttpSession session) {
		//session.invalidate();
		SecurityContextHolder.clearContext();
		
		return new ResponseEntity("You have successfully logged out!", HttpStatus.OK);
	}
	
	
}

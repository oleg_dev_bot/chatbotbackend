package com.chatbot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.domain.Plan;
import com.chatbot.services.PlanServiceImpl;

@RestController
public class PlanController {
	@Autowired
	private PlanServiceImpl planService;
	
	// input: none
	// output: plans
	// action: load all plans
	@GetMapping("/plan/load/all")
	public List<Plan> loadPlans(){
		return planService.loadPlans();
	}
	
	// input: identifier of chatbot and identifier of plan
	// output: none
	// action: assign plan to chatbot
	@RequestMapping("/plan/assign/chatbot")
	public void assignPlanToBot(@RequestParam("botId") Long botId, @RequestParam("planId") Long planId){
		planService.assignPlanToBot(botId, planId);
	}
	
	// input: identifier of chatbot 
	// output: money spend
	// action: get how much the bot spend
	@GetMapping("/chatbot/spend")
    public Double chatbotSpend(@PathVariable("") Long botId){
		return planService.chatbotSpend(botId);
	}
}

package com.chatbot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.domain.Dictionary;
import com.chatbot.domain.Script;
import com.chatbot.services.ScriptServiceImpl;

@RestController
public class ScriptController {
	@Autowired
	private ScriptServiceImpl scriptService;

	// input: script (JSON)
	// output: boolean (if script was saved successfully then true)
	// action: save script to DB
	@PostMapping("/script/save")
	public Boolean saveScript(@RequestBody Script script) {
		return scriptService.save(script);
	}
	
	// input: none
	// output: list of scripts
	// action: get all scripts from DB
	@GetMapping("/script/get/all")
	public List<Script> loadScripts(){
		return scriptService.loadScripts();
	}
	
	// input: identifier of chatbot
	// output: list of scripts
	// action: get all scripts of chatbot from DB
	@GetMapping("/script/chatbot/{chatbotId}")
	public List<Script> loadScripts(@PathVariable() Long chatbotId){
		return scriptService.loadScriptsOfChatbot(chatbotId);
	}
	
	// input: identifier of script
	// output: list of dictionary
	// action: get dictionaries of script
	@GetMapping("/script/dictionaries/{scriptId}")
	public List<Dictionary> loadDictionariesOfScript(@PathVariable("scriptId") Long scriptId){
		return scriptService.loadDictionariesOfScript(scriptId);
	}
	
	// input: identifier of script
	// output: none
	// action: delete script from DB
	@RequestMapping("/script/delete/{scriptId}")
	public void delete(Long scriptId) {
		scriptService.delete(scriptId);
	}
	
	// input: identifier of chatbot and identifier of script
	// output: boolean (if saved successfully then true)
	// action: save script of chatbot
	@RequestMapping("/script/chatbot/save")
	public Boolean saveChatbotScript(@RequestParam("chatbotId") Long chatbotId, 
									 @RequestParam("scriptId") Long scriptId) {
		return scriptService.saveChatbotScript(chatbotId, scriptId);
	}
}

package com.chatbot.controllers;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.domain.Alert;
import com.chatbot.domain.Chat;
import com.chatbot.domain.Message;
import com.chatbot.domain.User;
import com.chatbot.domain.UserSummary;
import com.chatbot.repositories.ChatRepository;
import com.chatbot.services.LiveChatService;
import com.chatbot.services.MessageService;
import com.chatbot.services.UserService;

/**
 * This controller manages the tasks of live chat.
 *
 */
@RestController
public class LiveChatController {
	@Autowired
	private MessageService messageService;
	@Autowired
	public LiveChatService liveChatService;
	@Autowired
	private UserService userService;
	
	// input: identifier of chat
	// output: time on chat
	// action: get how much time a spesific online chat is running
	@GetMapping("/chat/online/time")
	public ResponseEntity<Long> timeOnChat(@RequestParam("chatId") Long chatId){
		Long timeOnline = liveChatService.timeOnChat(chatId);
		
		if(timeOnline == -1) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(timeOnline);
	}
	
	// input: identifier of user
	// output: status of user
	// action: get the user's status from the DB and return it
	@GetMapping("/user/status/{userId}")
	public Integer getUserStatus(@PathVariable("userId") Long userId){
		Integer status = liveChatService.getUserStatus(userId);		
		
		//if user isn't exist then return -1 (not exist)
		if(status == -1) {
			return -1;
		}
		
		return status;
	}
	
	// input: identifier of chat
	// output: amount of messages sent on chat
	// action: get how many messages a specific online chat is did
	@GetMapping("/chat/messages/{chatId}")
	public Long howManyMessagesSentToChat(@PathVariable("chatId")Long chatId) {
		return liveChatService.howManyMessagesSentToChat(chatId);
	}
	
	// input: identifier of user
	// output: dominant emotion of user
	// action: get the main feeling from the user from the last message from Watson's tone analyser and present it
	@GetMapping("/user/emotion/dominant/{userId}")
	public Map<String, Double> getDominantEmotionUser(@PathVariable("userId") Long userId) {
		return liveChatService.getDominantEmotionUser(userId);
	}
	
	// input: identifier of user
	// output: 5 dominant emotions of user (String = emotion, Double = score)
	// action: get the 5 main feeling from the user from the 5 last message from Watson's tone analyzer
	@GetMapping("/user/emotions/five/dominant/{userId}")
	public List<Map<String, Double>> get5DominantEmotionsOfUser(@PathVariable("userId") Long userId) {
		return liveChatService.get5DominantEmotionsOfUser(userId);
	}
	
	// input: identifier of chatbot and identifier of chat
	// output: none
	// action: stops or starts the bot ability to answer an input
	@GetMapping("/chatbot/chat/control")
	public ResponseEntity giveAndTakeControlFromBot(@RequestParam("chatbotId") Long chatbotId, @RequestParam("chatId") Long chatId) {
		return liveChatService.giveAndTakeControlFromBot(chatbotId, chatId);
	}
	
	// input: identifier of user
	// output: AVG respone time
	// action: get how much time in AVG takes to the user to answer
	@GetMapping("/user/response/avg/{userId}")
	public Double userAVGResponseTime(@PathVariable("userId") Long userId) {
		return liveChatService.AVGResponseTime(userId);
	}
	
	// input: message from account
	// output: none
	// action: when sending message the account mark if specific or not and save it to DB
	@PostMapping("/message/mark/save")
	@ResponseBody
	public void saveOrUpdate(@RequestBody Message message) {
	    messageService.save(message);
	}
	
	// input: identifier of user
	// output: user's summary information
	// action: get the data that we have about an user - photo, status, name, summary, location, comment
	@GetMapping("/user/summary/{userId}")
	public UserSummary getUserSummary(@PathVariable("userId") Long userId) {
		return liveChatService.getUserSummary(userId);
	}
	
	// input: identifier of user
	// output: user's all information
	// action: get all data that we have about an user
	@GetMapping("/user/all/{userId}")
	public User getFullUserInfo(@PathVariable("userId") Long userId) {
		return userService.getUserById(userId);
	}
	
	// input: alert to account (JSON) and identifier of account
	// output: none
	// action: save alert of account
	@PostMapping("/alert/account/save/{accountId}")
	@ResponseBody
	public void saveOrUpdate(@RequestBody Alert alert, @PathVariable("accountId") Long accountId) {
	    liveChatService.saveAlertOfAccount(accountId, alert);
	}
}

package com.chatbot.controllers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.domain.AccountConnection;
import com.chatbot.domain.Action;
import com.chatbot.domain.Chat;
import com.chatbot.domain.Message;
import com.chatbot.domain.Temp;
import com.chatbot.domain.User;

import com.chatbot.services.IndexService;


/**
 * This class manages the Index page
 *
 */
@RestController
public class IndexController{	
	@Autowired
	public IndexService indexService;

	// input: none
	// output: total active chats
	// action: count how many chats we have online
	@GetMapping("/chats/online")
	public Long getOnlineChats() {		
		return indexService.getOnlineChats();
	}
	
	// input: none
	// output: amount of human chat
	// action: how many chats has human chat support flag
	@GetMapping("/chats/human")
	public Long getHumanChats() {		
		return indexService.getHumanChats();
	}
	
	// input: none
	// output: amount of online users
	// action: count how many users we have online
	@GetMapping("/users/online")
	public Long getOnlineUsers() {
		return indexService.getOnlineUsers();
	}
	
	// input: none
	// output: avg chat time
	// action: count time for each chat and divide in the number of chats
	@GetMapping("/chats/time/avg")
	public Double getAvgChatTime() throws InterruptedException {
		return indexService.getAvgChatTime();
	}
	
	/*@GetMapping("/accounts/online")
	public Long getAccounts() {
		Long count = indexService.getCountAccountsOnline();
		
		return count;
	}*/
	
	// input: none
	// output: total messages in last hour
	// action: count how many messages with time of now ( -1 hour)
	@GetMapping("/chats/messages/count")
	public Long getCountMessages() {
		return indexService.getCountMessages();
	}
	
	// input: none
	// output: gender distribution
	// action: get gender distribution graph
	@GetMapping("/users/gender")
	public Map<String, Long> genderDistribution() {
		return indexService.genderDistribution();
	}
	
	// input: none
	// output: human chat support 
	// action: how many chats has human chat support flag
	@GetMapping("/chats/support")
	public Map<String, Long> chatsSupport() {
		return indexService.humanSupport();
	}
	
	// input: none
	// output: accounts online
	// action: count how many accounts is online
	@GetMapping("/accounts/online")
	public Long getAccountsOnline() {
		return indexService.getCountAccountsOnline();
	}
	
	// input: none
	// output: human support percent
	// action: get human support percent graph
	@GetMapping("/chats/support/human")
	public Map<Integer, Double> getSupportHumanChats() {
		return indexService.getSupportHumanChats();
	}
	
	// input: none
	// output: amount of accounts in each department
	// action: get department distribution graph
	@GetMapping("/departments/accounts/disrtribution")
	public Map<String, Integer> getDepartmentDistribution() {
		return indexService.getDepartmentDistribution();
	}
	
	// input: identifier of account
	// output: 5 Last actions
	// action: table of the details of the last 5 actions the account did (alerts)
	@GetMapping("/account/actions/{id}")
	public List<Action> getLastActionsOfAccount(@PathVariable("id") Long accountId) {
		return indexService.getLastActionsOfAccount(accountId);
	}
	

	private void setUp() {		
		/*final Chat chat1 = new Chat(1L, 1L, 1L, 10, false, true, LocalDateTime.now());
		final Chat chat2 = new Chat(2L, 2L, 2L, 110, false, true, LocalDateTime.now());
		final Chat chat3 = new Chat(3L, 3L, 3L, 1000, true, true, LocalDateTime.now());
		final Chat chat4 = new Chat(4L, 4L, 4L, 35, false, true, LocalDateTime.now());
		final Chat chat5 = new Chat(5L, 5L, 5L, 1800, false, true, LocalDateTime.now());
		final Chat chat6 = new Chat(6L, 5L, 5L, 17, true, true, LocalDateTime.now());
		
		chatRepository.saveAll(Flux.just(chat1, chat2, chat3, chat4, chat5, chat6))
		.then()
		.block();
		
		final User user1 = new User(10L, "Bob", "fggtredeweZ", 1, 25, "iujyyyyhxxx", "bob@gmail.com", 5, "Chickago", "", true, LocalDate.now(), "", LocalDate.now(), 100, 1, 1, "", LocalDate.now(), "1893");
		final User user2 = new User(11L, "Billy", "fggtredewe", 1, 25, "iujyyyyhxxx", "bob@gmail.com", 5, "Chickago", "", false, LocalDate.now(), "", LocalDate.now(), 100, 1, 1, "", LocalDate.now(), "1897");
		
		userRepository.saveAll(Flux.just(user1, user2))
		.then()
		.block();*/

	}

}


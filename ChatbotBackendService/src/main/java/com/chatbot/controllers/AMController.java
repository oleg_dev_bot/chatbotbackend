package com.chatbot.controllers;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.util.Precision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.domain.Action;
import com.chatbot.services.AnalyticsMarketingService;


/**
 * This class manages analytics-marketing tasks
 *
 */
@RestController
public class AMController {
	@Autowired
	AnalyticsMarketingService amService;

	// input: identifier of chatbot
	// output: total messages for chatbot
	// action: count total messages from the first day of the bot
	@GetMapping("/chatbot/messages/count/{id}")
	public Long getCountMessagesForChatBot(@PathVariable("id") Long botId) {
		return amService.getCountMessagesForChatBot(botId);
	}
	
	// input: identifier of chatbot
	// output: total chats for chatbot
	// action: count total chats from the first day of the bot
	@GetMapping("/chatbot/chats/count/{id}")
	public Long getCountChatForChatBot(@PathVariable("id") Long botId) {		
		return amService.getCountChatsForChatBot(botId);
	}
	
	// input: identifier of chatbot
	// output: AVG chats per user
	// action: count total chats per user from the first day of the bot
	@GetMapping("/chatbot/chats/user/{botId}")
	public Double getTotalChatsPerUserForChatBot(@PathVariable("botId") Long botId) {		
		return amService.getAvgChatsPerUserForChatBot(botId);
	}
	
	// input: identifier of chatbot
	// output: AVG messages per user
	// action: count total messages per user from the first day of the bot
	@GetMapping("/chatbot/messages/user/{id}")
	public Double getTotalMessagesPerUserForChatBot(@PathVariable("id") Long botId) {		
		return amService.getAvgMessagesPerUserForChatBot(botId);
	}
	
	// input: identifier of chatbot
	// output: AVG chat time
	// action: count total chats time from the first day of the bot
	@GetMapping("/chatbot/chat/time/avg/{id}")
	public Double getChatTimeAvgForChatBot(@PathVariable("id") Long botId) {		
		return Precision.round(amService.getAvgChatTimeForChatBot(botId), 2);
	}
	
	// input: identifier of chatbot
	// output: total users (talked with the bot)
	// action: count total user from the first day of the bot
	@GetMapping("/chatbot/users/talked/total/{id}")
	public Long getTotalUsersTalkedWithChatBot(@PathVariable("id") Long botId) {
		return amService.getTotalUsersTalkedWithChatBot(botId);
	}
	
	// input: none
	// output: Integer = day, Long = total chats
	// action: count how many chats happen in each day of the week
	@GetMapping("/chats/week")
	public Map<Integer, Long> getChatsHappenEachDayOfTheWeek(){
		return amService.getChatsHappenEachDayOfTheWeek();
	}
	
	// input: none
	// output: Integer = hour, Long = total chats
	// action: count how many chats happen in each time of the day
	@GetMapping("/chats/day")
	public Map<Integer, Long> getChatsHappenEachTimeOfTheDay(){
		return amService.getChatsHappenEachTimeOfTheDay();
	}
	
	// input: identifier of chatbot
	// output: total error messages
	// action: the number of messages that the bot was not able to answer and open an 'human intervation" flag
	@GetMapping("/chatbot/error/messages/count/{id}")
	public Integer getTotalErrorMessagesForChatBot(@PathVariable("id") Long botId){
		return amService.getTotalErrorMessagesForChatBot(botId);
	}
	
	// input: none
	// output: total chats with errors
	// action: count the number of chats with happend an error message inside of them
	@GetMapping("/chats/error/total/percent")
	public Double getTotalChatsWithErrorMessages(){
		return amService.getTotalChatsWithErrorMessages();
	}
	
	// input: none
	// output: AVG percent of error messages in a chat
	// action: count the percent of errors messages in a chat 
	@GetMapping("/chats/errors/percent")
	public Double getChatsAVGErrorMessages(){
		return Precision.round(amService.getChatsAVGErrorMessages(), 2);
	}
	
	// input: identifier of chatbot, date and time
	// output: Integer = points (20) Long = total chats
	// action: get graph line how many chats over time (from the first day of the bot with time filter )
	@GetMapping("/chatbot/user/activity")
	public Map<Integer, Long> getUserActivityForChatbot(@RequestParam("id") Long botId,
														@RequestParam("filterDt") @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime filterDt){
		return amService.getUserActivity(botId, filterDt);
	}
	
	// input: none
	// output: Long = days, Long = retention
	// action: get table of the retention of the users in the last week
	@GetMapping("/retention/users")
	public Map<Long, Long> getRetentionUsersForLastWeek() {
		return amService.getRetentionUsersForLastWeek();
	}
	
	// input: none
	// output: String = name, Long = percent
	// action: get percent table (name, percent) of the locations of the users 
	@GetMapping("/locations")
	public Map<String, Double> getUsersLocations(){
		return amService.getUsersLocations();
	}
	
	// input: Integer = type(0 = days, 1 = weeks), amount (days or weeks, depend on type)
	// output: Integer = day/week, Long = summary timeOnChat
	// action: get graph line which present how long a over all chats have been taken in each day / week
	@GetMapping("/user/engagement")
	public Map<Integer, Long> getUserEngagement(@RequestParam("type") Integer type, @RequestParam("amount") Integer amount) {
		return amService.getUserEngagement(type, amount);
	}
}

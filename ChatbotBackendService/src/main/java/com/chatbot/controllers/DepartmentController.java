package com.chatbot.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.domain.Account;
import com.chatbot.domain.Answer;
import com.chatbot.domain.Department;
import com.chatbot.domain.Script;
import com.chatbot.dto.AccountDto;
import com.chatbot.dto.DepartmentAccountsDto;
import com.chatbot.dto.DepartmentScriptsDto;
import com.chatbot.services.DepartmentServiceImpl;


/**
 * This class is department controller
 *
 */
@RestController
public class DepartmentController {
	@Autowired
	public DepartmentServiceImpl departmentService;
	
	// input: identifier of department
	// output: department
	// action: get department by id
	@GetMapping("/department/find/{departmentId}")
	public Department findById(@PathVariable("departmentId") Long departmentId){
		return departmentService.findById(departmentId);
	}
	
	// input: none
    // output: list of departments
	// action: get all departments on page load
	@GetMapping("/departments")
	public List<Department> loadDepartments(){
		return departmentService.loadDepartments();
	}
	
	// input: Department (JSON)
	// output: boolean (if department saved successfully then return true)
	// action: save department to DB
	@PostMapping("/department/save")
	public Boolean saveOrUpdate(@RequestBody Department department){
		return departmentService.saveOrUpdate(department);
	}
	
	// input: identifier of department
	// output: none
	// action: delete department from the DB
	@RequestMapping("/department/delete/{departmentId}")
	public void delete(@PathVariable("departmentId") Long departmentId){
		departmentService.delete(departmentId);
	}
	
	// input: accounts in the department
	// output: boolean (if relation "department-account" saved successfully then return true)
	// action: update accounts in the department
	@PostMapping("/department/accounts/add")
	public Boolean addAccountsToDepartment(@RequestBody DepartmentAccountsDto departmentAccountDto) {
		return departmentService.addAccountsToDepartment(departmentAccountDto.getDepartmentId(), departmentAccountDto.getAccounts());
	}
	
	// input: identifier of department, identifiers of scripts
	// output: boolean (if relation "department-script" saved successfully then return true)
	// action: add scripts to department
	@PostMapping("/department/scripts/add")
	public Boolean addScriptsToDepartment(@RequestBody DepartmentScriptsDto departmentScriptDto) {
		return departmentService.addScriptsToDepartment(departmentScriptDto.getDepartmentId(), departmentScriptDto.getScripts());
	}
	
	// input: identifier of department, identifiers of accounts
	// output: answer (if relation "department-script" deleted successfully then return isSuccess true)
	// action: remove scripts from department
	@PostMapping("/department/scripts/remove")
	public Answer removeScriptsFromDepartment(@RequestBody DepartmentScriptsDto departmentScriptsDto){
		return departmentService.removeScriptsFromDepartment(departmentScriptsDto.getDepartmentId(), departmentScriptsDto.getScripts());
	}
	
	// input: identifier of department
	// output: account
	// action: get account by department's id
	@GetMapping("/department/owner/{departmentId}")
	public Account getAccountInfoByGroupId(@PathVariable("departmentId") Long groupId){
		return departmentService.getAccountInfoByDepartmentId(groupId);
	}
	
	// input: identifier of department
	// output: list of account
	// action: get accounts of department
	@GetMapping("/department/accounts/{departmentId}")
	public List<AccountDto> getAccountsByDepartmentId(@PathVariable("departmentId") Long departmentId){
		ModelMapper modelMapper = new ModelMapper();		
		List<Account> accounts = departmentService.getAccountsOfDepartment(departmentId);
		
		return accounts.stream()
			.map(account -> modelMapper.map(account, AccountDto.class))
			.collect(Collectors.toList());
	}
	
	// input: identifier of department, identifiers of accounts
	// output: boolean (if relation "department-account" deleted successfully then return true)
	// action: remove accounts from department
	@PostMapping("/department/accounts/remove")
	public Answer removeAccountsFromDepartment(@RequestBody DepartmentAccountsDto departmentAccountDto){
		return departmentService.removeAccountsFromDepartment(departmentAccountDto.getDepartmentId(), departmentAccountDto.getAccounts());
	}
	
	// input: identifier of department
	// output: list of scripts
	// action: get scripts of department
	@GetMapping("/department/scripts/{departmentId}")
	public List<Script> getScriptsOfDepartment(@PathVariable("departmentId") Long departmentId){			
		return departmentService.getScriptsOfDepartment(departmentId); 
	}
}

package com.chatbot.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.chatbot.config.SecurityUtility;
import com.chatbot.domain.Account;
import com.chatbot.dto.PasswordResetDto;
import com.chatbot.services.UserSecurityService;


/**
 * This class manage reset password
 *
 */
@Controller
@RequestMapping("/reset-password")
public class PasswordResetController {
	@Autowired
	private UserSecurityService userService;
	
	@ModelAttribute("passwordResetForm")
    public PasswordResetDto passwordReset() {
        return new PasswordResetDto();
    }	
	
	// input: token
	// output: none
	// action: send redirect to show reset-password page
	@GetMapping
    public void displayResetPasswordPage(@RequestParam(required = false) String token,
                                           Model model, HttpServletResponse response) throws IOException {
        
        Account account = userService.findUserByToken(token);
        
        String error = "";
        
        // check user by token
        if (account == null){
        	//model.addAttribute("error", "Could not find password reset token.");
        	token = "";
        	error = "account";//"Could not find password reset token";
        }  else {
        	//model.addAttribute("token", token);
        	error="none";
        }
        
        response.sendRedirect("http://viaeai.com/reset-password?token="  + token + "&error=" + error);
        //return "reset-password";
    }
	
	// input: reset-password form
	// output: none
	// action: update password and send redirect to login page
	@PostMapping
	@ResponseBody
    public void handlePasswordReset(@ModelAttribute("passwordResetForm") @Valid PasswordResetDto form,
                                      BindingResult result, HttpServletResponse response) throws IOException {
                                      //RedirectAttributes redirectAttributes) {

        if (result.hasErrors()){
            //redirectAttributes.addFlashAttribute(BindingResult.class.getName() + ".passwordResetForm", result);
            //redirectAttributes.addFlashAttribute("passwordResetForm", form);
            response.sendRedirect("http://viaeai.com/reset-password?token="  + form.getToken()+ "&error=form");//return "redirect:/reset-password?token=" + form.getToken();
            return;
        }

        Account account = userService.findUserByToken(form.getToken());
        
        if(account == null) {
        	response.sendRedirect("http://viaeai.com/reset-password?token" + form.getToken() + "&error=account");
        	return;
        }
        
        String updatedPassword = SecurityUtility.passwordEncoder().encode(form.getPassword());        
 
        account.setPassword(updatedPassword);
        account.setToken("");
        
        userService.updateAccount(account);
        

        response.sendRedirect("http://viaeai.com/login?resetSuccess");//"redirect:/login?resetSuccess";
    }


}

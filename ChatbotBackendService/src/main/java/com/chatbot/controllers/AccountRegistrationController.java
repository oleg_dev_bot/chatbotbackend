package com.chatbot.controllers;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.config.SecurityUtility;
import com.chatbot.domain.Account;
import com.chatbot.domain.Answer;
import com.chatbot.domain.EmailType;
import com.chatbot.domain.Mail;
import com.chatbot.dto.UserRegistrationDto;
import com.chatbot.enums.AnswerStatus;
import com.chatbot.services.CounterServiceImpl;
import com.chatbot.services.EmailServiceImpl;
import com.chatbot.services.UserSecurityService;



/**
 * This class manages the registration
 *
 */
@RestController
//@RequestMapping("/registration")
public class AccountRegistrationController {
	@Autowired
    private UserSecurityService userService;
	
	@Autowired
	private EmailServiceImpl emailService;
	
	@Autowired
	private CounterServiceImpl counterService;

    @ModelAttribute("account")
    public UserRegistrationDto userRegistrationDto() {
        return new UserRegistrationDto();
    }
    
    @Value("mail.from")
	private String mailFrom;

   /* @GetMapping
    public String showRegistrationForm(Model model) {
        return "registration";
    }*/

    // input: UserRegistrationDto (form), result of form binding, response
 	// output: none
 	// action: redirect user to registration page
    @PostMapping("/registration")
    public Answer registerUserAccount(@RequestBody @Valid UserRegistrationDto userDto,
                                      BindingResult result, HttpServletResponse response) throws IOException{

    	//System.out.println("EMAIL: " + userDto);
    	
    	// if form has errors then redirect to register page with error
        if (result.hasErrors()){
        	System.out.println("ERRORS!!!");
        	
        	return new Answer(AnswerStatus.DATA_ERRORS);
        }
    	    	
        Account existing = userService.loadAccountByEmail(userDto.getEmail());
        
        //account exist then redirect to register page (exist)
        if (existing != null){
            //result.rejectValue("email", null, "There is already an account registered with that email");
        	//response.sendRedirect("http://viaeai.com/register.html?exist");
        	        	
        	return new Answer(AnswerStatus.ACCOUNT_ALREDY_EXIST);
        }       
        
        Long aid = counterService.getNextIdSequence();
        
        Account account = new Account(aid, userDto.getName(), SecurityUtility.passwordEncoder().encode(userDto.getPassword()),
				"", userDto.getTitle(), userDto.getEmail(), "", "",
				0, LocalDateTime.now(), "", new String[] {"ROLE_USER"});
        
        userService.updateAccount(account);
        
        sendWelcomeMail(account);
        
        //return "redirect:/registration?success";
        
        return new Answer(AnswerStatus.OK);
    }
    
    // input: account
  	// output: none
  	// action: send email to user
    private void sendWelcomeMail(Account account) {
    	Mail mail = new Mail();
		mail.setFrom(mailFrom);
		mail.setTo(account.getEmail());
		mail.setSubject("Registration");
		mail.setEmailType(EmailType.WELCOME);
		
		Map<String, Object> model = new HashMap<>();
        model.put("account", account);
        model.put("signature", "http://viaeai.com");
                
        String url = "http://viaeai.com";
        model.put("loginUrl", url + "/");
        mail.setModel(model);
        emailService.sendEmail(mail);
    }
}

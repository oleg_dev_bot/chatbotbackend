package com.chatbot.controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.domain.Account;
import com.chatbot.domain.EmailType;
import com.chatbot.domain.Mail;
import com.chatbot.repositories.AccountRepository;
import com.chatbot.services.EmailService;
import com.chatbot.services.EmailServiceImpl;
import com.chatbot.services.UserSecurityService;


/**
 * This class manages password 
 *
 */
@RestController
public class PasswordController {
	@Autowired
	private UserSecurityService accountService;
	
	@Value("mail.from")
	private String mailFrom;
	
	@Autowired
	private EmailServiceImpl emailService;

	// input: email of user
	// output: HttpStatus.NOT_FOUND, HttpStatus.FOUND
	// action: receive email of user and send message to this one
	@PostMapping("/forgot")
	public ResponseEntity processForgotPassword(@RequestParam("email") String email, HttpServletRequest http) {
		Account account = accountService.loadAccountByEmail(email);
		ResponseEntity re = new ResponseEntity(HttpStatus.NOT_FOUND);
		
		// check account
		if(account == null) {
			return re;
		}
		
		String resetToken = UUID.randomUUID().toString();
		account.setToken(resetToken);
		accountService.updateAccount(account);
		
		Mail mail = new Mail();
		mail.setFrom(mailFrom);
		mail.setTo(account.getEmail());
		mail.setSubject("Password reset request");
		mail.setEmailType(EmailType.RESET);
		
		Map<String, Object> model = new HashMap<>();
        model.put("token", resetToken);
        model.put("account", account);
        model.put("signature", "http://viaeai.com");
                
        String url = http.getScheme() + "://" + http.getServerName() + ":" + http.getServerPort();
        model.put("resetUrl", url + "/reset-password?token=" + resetToken);
        mail.setModel(model);
        emailService.sendEmail(mail);
		
		return ResponseEntity.ok(HttpStatus.FOUND);
		
	}
}

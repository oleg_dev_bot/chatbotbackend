package com.chatbot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.domain.Chat;
import com.chatbot.services.IndexService;
import com.chatbot.services.MonitorService;


/**
 * This class manages the ROI
 *
 */
@RestController
public class MonitorController {
	@Autowired
	public MonitorService monitorService;
	
	// input: none
	// output: ROI
	// action: get ROI percent in last day
	@RequestMapping("/roi")
	public Double getROI() {
		return monitorService.getROI();
	}
}

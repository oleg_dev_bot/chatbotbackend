package com.chatbot.controllers;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.domain.Account;
import com.chatbot.domain.AccountConnection;
import com.chatbot.domain.AccountSettings;
import com.chatbot.dto.AccountDepartmentsDto;
import com.chatbot.dto.ActionDto;
import com.chatbot.dto.DepartmentAccountsDto;
import com.chatbot.dto.DepartmentDto;
import com.chatbot.services.AccountService;

/**
 * This controller manages accounts
 *
 */
@RestController
public class AccountController {
	@Autowired
	private AccountService accountService;

	// input: identifier of account (accountId)
	// output: none
	// action: delete account from DB
	@RequestMapping("/account/delete/{accountId}")
	public void delete(@PathVariable("accountId") Long accountId) {
		accountService.delete(accountId);
	}
	
	// input: amount of last events
	// output: list of events
	// action: get "top" of last events.If top=0 then it returns all events from DB (table accountConnection) 
	@GetMapping("/account/events/limit/{top}")
	public List<AccountConnection> getAllAccountConnection(@PathVariable("top") Integer top){
		return accountService.getTopEvents(top);
	}
	
	// input: none
	// output: list of accounts
	// action: get list of accounts from DB
	@GetMapping("/account/get/all")
	public List<Account> getAllAccounts(){
		return accountService.getAllAccounts();
	}
	
	// input: identifier of account (accountId)
	// output: list account's events (List of AccountConnection)
	// action: get list of events by account's id
	@GetMapping("/account/id/events/{accountId}")
	public List<AccountConnection> getEventsOfAccountById(@PathVariable("accountId") Long accountId){
		return accountService.getAllEventsOfAccountById(accountId);
	}
	
	// input: name of account (accountName)
	// output: list account's events (List of AccountConnection)
	// action: get list of events by account's name
	@GetMapping("/account/name/events/{accountName}")
	public List<AccountConnection> getEventsOfAccountByName(@PathVariable("accountName") String accountName){
		return accountService.getAllEventsOfAccountByName(accountName);
	}
	
	// input: account Account (mapped from JSON), boolean isPasswordUpdate
	// output: boolean (if true then account updated successfully)
	// action: update account in the DB
	@PostMapping("/account/update")
	public Boolean updateAccount(@RequestBody Account account, @RequestParam("isPasswordUpdate") Boolean isPasswordUpdate) {
		return accountService.update(account, isPasswordUpdate);
	}
	
	// input: none
	// output: total amount of events (login/logout)
	// action: get total amount of events from the DB
	@GetMapping("/events/total")
	public Long totalEvents() {
		return accountService.totalEvents();
	}
	
	// input: number and size of page (page=...&size=... then convert to Pageable).
	// output: page of events
	// action: get page of events
	@GetMapping("/events/page")
	public Page<AccountConnection> getPageOfEvents(Pageable pageable){
		return accountService.loadEventsByPage(pageable);
	}
	
	// input: settings of account
	// output: boolean (if true then settings of account updated successfully)
	// action: update account's settings
	@PostMapping("/account/settings/save")
	public Boolean saveOrUpdateSettingsOfAccount(@RequestBody AccountSettings accountSettings){
		return accountService.saveOrUpdateAccountSettings(accountSettings);
	}
	
	// input: account's identifier
	// output: graph, single point or empty
	// action: get amount account's chat per day in the period
	@GetMapping("/account/chats/{accountId}")
	public Map<Integer, Long> getAmountOfChatsForAccount(@PathVariable("accountId") Long accountId,
												 @RequestParam(name = "date") 
												 @DateTimeFormat(iso = ISO.DATE)
    											 LocalDateTime endDt){
			return accountService.getAmountOfChatsForAccount(accountId, endDt);
	}
	
	// input: account's identifier
	// output: list of related departments
	// action: get related departments
	@GetMapping("/account/department/relation/{accountId}")
	public List<DepartmentDto> getDepartmentsRelatedToAccount(Long accountId){
		return accountService.getDepartmentsRelatedToAccount(accountId);
	}
	
	// input: account's identifier
	// output: none
	// action: delete account from department
	@RequestMapping("/account/department/delete/{accountId}")
	public void deleteAccountFromDepartment(@PathVariable("accountId") Long accountId) {
			accountService.deleteAccountFromDepartment(accountId);
	}
	
	// input: none
	// output: size of actions
	// action: get size of actions
	@GetMapping("/account/actions/size")
	public Long getSizeOfActions() {
		return accountService.getSizeOfActions();
	}
	
	// input: number and size of page (page=...&size=... then convert to Pageable).
	// output: page of actions
	// action: get page of actions
	@GetMapping("/account/action/page")
	public List<ActionDto> loadActionByPage(Pageable pageable){
		return accountService.loadActionByPage(pageable);	
	}
	
	// input: departments related to account
	// output: boolean (if relation "department-account" saved successfully then return true)
	// action: update departments related to the account
	@PostMapping("/account/departments/relation")
	public Boolean updateDepartmentsRelated(@RequestBody AccountDepartmentsDto accountDepartmentsDto) {
			return accountService.updateDepartmentsRelatedAccount(accountDepartmentsDto.getAccountId(), accountDepartmentsDto.getDepartments());
	}
}

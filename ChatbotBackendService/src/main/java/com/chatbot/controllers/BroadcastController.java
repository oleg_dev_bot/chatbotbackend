package com.chatbot.controllers;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.domain.Broadcast;
import com.chatbot.domain.Department;
import com.chatbot.domain.Group;
import com.chatbot.dto.BroadcastDto;
import com.chatbot.dto.BroadcastFullInfoDto;
import com.chatbot.dto.BroadcastMessageDto;
import com.chatbot.services.BroadcastGraphServiceImpl;
import com.chatbot.services.BroadcastServiceImpl;
import com.chatbot.services.DepartmentServiceImpl;
import com.chatbot.services.MessageService;

@RestController
public class BroadcastController {
	@Autowired
	public BroadcastServiceImpl broadcastService;
	@Autowired
	public BroadcastGraphServiceImpl broadcastGraphService;
	@Autowired
	public MessageService messageService;
	
	
	// input: none
    // output: list of broadcasts
	// action: get broadcasts on page load
	@GetMapping("/broadcasts")
	public List<Broadcast> loadBroadcasts(){
		return broadcastService.loadBroadcasts();
	}
	
	// input: Broadcast (JSON)
	// output: boolean (if broadcast saved successfully then return true)
	// action: save broadcast to DB
	@PostMapping("/broadcast/save")
	public Boolean saveOrUpdate(@RequestBody Broadcast broadcast){
		return broadcastService.saveOrUpdate(broadcast);
	}
	
	// input: identifier of broadcast
	// output: none
	// action: delete broadcast from the DB
	@RequestMapping("/broadcast/delete/{broadcastId}")
	public void delete(@PathVariable("broadcastId") Long broadcastId){
		broadcastService.delete(broadcastId);
	}
	
	// input: identifier of broadcast
	// output: broadcast's name
	// action: get broadcast's name by id
	@GetMapping("/broadcast/name/{broadcastId}")
	public String getBroadcastNameById(@PathVariable("broadcastId") Long broadcastId) {
		return broadcastService.getBroadcastNameById(broadcastId);
	}
	
	// input: identifier of broadcast
	// output: broadcast's status
	// action: get broadcast's status by id
	@GetMapping("/broadcast/status/{broadcastId}")
	public Integer getBroadcastStatusById(@PathVariable("broadcastId") Long broadcastId) {
		return broadcastService.getBroadcastStatus(broadcastId);
	}
	
	// input: identifier of broadcast
	// output: broadcast's group
	// action: get broadcast's group
	@GetMapping("/broadcast/group/{relatedGroupId}")
	public Group getBroadcastGroup(@PathVariable("relatedGroupId") Long relatedGroupId) {
		return broadcastService.getBroadcastGroup(relatedGroupId);
	}
	
	// input: identifier of broadcast
	// output: amount of sent messages
	// action: get broadcast's sent messages
	@GetMapping("/broadcast/messages/sent/{broadcastId}")
	public Integer getBroadcastMessagesSent(@PathVariable("broadcastId") Long broadcastId) {
		return broadcastService.HowManyMessagesSent(broadcastId);
	}
	
	// input: identifier of broadcast
	// output: amount of opend chats
	// action: get opend chats
	@GetMapping("/broadcast/chats/opend/{broadcastId}")
	public Integer getHowManyOpendChats(@PathVariable("broadcastId") Long broadcastId) {
		return broadcastService.HowManyChatsOpend(broadcastId);
	}
	
	// input: identifier of broadcast
	// output: broadcast's script id + name (Long = id, String = name)
	// action: get broadcast's script id and name
	@GetMapping("/broadcast/script/name/{broadcastId}")
	public Map<Long, String> getBroadcastScriptName(Long broadcastId) {
		return broadcastService.getBroadcastScriptName(broadcastId);
	}
	
	// input: identifier of broadcast
	// output: broadcast's platform name
	// action: get which platform broadcast uses
	@GetMapping("/broadcast/platform/{broadcastId}")
	public String getBroadcastPlatform(Long broadcastId) {
		return broadcastService.getBroadcastPlatform(broadcastId);
	}
	
	// input: identifier of broadcast and date and time filter
	// output: broadcast open graph
	// action: get how much opend in percent
	@GetMapping("/broadcast/open/graph/{broadcastId}")
	public Map<Integer, Double> getBroadcastOpenGraph(@PathVariable("broadcastId") Long broadcastId, 
													  @RequestParam("filter") @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime filter) {
		return broadcastGraphService.getBroadcastOpenGraph(broadcastId, filter);
	}
	
	// input: identifier of broadcast and date and time filter
	// output: broadcast sent graph
	// action: get how much messages sent in percent
	@GetMapping("/broadcast/sent/graph/{broadcastId}")
	public Map<Integer, Double> getBroadcastSentGraph(@PathVariable("broadcastId") Long broadcastId, 
													  @RequestParam("filter") @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime filter) {
		return broadcastGraphService.getBroadcastSentGraph(broadcastId, filter);
	}
	
	// input: identifier of broadcast and date and time filter
	// output: broadcast device pie graph
	// action: get percent and amount messages from devices
	@GetMapping("/broadcast/device/graph/{broadcastId}")
	public Map<String, String> getBroadcastDeviceGraph(@PathVariable("broadcastId") Long broadcastId, 
													  @RequestParam("filter") @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime filter) {
		return broadcastGraphService.getBroadcastDeviceGraph(broadcastId, filter);
	}
	
	// input: identifier of broadcast and date and time filter
	// output: how many chats hit "end"
	// action: get how many chats hit "end"
	@GetMapping("/broadcast/convert/graph/{broadcastId}")
	public Map<Integer, Integer> getBroadcastConvertGraph(@PathVariable("broadcastId") Long broadcastId, 
													  @RequestParam("filter") @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime filter) {
		return broadcastGraphService.getBroadcastConvertGraph(broadcastId, filter);
	}
	
	// input: metadata of broadcast
	// output: broadcast
	// action: save broadcast's metadata to DB
	@PostMapping("/broadcast/metadata/save")
	public Broadcast saveBroadcastMetadata(@RequestBody BroadcastDto broadcastMetadata) {
		return broadcastService.saveBroadcastMetadata(broadcastMetadata);
	}
	
	// input: identifier of broadcast and message
	// output: boolean (true if save successfully)
	// action: add message to the broadcast
	@PostMapping("/broadcast/message/save")
	public void saveBroadcastMetadata(@RequestBody BroadcastMessageDto broadcastMessage) {
		messageService.saveToBroadcast(broadcastMessage);
	}
	
	// input: identifier of broadcast
	// output: broadcast
	// action: load broadcast from DB
	@GetMapping("/broadcast/load/{broadcastId}")
	public Broadcast loadBroadcastById(@PathVariable("broadcastId") Long broadcastId) {
		return broadcastService.loadBroadcastById(broadcastId);
	}
	
	@GetMapping("/broadcast/load/full/{broadcastId}")
	public BroadcastFullInfoDto loadFullInfoAboutBroadcastById(@PathVariable("broadcastId") Long broadcastId) {
		return broadcastService.loadFullInfoAboutBroadcastById(broadcastId);
	}
}

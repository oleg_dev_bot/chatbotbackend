package com.chatbot.controllers;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.domain.Message;
import com.chatbot.domain.Support;
import com.chatbot.repositories.MessagesRepository;
import com.chatbot.services.MessageService;
import com.chatbot.services.SupportServiceImpl;
import com.chatbot.services.UserSecurityService;

@Controller
public class TestController {
	@Autowired
	UserSecurityService userService;
	@Autowired
	private MessageService messageService;
	@Autowired
	private SupportServiceImpl supportService;
	
	@RequestMapping("/test")
	public String test(Model model, HttpServletResponse response) throws IOException {
		//response.sendRedirect("http://viaeai.com");
		model.addAttribute("user", userService.loadAccountByEmail("joe@gmail.com"));
		
		return "email/email-template";
	}
	
	@PostMapping("/test/message/save")
	@ResponseBody
	public void saveOrUpdate(@RequestBody Message message) {
	    messageService.save(message);
	}
	
	@RequestMapping("/test/grouped/support")
	public List<Support> getGroupedSupportList(){
		return supportService.getGroupedSupportList();
	}
	
	@RequestMapping("/test/page")
	@ResponseBody
	public String testPage(){
		return "HELLO WORLD FROM AWS!";
	}
}

package com.chatbot.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.dto.ChatbotDto;
import com.chatbot.services.ChatbotServiceImpl;

@RestController
public class ChatbotController {
	@Autowired
	private ChatbotServiceImpl chatbotService;
	
	@GetMapping("/chatbot/scripts/dictionaries/{chatbotId}")
	ChatbotDto loadScriptsAndDictionariesOfChatbot(Long chatbotId) {
		return chatbotService.loadScriptsAndDictionariesOfChatbot(chatbotId);
	}
}

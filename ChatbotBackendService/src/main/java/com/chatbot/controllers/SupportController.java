package com.chatbot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.domain.Department;
import com.chatbot.domain.Support;
import com.chatbot.dto.ChatbotSupportDto;
import com.chatbot.services.SupportServiceImpl;

/**
 * This class handles requests for support
 *
 */
@RestController
public class SupportController {
	
	@Autowired
	public SupportServiceImpl supportService;
	
	// input: none
    // output: list of open tickets
	// action: get list of open tickets
	@GetMapping("/tickets/open/get")
	public List<Support> loadOpenTickets(){
		return supportService.loadOpenTickets();
	}
	
	// input: none
    // output: list of closed tickets
	// action: get list of closed tickets
	@GetMapping("/tickets/closed/get")
	public List<Support> loadClosedTickets(){
		return supportService.loadClosedTickets();
	}
	
	// input: Support (JSON)
	// output: boolean (if ticket saved successfully then return true)
	// action: save ticket to DB
	@PostMapping("/ticket/save")
	public Boolean saveOrUpdate(@RequestBody Support ticket){
		return supportService.saveOrUpdate(ticket);
	}
	
	// input: identifier of ticket
	// output: none
	// action: delete ticket from the DB
	@RequestMapping("/ticket/delete/{ticketId}")
	public void delete(@PathVariable("ticketId") Long ticketId){
		supportService.delete(ticketId);
		
	}
	
	// input: identifier of chatbot
	// output: list of tickets
	// action: get all tickets of chatbot
	@RequestMapping("/tickets/chatbot/{chatbotId}")
	public List<Support> getAllTicketsOfChatbot(@PathVariable("chatbotId") Long chatbotId){
		return supportService.getAllTicketsOfChatbot(chatbotId);
	}
	
	// input: identifier of chatbot, ticket
	// output: result of saving (true if saved successfully)
	// action: save support of chatbot
	public Boolean saveChatbotSupport(ChatbotSupportDto cs) {
		return supportService.saveChatbotSupport(cs.getChatbotId(), cs.getTicket());
	}
}

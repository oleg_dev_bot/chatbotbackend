package com.chatbot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.domain.Account;
import com.chatbot.domain.Answer;
import com.chatbot.domain.Department;
import com.chatbot.domain.Group;
import com.chatbot.domain.User;
import com.chatbot.dto.DepartmentScriptsDto;
import com.chatbot.dto.GroupUsersDto;
import com.chatbot.services.DepartmentServiceImpl;
import com.chatbot.services.GroupServiceImpl;


/**
 * This class manages groups
 *
 */
@RestController
public class GroupController {
	@Autowired
	public GroupServiceImpl groupService;
	
	// input: none
    // output: list of groups
	// action: get all groups on page load
	@GetMapping("/groups")
	public List<Group> loadGroups(){
		return groupService.loadGroups();
	}
	
	// input: Group (JSON)
	// output: boolean (if group saved successfully then return true)
	// action: save group to DB
	@PostMapping("/group/save")
	public Boolean saveOrUpdate(@RequestBody Group group){
		return groupService.saveOrUpdate(group);
	}
	
	// input: identifier of group
	// output: none
	// action: delete group from the DB
	@RequestMapping("/group/delete/{groupId}")
	public void delete(@PathVariable("groupId") Long groupId){
		groupService.delete(groupId);
	}
	
	// input: identifier of group, identifiers of users
	// output: boolean (if relation "group-user" saved successfully then return true)
	// action: add users to group
	@PostMapping("/group/users/add")
	public Boolean addUsersToGroup(@RequestBody GroupUsersDto groupUsersDto) {
		return groupService.addUsersToGroup(groupUsersDto.getGroupId(), groupUsersDto.getUsers());
	}
	
	// input: identifier of group
	// output: list of users
	// action: get users of group
	@PostMapping("/group/users/{groupId}")
	public List<User> getUsersOfGroup(@PathVariable("groupId") Long groupId) {
		return groupService.getUsersOfGroup(groupId);
	}
	
	// input: identifier of group, identifiers of users
	// output: Answer (if users removed successfully then isSuccess true)
	// action: remove users from group
	@PostMapping("/group/users/delete")
	public Answer removeUsersFromGroup(@RequestBody GroupUsersDto groupUsersDto) {
		return groupService.removeUsersFromGroup(groupUsersDto.getGroupId(), groupUsersDto.getUsers());
	}
	
	// input: identifier of group
	// output: account
	// action: get account by group's id
	@GetMapping("/group/owner/{groupId}")
	public Account getAccountInfoByGroupId(@PathVariable("groupId") Long groupId){
		return groupService.getAccountInfoByGroupId(groupId);
	}
	
	// input: identifier of group
	// output: group
	// action: get info about group by id
	@GetMapping("/group/find/{groupId}")
	public Group fingGroupById(@PathVariable("groupId") Long groupId){
		return groupService.findById(groupId);
	}
}

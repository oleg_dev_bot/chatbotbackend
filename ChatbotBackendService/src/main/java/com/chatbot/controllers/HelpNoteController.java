package com.chatbot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chatbot.domain.Department;
import com.chatbot.domain.HelpNote;
import com.chatbot.services.DepartmentServiceImpl;
import com.chatbot.services.HelpNoteServiceImpl;


/**
 * Tnis class handles requests for help notes
 *
 */
@RestController
public class HelpNoteController {
	@Autowired
	public HelpNoteServiceImpl helpNoteService;
	
	// input: none
    // output: list of help notes
	// action: get all help notes on page load
	@GetMapping("/help/notes")
	public List<HelpNote> loadHelpNotes(){
		return helpNoteService.loadHelpNotes();
	}
	
	// input: HelpNote (JSON)
	// output: boolean (if help note saved successfully then return true)
	// action: save help note to DB
	@PostMapping("/help/note/save")
	public Boolean saveOrUpdate(@RequestBody HelpNote helpNote){
		return helpNoteService.saveOrUpdate(helpNote);
	}
	
	// input: identifier of help note
	// output: none
	// action: delete help note from the DB
	@RequestMapping("/help/note/delete/{id}")
	public void delete(@PathVariable("id") Long helpNoteId){
		helpNoteService.delete(helpNoteId);
	}
}

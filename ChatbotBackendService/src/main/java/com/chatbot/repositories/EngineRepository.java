package com.chatbot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.ChatMessage;
import com.chatbot.domain.Engine;

public interface EngineRepository extends MongoRepository<Engine, Long>{
	Engine findById(Long id);
}

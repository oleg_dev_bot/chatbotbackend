package com.chatbot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.Chat;
import com.chatbot.domain.Counter;

public interface CounterRepository extends MongoRepository<Counter, Long>{

}

package com.chatbot.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.AccountAction;
import com.chatbot.domain.AccountConnection;

public interface AccountActionRepository extends MongoRepository<AccountAction, Long>{
	List<AccountAction> findByAccountId(Long accountId);
}

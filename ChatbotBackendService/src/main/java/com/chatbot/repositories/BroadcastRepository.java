package com.chatbot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.Broadcast;

public interface BroadcastRepository extends MongoRepository<Broadcast, Long>{
	Broadcast findById(Long broadcastId);
}

package com.chatbot.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.Action;
import com.chatbot.domain.ChatbotChat;

public interface ChatbotChatRepository extends MongoRepository<ChatbotChat, Long>{
	List<ChatbotChat> findByChatbotId(Long chatbotId);
	ChatbotChat findByChatbotIdAndChatId(Long chatbotId, Long chatId);
}

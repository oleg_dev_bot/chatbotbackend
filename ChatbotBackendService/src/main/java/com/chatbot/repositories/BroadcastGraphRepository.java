package com.chatbot.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.BroadcastGraph;

public interface BroadcastGraphRepository extends MongoRepository<BroadcastGraph, Long> {
	List<BroadcastGraph> findByBroadcastIdOrderByTimeStampAsc(Long broadcastId);
}

package com.chatbot.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.AccountSettings;

public interface AccountSettingsRepository extends MongoRepository<AccountSettings, Long> {
	List<AccountSettings> findByAccountId(Long accountId);
}

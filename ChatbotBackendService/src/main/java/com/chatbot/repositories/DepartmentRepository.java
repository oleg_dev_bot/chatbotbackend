package com.chatbot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.Chat;
import com.chatbot.domain.Department;

public interface DepartmentRepository extends MongoRepository<Department, Long>{
	Department findById(Long departmentId);
}

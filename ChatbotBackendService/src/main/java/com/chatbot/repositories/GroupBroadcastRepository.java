package com.chatbot.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.GroupBroadcast;

public interface GroupBroadcastRepository extends MongoRepository<GroupBroadcast, Long> {
	GroupBroadcast findByBroadcastId(Long broadcastId);
	void deleteByBroadcastId(Long broadcastId);
}

package com.chatbot.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.ChatbotAccount;

public interface ChatbotAccountRepository extends MongoRepository<ChatbotAccount, Long>{
	List<ChatbotAccount> findByAccountId(Long accountId);
}

package com.chatbot.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.Action;
import com.chatbot.domain.ChatbotUser;

public interface ChatbotUserRepository extends MongoRepository<ChatbotUser, Long>{
	List<ChatbotUser> findByChatbotId(Long botId);
}

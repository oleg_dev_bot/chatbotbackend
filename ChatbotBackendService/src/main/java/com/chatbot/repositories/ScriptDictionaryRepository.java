package com.chatbot.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.ScriptDictionary;

public interface ScriptDictionaryRepository extends MongoRepository<ScriptDictionary, Long>{
	void deleteByDictionaryId(Long dictionaryId);
	List<ScriptDictionary> findByScriptId(Long scriptId);
}

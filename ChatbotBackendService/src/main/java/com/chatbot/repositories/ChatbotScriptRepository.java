package com.chatbot.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.ChatbotScript;
import com.chatbot.domain.Script;

public interface ChatbotScriptRepository extends MongoRepository<ChatbotScript, Long> {
	List<ChatbotScript> findByChatbotId(Long chatbotId);
	void deleteByScriptId(Long scriptId);
	ChatbotScript findByChatbotIdAndScriptId(Long chatbotId, Long scriptId);
}

package com.chatbot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.AccountAlert;
import com.chatbot.domain.Alert;

public interface AlertRepository extends MongoRepository<Alert, Long>{

}

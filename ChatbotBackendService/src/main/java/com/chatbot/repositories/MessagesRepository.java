package com.chatbot.repositories;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.Chat;
import com.chatbot.domain.Message;

public interface MessagesRepository extends MongoRepository<Message, Long>{
	List<Message> findByTimeBetween(LocalDateTime start, LocalDateTime current);
	Message findById(Long id);
	List<Message> findByCreatorId(Long botId);
}

package com.chatbot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.Script;

public interface ScriptRepository extends MongoRepository<Script, Long>{
	Script findById(Long scriptId);
}

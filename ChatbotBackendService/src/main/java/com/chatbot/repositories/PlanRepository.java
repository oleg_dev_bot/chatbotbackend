package com.chatbot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.Plan;

public interface PlanRepository extends MongoRepository<Plan, Long>{

}

package com.chatbot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.Account;
import com.chatbot.domain.Group;

public interface GroupRepository extends MongoRepository<Group, Long>{
	Group findById(Long groupId);
}

package com.chatbot.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.chatbot.domain.AccountConnection;

public interface EventsPageableRepository extends PagingAndSortingRepository<AccountConnection, Long>{

}

package com.chatbot.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.Group;
import com.chatbot.domain.GroupUser;

public interface GroupUserRepository extends MongoRepository<GroupUser, Long>{
	void deleteByGroupId(Long groupId);
	void deleteByGroupIdAndUserId(Long groupId, Long userId);
	List<GroupUser> findByGroupId(Long groupId);
	GroupUser findByGroupIdAndUserId(Long groupId, Long userId);
}

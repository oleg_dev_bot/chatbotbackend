package com.chatbot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.ChatFiltered;

public interface ChatFilteredRepository extends MongoRepository<ChatFiltered, Long>{

}

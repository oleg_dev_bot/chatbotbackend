package com.chatbot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.HelpNote;

public interface HelpNoteRepository extends MongoRepository<HelpNote, Long>{

}

package com.chatbot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.User;

public interface UserRepository extends MongoRepository<User, Long>{
	User findByName(String name);
	User findById(Long id);
	Long countByIsMale(int isMale);
	Long countByIsOnline(boolean isOnline);
}

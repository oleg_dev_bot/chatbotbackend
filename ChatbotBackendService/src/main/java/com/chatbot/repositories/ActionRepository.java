package com.chatbot.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.AccountAction;
import com.chatbot.domain.AccountConnection;
import com.chatbot.domain.Action;

public interface ActionRepository extends MongoRepository<Action, Long>{
	List<Action> findTop5ByOwnerIdOrderByDateDesc(Long Id);
	Action findById(Long id);
}

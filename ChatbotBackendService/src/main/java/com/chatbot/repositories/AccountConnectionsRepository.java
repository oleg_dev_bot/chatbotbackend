package com.chatbot.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.Account;
import com.chatbot.domain.AccountConnection;

public interface AccountConnectionsRepository extends MongoRepository<AccountConnection, Long>{
	List<AccountConnection> findTop1ByAccountIdOrderByTimeDateDesc(Long accountId);
	List<AccountConnection> findAllByOrderByTimeDateDesc();
	List<AccountConnection> findByAccountId(Long accountId);
}

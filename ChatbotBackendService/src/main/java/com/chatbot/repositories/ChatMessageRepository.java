package com.chatbot.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.Chat;
import com.chatbot.domain.ChatMessage;

public interface ChatMessageRepository extends MongoRepository<ChatMessage, Long>{
	List<ChatMessage> findByChatId(Long chatId);
	ChatMessage findByMessageId(Long messageId);
}

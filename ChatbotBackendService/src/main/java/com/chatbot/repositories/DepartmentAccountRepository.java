package com.chatbot.repositories;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.Department;
import com.chatbot.domain.DepartmentAccount;

public interface DepartmentAccountRepository extends MongoRepository<DepartmentAccount, Long>{
	Integer countByDepartmentId(Long departmentId);
	List<DepartmentAccount> findByAccountId(Long accountId);
	List<DepartmentAccount> findByDepartmentId(Long accountId);
	void deleteByAccountId(Long accountId);
	void deleteByDepartmentId(Long departmentId);
	void deleteByDepartmentIdAndAccountId(Long departmentId, Long AccountId);
	DepartmentAccount findByDepartmentIdAndAccountId(Long departmentId, Long accountId);
}

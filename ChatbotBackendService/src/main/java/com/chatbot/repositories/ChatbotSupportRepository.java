package com.chatbot.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.ChatbotSupport;

public interface ChatbotSupportRepository extends MongoRepository<ChatbotSupport, Long> {
	List<ChatbotSupport> findByChatbotId(Long chatbotId);
	void deleteBySupportId(Long supportId);
}

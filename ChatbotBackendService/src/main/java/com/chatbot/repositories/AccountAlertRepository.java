package com.chatbot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.AccountAction;
import com.chatbot.domain.AccountAlert;

public interface AccountAlertRepository extends MongoRepository<AccountAlert, Long>{

}

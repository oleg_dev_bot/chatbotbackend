package com.chatbot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.BroadcastMessage;

public interface BroadcastMessageRepository extends MongoRepository<BroadcastMessage, Long>{
	void deleteBybroadcastId(Long broadcastId);
	void deleteByMessageId(Long broadcastId);
	void deleteByMessageIdAndBroadcastId(Long messageId, Long broadcastId);
}

package com.chatbot.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.Support;

public interface SupportRepository extends MongoRepository<Support, Long>{
	List<Support> findByIsOpen(boolean isOpen);
}

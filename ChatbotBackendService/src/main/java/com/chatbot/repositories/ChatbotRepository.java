package com.chatbot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.Action;
import com.chatbot.domain.Chatbot;

public interface ChatbotRepository extends MongoRepository<Chatbot, Long>{
	Chatbot findById(Long botId);
}

package com.chatbot.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.DepartmentAccount;
import com.chatbot.domain.DepartmentScript;

public interface DepartmentScriptRepository extends MongoRepository<DepartmentScript, Long>{
	Integer countByDepartmentId(Long departmentId);
	List<DepartmentScript> findByScriptId(Long scriptId);
	List<DepartmentScript> findByDepartmentId(Long scriptId);
	DepartmentScript findByDepartmentIdAndScriptId(Long departmentId, Long scriptId);
	void deleteByScriptId(Long scriptId);
	void deleteByDepartmentId(Long departmentId);
	void deleteByDepartmentIdAndScriptId(Long departmentId, Long scriptId);
}

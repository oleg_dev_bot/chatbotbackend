package com.chatbot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.Dictionary;
import com.chatbot.domain.Script;

public interface DictionaryRepository extends MongoRepository<Dictionary, Long> {
	Dictionary findById(Long scriptId);
}

package com.chatbot.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.ChatbotDictionary;
import com.chatbot.domain.ChatbotScript;

public interface ChatbotDictionaryRepository extends MongoRepository<ChatbotDictionary, Long>{
	List<ChatbotDictionary> findByChatbotId(Long chatbotId);
	void deleteByDictionaryId(Long dictionaryId);
}

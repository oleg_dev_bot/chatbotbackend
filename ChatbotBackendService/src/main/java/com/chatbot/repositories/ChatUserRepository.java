package com.chatbot.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.ChatUser;
import com.chatbot.domain.ChatbotUser;

public interface ChatUserRepository extends MongoRepository<ChatUser, Long>{
	List<ChatUser> findByUserId(Long userId);
}

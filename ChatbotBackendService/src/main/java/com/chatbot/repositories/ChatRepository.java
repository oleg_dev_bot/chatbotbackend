package com.chatbot.repositories;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.chatbot.domain.Chat;

public interface ChatRepository extends MongoRepository<Chat, Long> {
	Long countByIsHuman(boolean isHuman);
	Long countByStartChatTimeBetween(LocalDateTime start, LocalDateTime current);
	Long countByIsHumanTalkedIsTrueAndStartChatTimeBetween(LocalDateTime dt1, LocalDateTime dt2);
	List<Chat> findByBotId(Long botId);
	List<Chat> findByUserId(Long userId);
	Long countByIsOpen(boolean isOpen);
	List<Chat> findByMessagesToHitGreaterThanAndStartChatTimeBetween(Integer amount, LocalDateTime start, LocalDateTime current);
	List<Chat> findByStartChatTimeBetween(LocalDateTime start, LocalDateTime current);
	List<Chat> findByBotIdOrderByStartChatTimeAsc(Long botId);
	Chat findTop1ByBotIdOrderByStartChatTimeAsc(Long botId);
	Chat findById(Long chatId);
	Chat findByIdAndIsOpenIsTrue(Long chatId);
	List<Chat> findByMechanismGreaterThan(int mechanism);
	 
}

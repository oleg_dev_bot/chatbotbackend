package com.chatbot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.chatbot.domain.Account;

public interface AccountRepository extends MongoRepository<Account, Long>{
	Account findByEmail(String email);
	Account findUserByToken(String resetToken);
	Account findByName(String resetToken);
	Account findById(Long accountId);
}

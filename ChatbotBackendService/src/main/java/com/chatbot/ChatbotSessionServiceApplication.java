package com.chatbot;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.authority.AuthorityUtils;

import com.chatbot.config.SecurityUtility;
import com.chatbot.domain.Account;
import com.chatbot.domain.Chat;
import com.chatbot.domain.ChatMessage;
import com.chatbot.domain.ChatUser;
import com.chatbot.domain.ChatbotChat;
import com.chatbot.domain.ChatbotUser;
import com.chatbot.domain.Counter;
import com.chatbot.domain.Department;
import com.chatbot.domain.Engine;
import com.chatbot.domain.Group;
import com.chatbot.domain.GroupUser;
import com.chatbot.domain.User;
import com.chatbot.repositories.AccountRepository;
import com.chatbot.repositories.ChatMessageRepository;
import com.chatbot.repositories.ChatRepository;
import com.chatbot.repositories.ChatUserRepository;
import com.chatbot.repositories.ChatbotChatRepository;
import com.chatbot.repositories.ChatbotUserRepository;
import com.chatbot.repositories.CounterRepository;
import com.chatbot.repositories.EngineRepository;
import com.chatbot.repositories.GroupRepository;
import com.chatbot.repositories.GroupUserRepository;
import com.chatbot.repositories.UserRepository;
import com.chatbot.services.CounterServiceImpl;
import com.chatbot.services.DepartmentServiceImpl;
import com.chatbot.services.UserSecurityService;

/**
*  This class is a main class for starting application.
* 
*/
@SpringBootApplication
public class ChatbotSessionServiceApplication implements CommandLineRunner{
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private ChatRepository chatRepository;
	@Autowired
	private ChatUserRepository chatUserRepository;
	@Autowired
	private EngineRepository engineRepository;
	@Autowired
	private ChatMessageRepository chatMessageRepository;
	@Autowired
	private GroupRepository groupRepository;
	@Autowired
	private GroupUserRepository groupUserRepository;
	@Autowired
	private ChatbotUserRepository chatbotUserRepository;
	@Autowired
	private ChatbotChatRepository chatbotChatRepository;
	@Autowired
	private CounterRepository counterRepository;
	@Autowired
	private CounterServiceImpl cs;
	

	public static void main(String[] args) {
		SpringApplication.run(ChatbotSessionServiceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		List<Counter> counterList = counterRepository.findAll();
		
		if(counterList.size() <= 0) {
			Counter counter = new Counter();
			counter.setName("sequence_id");
			counter.setSequence(1L);
			
			counterRepository.save(counter);
			
		}
		
		
		/*List<Account> list = new ArrayList();
		
		Account ac1 = new Account(1L, "account_Joe",SecurityUtility.passwordEncoder().encode("p"),
				"xcrethoi76ll", "title1", "joe@gmail.com", "567-678-98-78", "",
				1, LocalDateTime.now(), "", new String[] {"ROLE_USER"});
		list.add(ac1);
		
		Account ac2 = new Account(2L, "account_Bob", SecurityUtility.passwordEncoder().encode("psw"),
				"xcrethoi76ll", "title1", "bob@gmail.com", "900-678-98-78", "",
				1, LocalDateTime.now(), "", new String[] {"ROLE_ADMIN"});

		list.add(ac2);
		accountRepository.save(list);*/
		
		/*List<Chat> list = new ArrayList();
		final Chat chat1 = new Chat(1L, 1L, 1L, 10, false, true, true, LocalDateTime.now(), true, 2, 1L);
		list.add(chat1);
		final Chat chat2 = new Chat(2L, 1L, 1L, 10, false, true, true, LocalDateTime.now(), true, 3, 2L);
		list.add(chat2);
		final Chat chat3 = new Chat(3L, 1L, 1L, 10, false, true, true, LocalDateTime.now(), false, 7, 0L);
		list.add(chat3);
		final Chat chat4 = new Chat(4L, 1L, 1L, 10, false, true, true, LocalDateTime.now(), false, 1, 0L);
		list.add(chat4);
		final Chat chat5 = new Chat(5L, 1L, 1L, 10, false, true, true, LocalDateTime.now(), true, 17, 0L);
		list.add(chat5);
		final Chat chat6 = new Chat(6L, 1L, 1L, 10, false, true, true, LocalDateTime.now(), false, 5, 0L);
		list.add(chat6);
		
		chatRepository.save(list);*/
		//final Chat chat7 = new Chat(7L, 1L, 1L, 10, false, true, true, LocalDateTime.now().plusDays(2), false, 0L);
		//chatRepository.save(chat7);
		/*final User user = new User(10L, "Dino", "fggtredeweZ", 1, 25, "iujyyyyhxxx", "dino@gmail.com", 5, "German", "", true, LocalDate.now(), "", LocalDate.now(), 100, 1, 1, "", LocalDate.now(), "1893", "Summary", "phone", "photoUrl");	
		userRepository.save(user);*/
		/*List<Engine> list = new ArrayList();
		
		Engine engine1 = new Engine(1L, 0, 3) ;
		list.add(engine1);
		Engine engine2 = new Engine(2L, 0, 2) ;
		list.add(engine2);
		
		engineRepository.save(list);*/
		
		/*ChatUser cu1 = new ChatUser(10L, 1L, 1L);
		chatUserRepository.save(cu1);
		ChatUser cu2 = new ChatUser(10L, 6L, 1L);
		chatUserRepository.save(cu1);*/
		//ChatUser cu1 = new ChatUser(10L, 7L, 1L);
		//chatUserRepository.save(cu1);
		
		/*ChatMessage cm1 =  new ChatMessage(1L, 1L);
		chatMessageRepository.save(cm1);
		ChatMessage cm2 =  new ChatMessage(1L, 2L);
		chatMessageRepository.save(cm2);
		ChatMessage cm3 =  new ChatMessage(2L, 3L);
		chatMessageRepository.save(cm3);*/
		
		/*Group group1 = new Group(1L, "Managers", "", 1L);
		groupRepository.save(group1);
		Group group2 = new Group(2L, "Admins", "", 2L);
		groupRepository.save(group2);
		Group group3 = new Group(3L, "Users", "", 3L);
		groupRepository.save(group3);
		
		GroupUser gu1 = new GroupUser(1L, 10L); 
		groupUserRepository.save(gu1);
		GroupUser gu2 = new GroupUser(1L, 11L); 
		groupUserRepository.save(gu2);
		GroupUser gu3 = new GroupUser(1L, 111L); 
		groupUserRepository.save(gu3);
		
		GroupUser gu4 = new GroupUser(2L, 111L); 
		groupUserRepository.save(gu4);
		
		GroupUser gu5 = new GroupUser(2L, 11L); 
		groupUserRepository.save(gu5);*/
		
		/*ChatUser chu1 = new ChatUser(11L, 2L, 1L);
		chatUserRepository.save(chu1);
		
	
		ChatbotUser cu1 = new ChatbotUser(1L, 10L);
		chatbotUserRepository.save(cu1);
		
		ChatbotUser cu2 = new ChatbotUser(2L, 11L);
		chatbotUserRepository.save(cu2);*/
		
		/*ChatbotChat chatbotChat1 = new ChatbotChat(1L, 1L);
		chatbotChatRepository.save(chatbotChat1);
		ChatbotChat chatbotChat2 = new ChatbotChat(1L, 2L);
		chatbotChatRepository.save(chatbotChat2);
		ChatbotChat chatbotChat3 = new ChatbotChat(1L, 3L);
		chatbotChatRepository.save(chatbotChat3);*/
	}
}

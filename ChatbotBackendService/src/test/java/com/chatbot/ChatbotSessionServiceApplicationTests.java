package com.chatbot;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import com.chatbot.config.SecurityUtility;
import com.chatbot.domain.ChatUser;
import com.chatbot.domain.ChatbotChat;
import com.chatbot.domain.ChatbotUser;
import com.chatbot.domain.Department;
import com.chatbot.domain.Engine;
import com.chatbot.domain.GroupUser;
import com.chatbot.domain.HelpNote;
import com.chatbot.domain.Status;
import com.chatbot.domain.Support;
import com.chatbot.domain.User;
import com.chatbot.repositories.ChatUserRepository;
import com.chatbot.repositories.ChatbotChatRepository;
import com.chatbot.repositories.ChatbotUserRepository;
import com.chatbot.repositories.EngineRepository;
import com.chatbot.repositories.GroupUserRepository;
import com.chatbot.services.CounterServiceImpl;
import com.chatbot.services.DepartmentServiceImpl;
import com.chatbot.services.HelpNoteServiceImpl;
import com.chatbot.services.SupportServiceImpl;
import com.chatbot.services.UserService;

@RunWith(SpringJUnit4ClassRunner.class)//SpringRunner.class)
@SpringBootTest
public class ChatbotSessionServiceApplicationTests {
	@Autowired
	private UserService userService;
	
	@Autowired
	private DepartmentServiceImpl departmentService;
	
	@Autowired
	private SupportServiceImpl supportService;
	
	@Autowired
	private HelpNoteServiceImpl helpNoteService;
	
	@Autowired
	private CounterServiceImpl counterService;
	
	@Autowired
	private ChatbotChatRepository chatbotChatRepository;
	
	@Autowired
	private ChatbotUserRepository chatbotUserRepository;
	
	@Autowired
	private ChatUserRepository chatUserRepository;
	
	@Autowired
	private EngineRepository engineRepository;
	
	@Autowired
	private GroupUserRepository groupUserRepository;
	
	@Test
	public void contextLoads() {
		int count = 1;
		
		/*do {
			 User user = new User();
			 user.setId(counterService.getNextIdSequence());
			 user.setName("Test-user-" + count);
			 user.setPassword(SecurityUtility.passwordEncoder().encode("pswd" + count));
			 user.setIsMale(1);
			 user.setAge(25);
			 user.setToken(String.valueOf(count * 1000000000 + 1));
			 user.setEmail(user.getName() + "@test.com");
			 user.setStatus(Status.A.getCode());
			 user.setLocation("Chickago");
			 user.setEnterPoint("");
			 user.setIsOnline(true);
			 user.setLastOnline(LocalDate.now());
			 user.setComment("test user");
			 user.setTokenTime(LocalDate.now());
			 user.setSecInChats(100);
			 user.setMessagesSent(count * 5);
			 user.setMoneySpend(count * 100 -1);
			 user.setPasswordToken(user.getPassword() + "-" +String.valueOf(count * 127));
			 user.setPasswordTokenTime(LocalDate.now());
			 user.setUUID(UUID.randomUUID().toString());
			 user.setSummary("summary" + count);
			 user.setPhone("678-567-11" + count);
			 user.setPhotoURL("none");
			 
			 userService.saveUser(user);
			 
			 count++;
		} while(count <= 5);*/
		
		/*do {
			Engine engine = new Engine((long) count, 0, 0, count, count);
			engineRepository.save(engine);
			count++;
		} while(count <= 3);*/
		/*do {
			ChatbotChat chatbotChat = new ChatbotChat(1L, (long) count);
			chatbotChatRepository.save(chatbotChat);
			count++;
		} while(count <= 3);*/
		
			/*List<ChatbotUser> cuList = new ArrayList<>();
			ChatbotUser chatbotChat1 = new ChatbotUser(1L, 10L);
			cuList.add(chatbotChat1);
			ChatbotUser chatbotChat2 = new ChatbotUser(1L, 41L);
			cuList.add(chatbotChat2);
			ChatbotUser chatbotChat3 = new ChatbotUser(1L, 42L);
			cuList.add(chatbotChat3);
			chatbotUserRepository.save(cuList);*/
			
			/*List<ChatUser> cuList = new ArrayList<>();
			ChatUser userChat1 = new ChatUser(10L, 1L, 1L);
			cuList.add(userChat1);
			ChatUser userChat2 = new ChatUser(10L, 2L, 1L);
			cuList.add(userChat2);
			ChatUser userChat3 = new ChatUser(41L, 3L, 1L);
			cuList.add(userChat3);
			ChatUser userChat4 = new ChatUser(42L, 4L, 1L);
			cuList.add(userChat4);
			ChatUser userChat5 = new ChatUser(42L, 5L, 1L);
			cuList.add(userChat5);
			chatUserRepository.save(cuList);*/
		
		
			List<GroupUser> guList = new ArrayList<>();
			GroupUser groupUser1 = new GroupUser(1L, 10L);
			guList.add(groupUser1);
			GroupUser groupUser2 = new GroupUser(2L, 41L);
			guList.add(groupUser2);
			GroupUser groupUser3 = new GroupUser(3L, 42L);
			guList.add(groupUser3);
			
			groupUserRepository.save(guList);
			count++;
		
		
		/*do {
			Department department = new Department(0L, "department" + count, 1L);
			departmentService.saveOrUpdate(department);
			count++;
		} while(count <= 5);*/
		
		/*do {
			Support support = new Support(counterService.getNextIdSequence(), 0, 0, "ticket" + count, "", 1, LocalDateTime.now().minusDays(count), LocalDateTime.now(), "", true);
			supportService.saveOrUpdate(support);
			count++;
		} while(count <= 5);*/
		
		/*do {
			HelpNote hn = new HelpNote(counterService.getNextIdSequence(), "help note" + count, "test"); 
			helpNoteService.saveOrUpdate(hn);
			count++;
		} while(count <= 5);*/
	}
}
